import React from "react";
import InvestmentPlanPage from "./InvestmentplanPage";

export default class InvestmentplanList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            investmentplans: [],
            investmentplan: null,
            fetchingInvestmentplans: true
        }

        this.goToList = this.goToList.bind(this);

        this.getInvestmentplans = this.getInvestmentplans.bind(this);

        this.updateInvestmentplans = this.updateInvestmentplans.bind(this);
    }

    getInvestmentplans(investmentplans) {
        this.setState({investmentplans: investmentplans, fetchingInvestmentplans: false});
    }

    componentDidMount() {
        Liferay.Service(
            '/investeringsplan.investeringsplan/get-all-investeringsplannen',
            (investmentplans) => this.getInvestmentplans(investmentplans)
        );
    }

    goToInvestmentplanPage(investmentplan) {
        this.setState({investmentplan: investmentplan});
    }

    goToList() {
        this.setState({investmentplan: null});
    }

    updateInvestmentplans(investmentplan){
        let copyArr = this.state.investmentplans;

        for (let i = 0; i < copyArr.length; i++) {
            if (copyArr[i].investeringsplanId === investmentplan.investeringsplanId){
                copyArr[i] = investmentplan;
            }
        }

        this.setState({investmentplans: copyArr});
    }

    render() {
        if (this.state.investmentplan !== null) {
            return <InvestmentPlanPage investmentplan={this.state.investmentplan}
                                       goToList={this.goToList} updateInvestmentplans={this.updateInvestmentplans}/>
        }

        return (<div>
                {this.state.investmentplans.length === 1 ?
                    <h2>Investment plan</h2>
                    : <h2>Investment plans</h2>}

                <br/>

                {this.state.fetchingInvestmentplans === false ?
                    <div>
                        {this.state.investmentplans.length === 0 ?
                            <h2 style={{'textAlign': "center"}}>You haven't invested in any investment plans</h2> :
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Progress</th>
                                </tr>
                                </thead>

                                <tbody>
                                {this.state.investmentplans.map((investmentplan, index) =>
                                    <tr key={index} onClick={() => this.goToInvestmentplanPage(investmentplan)}>
                                        <th scope="row">{index + 1}</th>
                                        <td>{investmentplan.naam}</td>
                                        <td>{investmentplan.voortgang} %</td>
                                    </tr>)}
                                </tbody>
                            </table>}
                    </div>
                    : <h2>Loading...</h2>}

            </div>
        )
    }
}