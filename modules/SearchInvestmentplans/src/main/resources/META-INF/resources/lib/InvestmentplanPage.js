import React from "react";
import Milestones from "./MilestonePlan";
import Moment from "moment";

export default class InvestmentplanPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fetchingMilestones: true,
            investmentplan: props.investmentplan,
            milestones: [],
            modal: false
        }

        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);

        this.invest = this.invest.bind(this);

        this.addMilestones = this.addMilestones.bind(this);
        this.formatMilestoneData = this.formatMilestoneData.bind(this);

        this.updateInvestmentplan = this.updateInvestmentplan.bind(this);
    }

    addMilestones(milestones) {
        this.setState({milestones: milestones, fetchingMilestones: false});
    }

    componentDidMount() {
        Liferay.Service(
            '/milestone.milestone/get-milestones-by-investment-plan-id',
            {
                investmentPlanId: this.state.investmentplan.investeringsplanId
            },
            (milestones) => this.addMilestones(milestones)
        );
    }

    formatMilestoneData() {
        let data = [];
        let startDate = this.state.investmentplan.begindatum;

        this.state.milestones.map((milestone, index) => {
            let startDateFormat = new Date(startDate);
            let endDateFormat = new Date(milestone.einddatum);

            data.push(["Milestone " + (index + 1), milestone.naam, Moment(new Date(startDateFormat.getFullYear(), startDateFormat.getMonth(), startDateFormat.getDate())).toDate()
                , Moment(new Date(endDateFormat.getFullYear(), endDateFormat.getMonth(), endDateFormat.getDate())).toDate()]);
            startDate = milestone.einddatum;
        });

        return data;
    }

    updateInvestmentplan(investmentplan){
        this.setState({investmentplan: investmentplan});
        this.props.updateInvestmentplans(investmentplan);

        this.hideModal();
    }

    invest(amount) {
        Liferay.Service(
            '/investering.investering/add-investering',
            {
                investeringsplanId: this.state.investmentplan.investeringsplanId,
                participantId: 1,
                waarde: amount
            },
            function(obj) {
                console.log(obj);
            }
        );

        let currentFunding = (this.state.investmentplan.doel * this.state.investmentplan.voortgang) / 100;
        let newFunding = parseInt(currentFunding) + parseInt(amount);
        let newVoortgang = (newFunding / this.state.investmentplan.doel) * 100;

        Liferay.Service(
            '/investeringsplan.investeringsplan/update-investeringsplan-progress',
            {
                investeringsplanId: this.state.investmentplan.investeringsplanId,
                voortgang: newVoortgang
            },
            (investmentplan) => this.updateInvestmentplan(investmentplan)
        );
    }

    showModal() {
        this.setState({modal: true});
    }

    hideModal() {
        this.setState({modal: false});
    }

    render() {
        return (
            <div className="investment-plan-page">
                <div>
                    <svg className="bi bi-arrow-left-short" width="48" height="48" viewBox="0 0 16 16"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg" onClick={this.props.goToList}>
                        <path fillRule="evenodd"
                              d="M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z"/>
                        <path fillRule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </div>

                {this.state.fetchingMilestones === false ?
                    <div className="portlet-div">
                        <div className="flexboxhorizontal">
                            <div className="childcenter"><img
                                src="https://www.liferay.com/o/osb-www-theme/images/custom/open_graph_image.png"
                                alt="Logo"
                                width="300px" height="300px"/></div>
                            <div className="roundborder">
                                <div className="plan-info">
                                    <h1>{this.state.investmentplan.naam}</h1>
                                    <hr/>
                                    <h2>Status</h2>
                                    <p>{this.state.investmentplan.status}</p>
                                    <h2>Target</h2>
                                    <p>{new Intl.NumberFormat('nl-NL', {
                                        style: 'currency',
                                        currency: 'EUR'
                                    }).format(this.state.investmentplan.doel)}</p>
                                    <h2>Progress</h2>
                                    <p>{this.state.investmentplan.voortgang}%</p>
                                </div>
                            </div>
                        </div>

                        <div className="flexboxhorizontal">
                            <div className="childborder milestone-description">
                                <h2>Beschrijving</h2>
                                {this.state.investmentplan.beschrijving}
                            </div>
                        </div>

                        <div className="flexboxhorizontal">
                            <Milestones milestones={this.formatMilestoneData()}/>
                        </div>
                    </div>
                    : <h2>Loading...</h2>}

                <div className="flexbuttons">
                    <button type="button" className="btn btn-primary" onClick={this.showModal}
                            style={{margin: "0px 12px 0px 0px"}}>Invest
                    </button>
                </div>

                {this.state.modal === true ?
                    <Modal hideModal={this.hideModal} invest={this.invest} investmentplan={this.state.investmentplan}/>
                    : <div/>}
            </div>)
    }
}

class Modal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLongTitle">Invest
                            in {this.props.investmentplan.naam}</h5>

                        <button type="button" className="close" aria-label="Close" onClick={this.props.hideModal}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>Amount in euro's:</p>
                        <input type="number" className="form-control" id="amount"/>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={this.props.hideModal}>Cancel
                        </button>
                        <button type="button" className="btn btn-primary"
                                onClick={() => this.props.invest(document.getElementById('amount').value)}>Invest
                        </button>
                    </div>
                </div>
            </div>
        </div>
    }
}
