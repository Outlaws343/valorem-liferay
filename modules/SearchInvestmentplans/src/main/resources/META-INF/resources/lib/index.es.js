import React from 'react';
import ReactDOM from 'react-dom';
import InvestmentplanList from "./InvestmentplanList.js";

export default function (elementId) {
	ReactDOM.render(<InvestmentplanList/>, document.getElementById(elementId));
}