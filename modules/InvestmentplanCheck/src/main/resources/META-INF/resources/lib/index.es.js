import React from 'react';
import ReactDOM from 'react-dom';
import InvestmentList from "./InvestmentList.js";

export default function (elementId) {
	ReactDOM.render(<InvestmentList/>, document.getElementById(elementId));
}