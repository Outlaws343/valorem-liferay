import React from "react";
import InvestmentplanPage from "./InvestmentplanPage";

export default class InvestmentList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            investmentplans: [],
            investmentplan: null,
            investmentplancheck: [],
            investmentplanaccepted: [],
            investmentplandenied: [],
            dropdowntext: "Controle",
            dropdownselected: {
                checked: true,
                accepted: false,
                denied: false
            }
        }

        this.goToList = this.goToList.bind(this);

        this.getInvestmentplans = this.getInvestmentplans.bind(this);
        this.changeInvesteringsplanStatus = this.changeInvesteringsplanStatus.bind(this);
    }

    getInvestmentplans(investmentplans, key) {
        switch (key) {
            case "notchecked":
                this.setState({investmentplans: investmentplans, investmentplancheck: investmentplans});
                break;
            case "accepted":
                this.setState({investmentplanaccepted: investmentplans});
                break;
            case "denied":
                this.setState({investmentplandenied: investmentplans});
                break;
        }
    }

    componentDidMount() {
        Liferay.Service(
            '/investeringsplan.investeringsplan/get-not-checked-investeringsplan',
            (obj) => this.getInvestmentplans(obj, "notchecked")
        );

        Liferay.Service(
            '/investeringsplan.investeringsplan/get-accepted-investeringsplan',
            (obj) => this.getInvestmentplans(obj, "accepted")
        );

        Liferay.Service(
            '/investeringsplan.investeringsplan/get-denied-investeringsplan',
            (obj) => this.getInvestmentplans(obj, "denied")
        );
    }

    goToAccountPage(investmentplan) {
        this.setState({investmentplan: investmentplan});
    }

    goToList() {
        this.setState({investmentplan: null});
    }

    changeDropdown(selected) {
        switch (selected) {
            case "Controle":
                this.setState({
                    investmentplans: this.state.investmentplancheck, dropdowntext: selected,
                    dropdownselected: {checked: true, accepted: false, denied: false}
                });
                break;
            case "Geaccepteerd":
                this.setState({
                    investmentplans: this.state.investmentplanaccepted, dropdowntext: selected,
                    dropdownselected: {checked: false, accepted: true, denied: false}
                });
                break;
            case "Geweigerd":
                this.setState({
                    investmentplans: this.state.investmentplandenied, dropdowntext: selected,
                    dropdownselected: {checked: false, accepted: false, denied: true}
                });
                break;
        }
    }

    changeInvesteringsplanStatus(isAccept, reason){
        let newInvesteringsplanCheck = this.state.investmentplans.filter(investmentplan => investmentplan !== this.state.investmentplan);

        this.state.investmentplan.reden = reason;

        if (isAccept){
            this.setState({investmentplanaccepted: this.state.investmentplanaccepted.concat(this.state.investmentplan), investmentplancheck: newInvesteringsplanCheck, investmentplans: newInvesteringsplanCheck});
        } else {
            this.setState({investmentplandenied: this.state.investmentplandenied.concat(this.state.investmentplan), investmentplancheck: newInvesteringsplanCheck, investmentplans: newInvesteringsplanCheck});
        }
    }

    render() {
        if (this.state.investmentplan !== null) {
            return <InvestmentplanPage investmentplan={this.state.investmentplan}
                                       checked={this.state.dropdownselected}
                                       goToList={this.goToList}
            changeInvesteringsplanStatus={this.changeInvesteringsplanStatus}/>
        }

        return (<div>
                <div>
                    {this.state.investmentplans.length === 1 ?
                        <h2>Investeringsplan controle</h2>
                    : <h2>Investeringsplannen controle</h2>}

                    <div className="dropdown">
                        <button className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            {this.state.dropdowntext}
                        </button>

                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            {this.state.dropdownselected.checked === true ?
                                <a className="dropdown-item active"
                                   onClick={() => this.changeDropdown("Controle")}>Controle</a> :
                                <a className="dropdown-item"
                                   onClick={() => this.changeDropdown("Controle")}>Controle</a>}

                            {this.state.dropdownselected.accepted === true ?
                                <a className="dropdown-item active"
                                   onClick={() => this.changeDropdown("Geaccepteerd")}>Geaccepteerd</a> :
                                <a className="dropdown-item"
                                   onClick={() => this.changeDropdown("Geaccepteerd")}>Geaccepteerd</a>}

                            {this.state.dropdownselected.denied === true ?
                                <a className="dropdown-item active"
                                   onClick={() => this.changeDropdown("Geweigerd")}>Geweigerd</a> :
                                <a className="dropdown-item"
                                   onClick={() => this.changeDropdown("Geweigerd")}>Geweigerd</a>}
                        </div>
                    </div>
                </div>


                {this.state.investmentplans.length === 0 ?
                    <h2 style={{'textAlign': "center"}}>Er zijn geen nieuwe investeringsplannen ingediend</h2> :
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Naam</th>
                            <th scope="col">Beschrijving</th>
                        </tr>
                        </thead>

                        <tbody>
                        {this.state.investmentplans.map((investmentplan, index) =>
                            <tr key={index} onClick={() => this.goToAccountPage(investmentplan)}>
                                <th scope="row">{investmentplan.investeringsplanId}</th>
                                <td>{investmentplan.naam}</td>
                                <td>{investmentplan.beschrijving}</td>
                            </tr>)}
                        </tbody>
                    </table>}
            </div>
        )
    }
}