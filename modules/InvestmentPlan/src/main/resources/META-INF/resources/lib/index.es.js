import React from 'react';
import ReactDOM from 'react-dom';
import List from "./List.js";

export default function (elementId) {
	ReactDOM.render(<List/>, document.getElementById(elementId));
}