import React, { Component } from "react";
import Question, { FormQuestionTypes } from './question';
import Moment from "moment";

/**
 * An array with the questions and it's basic configuration for the form.
 *
 * @type {({id: string, placeholder: string, title: string, type: {args: undefined, key: string}, info: undefined}|{id: string, placeholder: string, title: string, type: {args: {maxLength: number}, key: string}, info: string}|{id: string, placeholder: string, title: string, type: {args: {min: number, type: string}, key: string}, info: string}|{id: string, placeholder: string, title: string, type: {args: {min: string, type: string}, key: string}, info: string}|{id: string, placeholder: string, title: string, type: {args: undefined, key: string}, info: string})[]}
 */
let questions = [
    {
        id: "name",
        title: "Investment plan name",
        placeholder: "The name of the new investment plan",
        info: undefined,
        type: {
            key: FormQuestionTypes.INPUT,
            args: undefined
        }
    },
    {
        id: "description",
        title: "Investment plan description",
        placeholder: "The description of the new investment plan",
        info: "The investment plan description has a maximum 200 characters.",
        type: {
            key: FormQuestionTypes.TEXTAREA,
            args: {
                maxLength: 200
            }
        }
    },
    {
        id: "target",
        title: "Investment plan target amount",
        placeholder: "The target amount for the new investment plan",
        info: "Target amount is the amount of Euro's you want to raise",
        type: {
            key: FormQuestionTypes.INPUT,
            args: {
                type: "number",
                min: 0
            }
        }
    },
    {
        id: "startDate",
        title: "Investment plan start date",
        placeholder: "The start date of the new investment plan",
        info: "",
        type: {
            key: FormQuestionTypes.INPUT,
            args: {
                type: "date",
                min: Moment().format("YYYY-MM-DD")
            }
        }
    },
    {
        id: "milestones",
        title: "Investment plan milestones",
        placeholder: "The milestones of the new investment plan",
        info: "Add here your milestones for the new investment plan.\n" +
            "NOTE: The end date of the last milestone is also the end date of the investment plan!",
        type: {
            key: FormQuestionTypes.LIST,
            args: undefined
        }
    }
];

/**
 * A component build in React to generate, render and largely handle the functionality to make and add an new investment plan.
 *
 * Bundled with the InvestmentPlanList module and bundles the Question and FormQuestionTypes modules.
 *
 * The Question module generates, renders and largely handles the questions of the form.
 *
 * The FormQuestionTypes is an predefined list of FormQuestion types Enums.
 *
 * @see List InvestmentPlanList
 * @see Question FormQuestion
 * @see FormQuestionTypes
 */
export default class InvestmentPlanForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            totalPercentage: 0,
            newPlan: {
                name: "",
                status: "Pending review...",
                target: 0,
                progress: 0,
                description: "",
                logo: "https://www.liferay.com/o/osb-www-theme/images/custom/open_graph_image.png",
                startDate: Moment(),
                milestones: []
            },
            errors: {}
        }

        this.handleChange = this.handleChange.bind(this);
        this.addMilestone = this.addMilestone.bind(this);
        this.removeMilestone = this.removeMilestone.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Handle the event when the value of a question has changed.
     *
     * @param e The handleChange event that contains the changes value.
     */
    handleChange(e) {
        let changes = e.target.name === "startDate"? {[e.target.name]: Moment(e.target.value, "YYYY-MM-DD")} :
            {[e.target.name]: e.target.value};
        this.setState(prevState => {
            return {newPlan: {...prevState.newPlan, ...changes}};
        });
    }

    /**
     * Add a new milestone to the state.
     *
     * @param newMilestone The new milestone
     * @param totalPercentage The total percentage of the investment target that has already been divided
     * by the milestones including the new milestone.
     */
    addMilestone(newMilestone, totalPercentage) {
        this.setState(prevState => {
            let newMilestones = prevState.newPlan.milestones;
            newMilestones.push(newMilestone);
            return {
                totalPercentage: totalPercentage,
                newPlan: {...prevState.newPlan, milestones: newMilestones}
            }
        });
    }

    /**
     * Remove the given milestone from the state.
     *
     * @param milestone The milestone to be deleted
     * @param totalPercentage The total percentage of the investment target that has already been divided
     * by the milestones including the new milestone.
     */
    removeMilestone(milestone, totalPercentage) {
        this.setState(prevState => {
            let milestones = prevState.newPlan.milestones;
            let index = milestones.indexOf(milestone);
            if(index > -1)
                milestones.splice(index, 1);
            return {
                totalPercentage: totalPercentage,
                newPlan: {...prevState.newPlan, milestones: milestones}}
        });
    }

    /**
     * Validate the values of the questions of the form and sets/updates the errors section of the state when there are errors.
     *
     * @returns {boolean} Returns true if everything is valid.
     *                    Returns false if something is invalid.
     */
    validatePlan() {
        let plan = this.state.newPlan;
        let errors = {};

        if(!plan.name)
            errors.name = "The name of the investment plan is required!";

        if(!plan.target)
            errors.target = "The target amount of the investment plan is required and needs to be greater than 0!";

        if(!plan.description)
            errors.description = "The description of the investment plan is required!";

        if(!plan.description.length > 200)
            errors.description = "The description may not exceed the 200 characters!";

        if(!plan.milestones)
            errors.milestones = "The investment plan needs to have at least 1 milestone!";

        if(this.state.totalPercentage !== 100)
            errors.totalPercentage = "The payment percentages of the all the milestones must be 100%!"

        this.setState({errors: errors});

        return Object.keys(errors).length === 0;
    }

    /**
     * Handle the form submit event. This function ignores the default actions of the handleSubmit and runs the
     * validate function of the module.
     *
     * If the validation fails it returns the function and nothing happens.
     *
     * If the validation succeeds it calls the submitHandler function and the goToList helper function passed via the properties.
     *
     * @see this.props.addInvestmentPlan submitHandler
     * @see this.props.goToList goToList
     *
     * @param event The handleSubmit event from the form.
     */
    handleSubmit(event) {
        event.preventDefault();

        if(!this.validatePlan())
            return;

        this.props.submitHandler(this.state.newPlan);
        this.props.goToList();
    }

    render() {
        return (
            <div className="investment-plan-form mb-7">
                <div>
                    <svg className="bi bi-arrow-left-short" width="48" height="48" viewBox="0 0 16 16"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg" onClick={this.props.goToList}>
                        <path fillRule="evenodd"
                              d="M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z"/>
                        <path fillRule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </div>
                <form onSubmit={this.handleSubmit} className="needs-validation" noValidate>
                    {this.state.errors["totalPercentage"] && <div className="alert alert-danger" role="alert">
                        {this.state.errors["totalPercentage"]}
                    </div>}
                    {questions.map((question, index) => {
                        return <Question key={index}
                                         question={question}
                                         planData={this.state.newPlan}
                                         handleChange={this.handleChange}
                                         totalPercentage={this.state.totalPercentage}
                                         addMilestone={this.addMilestone}
                                         removeMilestone={this.removeMilestone}
                                         error={this.state.errors[question.id]} />
                    })}
                    <button type="submit" className="btn btn-primary float-right">Submit new investment plan</button>
                </form>
            </div>
        )
    }
}
