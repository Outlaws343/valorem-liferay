import React from "react";
import Milestones from "./MilestonePlan"
import Modal from "./modal"

/**
 * A component build in React to display a detail page with all the information available about a specific investment plan.
 *
 * Bundled with the InvestmentPlanList module and bundles the Milestone and Modal modules.
 *
 * The Milestones module shows an Google Chart with the data from the milestones.
 *
 * The Modal handles partly the functionality to complete the next milestone of an investment plan.
 *
 * @see List InvestmentPlanList
 * @see Milestones
 * @see Modal
 *
 */
export default class InvestmentPlanPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            milestones: [],
            modal: false
        }

        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.updateAction = this.updateAction.bind(this);
    }

    componentDidMount() {
        let data = [];
        let startDate = this.props.plan.startDate;

        // Compile an array with the data from the milestones that is compatible with the google chart in the Milestones module
        if(this.props.plan.milestones.length > 0) {
            this.props.plan.milestones.map((milestone, index) => {
                data.push(["Milestone " + (index + 1), milestone.name, startDate.toDate(), milestone.endDate.toDate()]);
                startDate = milestone.endDate;
            });
        }

        this.setState({milestones: data});
    }

    showModal() {
        this.setState({modal: true});
    }

    hideModal() {
        this.setState({modal: false});
    }

    /**
     * An handler method given to the Modal module as an attribute to update the milestone in the system via the API and
     * calls the updateData handler function passed by the properties.
     *
     * @see Modal
     *
     * @param description The description of why the milestone is complete
     */
    updateAction(description) {
        Liferay.Service(
            '/milestone.milestone/complete-next-milestone',
            {
                investeringsplanId: this.props.plan.investmentPlanId,
                beschrijving: description
            },
            () => {}
        );
        this.props.updateData();
        this.hideModal();
    }

    render() {
        let showReason = this.props.plan.isDenied && this.props.plan.isCheck;
        return (
            <div className="investment-plan-page">
                <div className="controls">
                    <svg className="bi bi-arrow-left-short" width="48" height="48" viewBox="0 0 16 16"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg" onClick={this.props.goToList}>
                        <path fillRule="evenodd"
                              d="M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z"/>
                        <path fillRule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                    {!this.props.plan.allMilestonesAreCompleted && <button className="btn btn-info complete-milestone float-right" onClick={this.showModal}>
                        Complete the next milestone
                    </button>}
                </div>
                <div className="portlet-div">
                    <div className="flexboxhorizontal">
                        <div className="childcenter"><img
                            src={this.props.plan.logo}
                            alt="Logo"
                            width="300px" height="300px"/></div>
                        <div className="roundborder">
                            <div className="plan-info">
                                <h1>{this.props.plan.name}</h1>
                                <hr />
                                <h2>Status</h2>
                                <p>{this.props.plan.status}</p>
                                <h2>Target</h2>
                                <p>{new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(this.props.plan.target)}</p>
                                <h2>Progress</h2>
                                <p>{this.props.plan.progress}%</p>
                            </div>
                        </div>
                    </div>

                    <div className="flexboxhorizontal">
                        <div className="childborder milestone-description">
                            <h2>Description</h2>
                            {this.props.plan.description}
                        </div>
                    </div>

                    {this.state.milestones.length > 0 ?
                        <div className="flexboxhorizontal">
                            <Milestones milestones={this.state.milestones} />
                        </div> : <h2>Milestones not available</h2>}

                    {showReason === true ?
                        <div className="flexboxhorizontal">
                            <div className="childborder">
                                <h2>Reden van afwijzing</h2>
                                <p>{this.props.plan.reason}</p>
                            </div>
                        </div>
                        : <div/>}

                </div>

                {this.state.modal === true &&
                    <Modal hideModal={this.hideModal}
                           updateAction={this.updateAction}/>}
            </div>
        )
    }
}