import React from "react";
import Chart from "react-google-charts";

var dataTypes = [[
    { type: 'string', id: 'Milestone' },
    { type: 'string', id: 'Name' },
    { type: 'date', id: 'Start' },
    { type: 'date', id: 'End' }]];

/**
 * A component build in React to show an Google Chart with the data from the milestones.
 *
 * Bundled with the InvestmentPlanPage module.
 *
 * @see InvestmentPlanPage
 */
export default class MilestonePlan extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: dataTypes.concat(props.milestones),
            number: props.milestones.length
        }
    }

    render() {
        return (<div className="childborder milestone-diagram">
            <Chart
        width={'100%'}
        height={50 + 50 * this.state.number}
        chartType="Timeline"
        loader={<div>Loading Chart</div>}
        data={this.state.data}
        />
        </div>
    );
    }
}