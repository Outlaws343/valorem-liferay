import React, {Component} from "react";
import InvestmentPlanPage from "./InvestmentPlanPage";
import InvestmentPlanForm from "./form/index";
import Moment from "moment";

let defaultLogo = "https://www.liferay.com/o/osb-www-theme/images/custom/open_graph_image.png";

/**
 * A component build in React to display an interactive list of investment plans.
 *
 * Bundles with this module comes the InvestmentPlanPage and the InvestmentPlanForm modules.
 *
 * The InvestmentPlanPage displays the information of an investment plan you have clicked on in the list.
 *
 * The InvestmentPlanForm adds the functionality to submit new investment plans and can be accessed with the appropriate button above the list.
 *
 * @see InvestmentPlanPage
 * @see InvestmentPlanForm
 */
export default class List extends Component {
    constructor(props) {
        super(props);

        this.state = {
            issuerId: 1,
            fetchingInvestmentPlans: true,
            investmentPlans: [],
            redirect: false,
            investmentPlan: null
        }

        this.goToList = this.goToList.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.updateData = this.updateData.bind(this);
        this.addInvestmentPlan = this.addInvestmentPlan.bind(this);
        this.addMilestones = this.addMilestones.bind(this);
        this.goToSubmitNewInvestmentPlanForm = this.goToSubmitNewInvestmentPlanForm.bind(this);
        this.goToInvestmentPlanPage = this.goToInvestmentPlanPage.bind(this);
        this.getInvestmentPlans = this.getInvestmentPlans.bind(this);
        this.getMilestones = this.getMilestones.bind(this);
    }

    /**
     * Create from the received investment plans data from the API investment plan objects that this React module can use.
     * This includes fetching the milestone data from each investment plan.
     *
     * @param investmentPlans A List with investment plans (fetched from the API).
     */
    getInvestmentPlans(investmentPlans) {
        let plans = [];
        if(investmentPlans.length) {
            investmentPlans.map(ip => {
                let plan = {
                    name: ip.naam,
                    status: ip.status,
                    description: ip.beschrijving,
                    target: ip.doel,
                    progress: ip.voortgang,
                    logo: defaultLogo,
                    startDate: Moment(ip.begindatum),
                    investmentPlanId: ip.investeringsplanId,
                    issuerId: ip.issuerId,
                    isCheck: ip.gecontroleerd,
                    isDenied: !ip.geaccepteerd,
                    reason: ip.reden,
                    allMilestonesAreCompleted: false
                }
                // Fetch all the milestones of a investment plan
                Liferay.Service(
                    '/milestone.milestone/get-milestones-by-investment-plan-id',
                    {
                        investmentPlanId: plan.investmentPlanId
                    },
                    milestones => {
                        let result = this.getMilestones(milestones);
                        plan.milestones = result.milestonesList;
                        plan.allMilestonesAreCompleted = result.allMilestonesAreCompleted;
                        plans.push(plan);
                        this.setState({
                            fetchingInvestmentPlans: false,
                            investmentPlans: plans
                        });
                    }
                )
            });
        }
    }

    /**
     * Create from the received milestones data from the API milestone objects that this React module can use.
     *
     * @param milestones A List with milestones (fetched from the API).
     *
     * @returns {{allMilestonesAreCompleted: boolean, milestonesList: []}} A object with the boolean property allMilestonesAreCompleted
     * that indicates if all the milestones are completed already and the property milestonesList which is a list with milestones objects
     * in the format this React module can use.
     */
    getMilestones(milestones) {
        let allMilestonesAreCompleted = true;
        let milestonesList = [];
        milestones.map(ms => {
            milestonesList.push({
                name: ms.naam,
                milestonePercentage: ms.milestone_percentage,
                endDate: Moment(ms.einddatum),
                description: ms.beschrijving,
                isCheck: ms.gecontroleerd,
                isDenied: !ms.geaccepteerd,
            });
            if(ms.inleverings_beschrijving.length === 0) {
                allMilestonesAreCompleted = false;
            }
        });
        return {
            milestonesList,
            allMilestonesAreCompleted: allMilestonesAreCompleted
        };
    }

    /**
     * When this component mounts call the fetchData function to fetch all the required data from the API.
     *
     * @see fetchData
     */
    componentDidMount() {
        this.fetchData(1);
    }

    /**
     * A helper method to update the current data by fetching the newly updated data from the API
     * and updates the state with the new values.
     */
    updateData() {
        this.fetchData(this.state.issuerId);
        this.setState(prevState => {
            return {
                investmentPlan: {...prevState.investmentPlan, allMilestonesAreCompleted: true}
            }
        })
    }

    /**
     * Get all the investment plans from an issuer including all the milestones
     *
     * @param issuerId The id of the issuer
     */
    fetchData(issuerId) {
        Liferay.Service(
            '/investeringsplan.investeringsplan/get-investeringsplans-by-issuer-id',
            {
                issuerId: issuerId
            },
            investmentPlans => this.getInvestmentPlans(investmentPlans)
        );
    }

    /**
     * If an investment plan is clicked on show the InvestmentPlanPage by updating the state.
     *
     * @see InvestmentPlanPage
     *
     * @param investmentPlan The investment plan that is clicked on.
     */
    goToInvestmentPlanPage(investmentPlan) {
        this.setState({redirect: true, investmentPlan: investmentPlan});
    }

    /**
     * An handler method given to an child module as an attribute to update the state to show the list again.
     *
     * @see List InvestmentPlanList
     */
    goToList() {
        this.setState({redirect: false, investmentPlan: null});
    }

    /**
     * If an investment plan is clicked on show the InvestmentPlanPage by updating the state.
     *
     * @see InvestmentPlanForm
     */
    goToSubmitNewInvestmentPlanForm() {
        this.setState({redirect: true, investmentPlan: null});
    }

    /**
     * Add each given milestone to the system via the API.
     *
     * @param milestones List with new milestones
     * @param investmentPlan The investment plan includes the milestones
     */
    addMilestones(milestones, investmentPlan) {
        milestones.map((milestone, index) => {
            Liferay.Service(
                '/milestone.milestone/add-milestone',
                {
                    naam: milestone.name,
                    investeringsplanId: investmentPlan.investeringsplanId,
                    eindDatum: milestone.endDate.format("YYYY-MM-DD"),
                    percentage: milestone.percentage,
                    beschrijving: milestone.description
                },
                (milestone) => {
                    if(index === (milestones.length - 1))
                        this.fetchData(investmentPlan.issuerId);
                }
            );
        });
    }

    /**
     * Add the investment plan to the system via the API.
     *
     * @param newPlan The new investment plan
     */
    addInvestmentPlan(newPlan) {
        Liferay.Service(
            '/investeringsplan.investeringsplan/add-investeringsplan',
            {
                naam: newPlan.name,
                doel: newPlan.target,
                beginDatum: newPlan.startDate.format("YYYY-MM-DD HH:mm:ss"),
                beschrijving: newPlan.description
            },
            investmentPlan => this.addMilestones(newPlan.milestones, investmentPlan)
        );
    }

    render() {
        if (this.state.investmentPlan !== null && this.state.redirect === true) {
            return <InvestmentPlanPage plan={this.state.investmentPlan} goToList={this.goToList} updateData={this.updateData}/>
        } else if (this.state.investmentPlan === null && this.state.redirect === true) {
            return <InvestmentPlanForm plans={this.state.investmentPlans}
                                       submitHandler={this.addInvestmentPlan}
                                       goToList={this.goToList}/>
        }

        return (<div className="investment-plans-list">
                <button className="btn btn-primary new-investment-plan" onClick={this.goToSubmitNewInvestmentPlanForm}>
                    Apply a new investment plan
                </button>
                {this.state.fetchingInvestmentPlans ? <h2 style={{'textAlign': "center"}}>Fetching investment plans...</h2> :
                    this.state.investmentPlans.length === 0 ? <h2 style={{'textAlign': "center"}}>There are no investment plans</h2> :
                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col-8">Investment Plan</th>
                                <th scope="col">Status</th>
                            </tr>
                            </thead>

                            <tbody>
                            {this.state.investmentPlans.map((ip, index) =>
                                <tr key={index} onClick={() => this.goToInvestmentPlanPage(ip)}>
                                    <td>{ip.name}</td>
                                    <td>{ip.status}</td>
                                </tr>)}
                            </tbody>
                        </table>
                }
            </div>
        )
    }
}