import React from "react";

/**
 * A component build in React to handle partly the functionality to complete a new milestone of an investment plan.
 *
 * Bundled with the InvestmentPlanPage module.
 *
 * @see InvestmentPlanPage
 */
export default class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: "",
            description: ""
        }

        this.validate = this.validate.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Handle the event when the value of the description textArea has changed.
     *
     * @param e The handleChange event from the textArea which contains the changes value.
     */
    handleChange(e) {
        this.setState({description: e.target.value});
    }

    /**
     * Handle the form submit event. This function ignores the default actions of the handleSubmit and runs the
     * validate function of the module.
     *
     * If the validation fails it returns the function and nothing happens.
     *
     * If the validation succeeds it calls the updateAction handler function passed via the properties.
     *
     * @see updateAction
     *
     * @param e The handleSubmit event from the form.
     */
    handleSubmit(e) {
        e.preventDefault();

        if(!this.validate())
            return;

        this.props.updateAction(this.state.description);
    }

    validate() {
        let error = "";

        if(!this.state.description)
            error = "A description is required to complete a milestone!";

        if(this.state.description.length > 75)
            error = "The description may not exceed the 75 characters!";

        this.setState({error: error});
        return error.length === 0;
    }

    render() {
        return <div className="modal" id="complete-milestone-modal" tabIndex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <form onSubmit={this.handleSubmit} className="needs-validation" noValidate>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Complete the next milestone</h5>
                            <button type="button" className="close" aria-label="Close" onClick={this.props.hideModal}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="form-group">
                                <label htmlFor="description">Description of completion</label>
                                <textarea className={"form-control " + (this.state.error.length > 0 && "is-invalid")}
                                          id="description"
                                          rows="5"
                                          maxLength="75"
                                          value={this.state.description}
                                          onChange={this.handleChange} />
                                <small className="desc-count float-right">({this.state.description.length}/75)</small>
                                {this.state.error && <div className="invalid-feedback">{this.state.error}</div>}
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={this.props.hideModal}>Cancel
                            </button>
                            <button type="submit" className="btn btn-primary">Complete milestone</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    }
}