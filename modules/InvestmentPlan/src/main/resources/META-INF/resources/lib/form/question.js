import React, { Component } from "react";
import FormList from "./list";
import Moment from "moment";

/**
 * A component build in React to generate, render and largely handle the functionality for the InvestmentPlanForm.
 *
 * Bundled with the InvestmentPlanForm module and bundles the FormMilestoneList and FormQuestionTypes modules.
 *
 * The FormMilestoneList module generates, renders and largely handles the list and questions for the milestones
 * section of the form.
 *
 * The FormQuestionTypes is an predefined list of FormQuestion types Enums.
 *
 * @see InvestmentPlanForm
 * @see FormList FormMilestoneList
 * @see FormQuestionTypes
 */
export default class FormQuestion extends Component {
    constructor(props) {
        super(props);

        this.renderInput = this.renderInput.bind(this);
    }

    /**
     * Render a specific input or inputs for a form question.
     * @returns {null|*} Null or a JSX Object in form of a textarea, FormList or an input.
     */
    renderInput() {
        if(this.props.question.type === undefined)
            return null;

        switch (this.props.question.type.key) {
            case FormQuestionTypes.TEXTAREA:
                return <textarea name={this.props.question.id}
                                 {... this.props.question.type.args}
                                 value={this.props.planData[this.props.question.id]}
                                 className={"form-control " + (this.props.error && "is-invalid")}
                                 id={this.props.question.id}
                                 placeholder={this.props.question.placeholder}
                                 onChange={this.props.handleChange} />
            case FormQuestionTypes.LIST:
                return <FormList question={this.props.question}
                                 plan={this.props.planData}
                                 totalPercentage={this.props.totalPercentage}
                                 addMilestone={this.props.addMilestone}
                                 removeMilestone={this.props.removeMilestone} />
            case FormQuestionTypes.INPUT:
            default:
                return <input name={this.props.question.id}
                              {... this.props.question.type.args}
                              value={this.props.question.id === "startDate" ?
                                  Moment(this.props.planData[this.props.question.id]).format("YYYY-MM-DD") :
                                  this.props.planData[this.props.question.id]}
                              className={"form-control " + (this.props.error && "is-invalid")}
                              id={this.props.question.id}
                              placeholder={this.props.question.placeholder}
                              onChange={this.props.handleChange} />
        }
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor={this.props.question.id}>{this.props.question.title}</label>{this.props.question.info && (
                    <small className="form-text text-muted">{this.props.question.info}</small>)}
                    {this.renderInput()}
                    {this.props.question.id === "description" && <small className="desc-count float-right">({this.props.planData[this.props.question.id].length}/200)</small>}
                    {this.props.error && <div className="invalid-feedback">{this.props.error}</div>}
                </div>
            </div>
        )
    }
}

/**
 * Question types for the form questions.
 *
 * Bundled with the InvestmentPlanForm and the FormQuestion modules.
 *
 * @see FormQuestion
 * @see InvestmentPlanForm
 *
 * @type {{INPUT: string, TEXTAREA: string, LIST: string}}
 */
export const FormQuestionTypes = {
    INPUT: "input",
    TEXTAREA: "textarea",
    LIST: "list",
}
