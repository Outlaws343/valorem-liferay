import React, { Component } from "react";
import Moment from "moment";

/**
 * A component build in React to generate, render and largely handle the functionality for the milestones
 * section of the form
 *
 * Bundled with the FormQuestion module.
 *
 * @see FormQuestion
 */
export default class FormList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            lastDate: Moment(this.props.plan.startDate),
            nameList: [],
            newMilestone: {
                name: "",
                percentage: 0,
                endDate: Moment(this.props.plan.startDate).add(6, "M"), // Set 6 months after the current date as the default end date
                description: ""
            },
            errors: {}
        }

        this.handleChange = this.handleChange.bind(this);
        this.validate = this.validate.bind(this);
        this.addMilestone = this.addMilestone.bind(this);
        this.removeMilestone = this.removeMilestone.bind(this);
    }

    /**
     * Handle the event when the value of a question has changed.
     *
     * @param e The handleChange event that contains the changes value.
     */
    handleChange(e) {
        let value = e.target.value;
        switch (e.target.name) {
            case "milestone-name":
                this.setState(prevState => {
                    return {newMilestone: {...prevState.newMilestone, name: value}}
                })
                break;
            case "milestone-end-date":
                this.setState(prevState => {
                    return {newMilestone: {...prevState.newMilestone, endDate: Moment(value)}}
                })
                break;
            case "milestone-description":
                if(value.length <= 200) {
                    this.setState(prevState => {
                        return {newMilestone: {...prevState.newMilestone, description: value}}
                    })
                }
                break;
            case "milestone-percentage":
                this.setState(prevState => {
                    return {newMilestone: {...prevState.newMilestone, percentage: Number(value)}}
                })
                break;
        }
    }

    /**
     * Validates the entered values of the new milestone
     * @returns {boolean} `true` if there are no errors and `false` if there are errors
     */
    validate() {
        let milestone = this.state.newMilestone;
        let errors = {};
        if(!milestone.name)
            errors.milestoneName = "The name of the milestone is required!";

        if(this.state.nameList.includes(milestone.name.toLowerCase()))
            errors.milestoneName = "The name of the milestone is already used within the investment plan! The name of a milestone must be unique within a investment plan.";

        if(this.props.totalPercentage + milestone.percentage > 100)
            errors.milestonePercentage = "All the payment percentages of all the milestones combined can't exceed 100%!";

        let regExp = new RegExp("^[1-9][0-9]?$|^100$|^0$");
        if(!regExp.test(milestone.percentage))
            errors.milestonePercentage = "The payment percentage must be between 0 and 100!";

        if(!milestone.endDate.isAfter(this.state.lastDate.isBefore(this.props.plan.startDate) ? this.props.plan.startDate : this.state.lastDate))
            errors.milestoneEndDate = "The end date of the new milestone must be AFTER the end date of the last milestone!";

        if(!milestone.description)
            errors.milestoneDescription = "The description of the milestone is required!";

        if(!milestone.length > 200)
            errors.milestoneDescription = "The description may not exceed the 200 characters!";

        this.setState({errors: errors});
        return Object.keys(errors).length === 0;
    }

    /**
     * Add a new milestone to the state.
     *
     * @param e The onClick event of the add milestone button.
     */
    addMilestone(e) {
        e.preventDefault();

        if(!this.validate())
            return;

        let milestonePercentage = this.state.newMilestone.percentage;

        this.setState(prevState => {
            return {
                lastDate: prevState.newMilestone.endDate,
                nameList: [...prevState.nameList, prevState.newMilestone.name.toLowerCase()],
                newMilestone: {
                    name: "",
                    percentage: 0,
                    endDate: Moment(prevState.newMilestone.endDate).add(1, "d"),
                    description: ""
                },
                errors: {}
            }
        });

        this.props.addMilestone(this.state.newMilestone, this.props.totalPercentage + milestonePercentage);
    }

    /**
     * Remove a new milestone from the state.
     *
     * @param e The onClick event of the remove icon.
     * @param milestone The milestone that has to be removed.
     */
    removeMilestone(e, milestone) {
        e.preventDefault();

        this.setState(prevState => {
            let milestones = this.props.plan.milestones;
            let index = milestones.indexOf(milestone);
            if(index > -1)
                milestones.splice(index, 1);
            let lastMilestoneDate = milestones.length > 0 ? milestones[milestones.length - 1].endDate :
                this.props.plan.startDate;

            let nameList = prevState.nameList;
            let removedNameIndex = nameList.indexOf(milestone.name.toLowerCase());
            if(removedNameIndex > -1)
                nameList.splice(removedNameIndex, 1);

            return {
                lastDate: lastMilestoneDate,
                nameList: nameList,
                newMilestone: {
                    name: "",
                    percentage: 0,
                    endDate: Moment(lastMilestoneDate).add(1, "d"),
                    description: ""
                },
                errors: {}
            }
        })

        this.props.removeMilestone(milestone, this.props.totalPercentage - milestone.percentage);
    }

    render() {
        return (
            <div className="form-milestones mt-3">
                {this.props.plan.milestones.map((milestone, index) => {
                    return (
                        <div key={index} className="card milestone-info">
                            <div className="card-body">
                                <span className="float-right delete-icon pt-2 pr-2" onClick={e => this.removeMilestone(e, milestone)}>
                                   <svg className="bi bi-trash-fill" width="1.2em" height="1.2em" viewBox="0 0 16 16"
                                        fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                            d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                    </svg>
                                </span>
                                <h5 className="card-title">Milestone {index + 1}</h5>
                                <h6 className="card-subtitle mb-2 text-muted">{milestone.name}</h6>
                                <p className="card-text">{milestone.description}</p>
                                <ul className="list-group list-group-flush mb-0">
                                    <li className="list-group-item">End date: {Moment(milestone.endDate).format("D MMMM YYYY")}</li>
                                    <li className="list-group-item">Payment percentage: {milestone.percentage}%</li>
                                </ul>
                            </div>
                        </div>
                    )})}

                <div className="card new-milestone">
                    <div className="card-header">
                        <span>Add a new milestone</span>
                    </div>
                    <div className="card-body">
                        <div className="form-group">
                            <label htmlFor="milestone-name">Milestone name</label>
                            <input className={"form-control " + (this.state.errors["milestoneName"] && "is-invalid")}
                                   id="milestone-name"
                                   name="milestone-name"
                                   value={this.state.newMilestone.name}
                                   placeholder="The name of the milestone" onChange={this.handleChange}/>
                            {this.state.errors["milestoneName"] && <div className="invalid-feedback">{this.state.errors["milestoneName"]}</div>}
                        </div>
                        <div className="form-group">
                            <label htmlFor="milestone-end-date">Milestone end date</label>
                            <input className={"form-control " + (this.state.errors["milestoneEndDate"] && "is-invalid")}
                                   id="milestone-end-date"
                                   name="milestone-end-date"
                                   type="date"
                                   value={Moment(this.state.newMilestone.endDate).format("YYYY-MM-DD")}
                                   min={Moment(!this.props.plan.milestones.length ? this.props.plan.startDate.format("YYYY-MM-DD") :
                                       this.props.plan.milestones[this.props.plan.milestones.length - 1].endDate)
                                       .add(1, "d").format("YYYY-MM-DD")}
                                   placeholder="The end date of the milestone" onChange={this.handleChange}/>
                            {this.state.errors["milestoneEndDate"] && <div className="invalid-feedback">{this.state.errors["milestoneEndDate"]}</div>}
                        </div>
                        <div className="form-group">
                            <label htmlFor="milestone-percentage">Milestone payment percentage</label>
                            <input className={"form-control " + (this.state.errors["milestonePercentage"] && "is-invalid")}
                                   id="milestone-percentage"
                                   name="milestone-percentage"
                                   type="number"
                                   min="0"
                                   max="100"
                                   value={this.state.newMilestone.percentage}
                                   onChange={this.handleChange}/>
                            {this.state.errors["milestonePercentage"] && <div className="invalid-feedback">{this.state.errors["milestonePercentage"]}</div>}
                        </div>
                        <div className="form-group milestone-description-form-group">
                            <label htmlFor="milestone-description">Milestone description</label>
                            <small className="form-text text-muted">The milestone description has a maximum 200 characters.</small>
                            <textarea className={"form-control " + (this.state.errors["milestoneDescription"] && "is-invalid")}
                                   id="milestone-description"
                                   name="milestone-description"
                                   value={this.state.newMilestone.description}
                                   maxLength="200"
                                   placeholder="The description of the milestone" onChange={this.handleChange}/>
                            <small className="desc-count float-right">({this.state.newMilestone.description.length}/200)</small>
                            {this.state.errors["milestoneDescription"] && <div className="invalid-feedback">{this.state.errors["milestoneDescription"]}</div>}
                        </div>
                        <button className="btn btn-success btn-block" onClick={this.addMilestone}>+ Add</button>
                    </div>
                </div>

            </div>
        );
    }
}