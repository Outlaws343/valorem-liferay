import React from "react";
import InvestmentPage from "./InvestmentPage";

export default class InvestmentList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            investments: [],
            investment: null,
            fetchingInvestments: true
        }

        this.goToList = this.goToList.bind(this);

        this.getInvestments = this.getInvestments.bind(this);
    }

    getInvestments(investments) {
        this.setState({investments: investments, fetchingInvestments: false});
    }

    componentDidMount() {
        Liferay.Service(
            '/investering.investering/get-investering-by-id',
            {
                participantId: 1
            },
            (obj) => this.getInvestments(obj)
        );
    }

    goToInvestmentPage(investment) {
        this.setState({investment: investment});
    }

    goToList() {
        this.setState({investment: null});
    }

    render() {
                if (this.state.investment !== null) {
                    return <InvestmentPage investment={this.state.investment}
                                           goToList={this.goToList}/>
                }

        return (<div>
                {this.state.investments.length === 1 ?
                    <h2>Investment</h2>
                    : <h2>Investments</h2>}

                <br/>

                {this.state.fetchingInvestments === false ?
                    <div>
                        {this.state.investments.length === 0 ?
                            <h2 style={{'textAlign': "center"}}>You haven't invested in any investment plans</h2> :
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Worth</th>
                                </tr>
                                </thead>

                                <tbody>
                                {this.state.investments.map((investment, index) =>
                                    <tr key={index} onClick={() => this.goToInvestmentPage(investment)}>
                                        <th scope="row">{index + 1}</th>
                                        <td>Temporary name</td>
                                        <td>{new Intl.NumberFormat('nl-NL', {
                                            style: 'currency',
                                            currency: 'EUR'
                                        }).format(investment.waarde)}</td>
                                    </tr>)}
                                </tbody>
                            </table>}
                    </div>
                    : <h2>Loading...</h2>}

            </div>
        )
    }
}