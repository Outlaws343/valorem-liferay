import React from "react";
import Milestones from "./MilestonePlan";
import Moment from "moment";

export default class InvestmentPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fetchingInvestmentplan: true,
            fetchingMilestones: true,
            investmentplan: null,
            milestones: [],
            investment: props.investment
        }

        this.addMilestones = this.addMilestones.bind(this);
        this.formatMilestoneData = this.formatMilestoneData.bind(this);
        this.getInvestmentplan = this.getInvestmentplan.bind(this);
    }

    getInvestmentplan(investmentplan) {
        this.setState({investmentplan: investmentplan, fetchingInvestmentplan: false});
    }

    addMilestones(milestones) {
        this.setState({milestones: milestones, fetchingMilestones: false});
    }

    componentDidMount() {
        Liferay.Service(
            '/investeringsplan.investeringsplan/get-investeringsplan-by-id',
            {
                investeringsplanId: this.state.investment.investeringsplanId
            },
            (investment) => this.getInvestmentplan(investment)
        );

        Liferay.Service(
            '/milestone.milestone/get-milestones-by-investment-plan-id',
            {
                investmentPlanId: this.state.investment.investeringsplanId
            },
            (milestones) => this.addMilestones(milestones)
        );
    }

    formatMilestoneData() {
        let data = [];
        let startDate = this.state.investmentplan.begindatum;

        this.state.milestones.map((milestone, index) => {
            let startDateFormat = new Date(startDate);
            let endDateFormat = new Date(milestone.einddatum);

            data.push(["Milestone " + (index + 1), milestone.naam, Moment(new Date(startDateFormat.getFullYear(), startDateFormat.getMonth(), startDateFormat.getDate())).toDate()
                , Moment(new Date(endDateFormat.getFullYear(), endDateFormat.getMonth(), endDateFormat.getDate())).toDate()]);
            startDate = milestone.einddatum;
        });

        return data;
    }

    render() {
        return (
            <div className="investment-plan-page">
                <div>
                    <svg className="bi bi-arrow-left-short" width="48" height="48" viewBox="0 0 16 16"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg" onClick={this.props.goToList}>
                        <path fillRule="evenodd"
                              d="M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z"/>
                        <path fillRule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </div>

                {this.state.fetchingMilestones === false && this.state.fetchingInvestmentplan === false ?
                    <div className="portlet-div">
                        <div className="flexboxhorizontal">
                            <div className="childcenter"><img
                                src="https://www.liferay.com/o/osb-www-theme/images/custom/open_graph_image.png"
                                alt="Logo"
                                width="300px" height="300px"/></div>
                            <div className="roundborder">
                                <div className="plan-info">
                                    <h1>{this.state.investmentplan.naam}</h1>
                                    <hr/>
                                    <h2>Status</h2>
                                    <p>{this.state.investmentplan.status}</p>
                                    <h2>Target</h2>
                                    <p>{new Intl.NumberFormat('nl-NL', {
                                        style: 'currency',
                                        currency: 'EUR'
                                    }).format(this.state.investmentplan.doel)}</p>
                                    <h2>Progress</h2>
                                    <p>{this.state.investmentplan.voortgang}%</p>
                                    <h2>Invested</h2>
                                    <p>{new Intl.NumberFormat('nl-NL', {
                                        style: 'currency',
                                        currency: 'EUR'
                                    }).format(this.state.investment.waarde)}</p>
                                </div>
                            </div>
                        </div>

                        <div className="flexboxhorizontal">
                            <div className="childborder milestone-description">
                                <h2>Beschrijving</h2>
                                {this.state.investmentplan.beschrijving}
                            </div>
                        </div>

                        <div className="flexboxhorizontal">
                            <Milestones milestones={this.formatMilestoneData()}/>
                        </div>
                    </div>
                    : <h2>Loading...</h2>}

            </div>)
    }
}
