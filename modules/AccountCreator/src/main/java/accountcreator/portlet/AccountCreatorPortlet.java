//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package accountcreator.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.servlet.http.HttpServletRequest;

@Component(
		immediate = true,
		property = {"com.liferay.portlet.display-category=category.AccountCreator", "com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=true", "javax.portlet.display-name=AccountCreation", "javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.name=accountcreation_AccountCreationPortlet", "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user"},
		service = {Portlet.class}
)
public class AccountCreatorPortlet extends MVCPortlet {
	@Reference
	private Portal _portal;

	public AccountCreatorPortlet() {
	}

	public void addEntry(ActionRequest actionRequest, ActionResponse response) throws PortalException {
		HttpServletRequest httpServletRequest = this._portal.getHttpServletRequest(actionRequest);
		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");

		String Voornaam = ParamUtil.getString(actionRequest, "First name");
		String Achternaam = ParamUtil.getString(actionRequest, "Last name");
		String Woonplaats = ParamUtil.getString(actionRequest, "City");
		String email = ParamUtil.getString(actionRequest, "Email");
		String password1 = ParamUtil.getString(actionRequest, "Password");
		String NaamOrganisatie = ParamUtil.getString(actionRequest, "Organisation name");
		String Kvk = ParamUtil.getString(actionRequest, "Kvk");
		String vog = ParamUtil.getString(actionRequest, "Vog");
		String BKR = ParamUtil.getString(actionRequest, "BKR");
		String Telefoonnummer = ParamUtil.getString(actionRequest, "Phone number");
		String BSN = ParamUtil.getString(actionRequest, "BSN");
		String Adres = ParamUtil.getString(actionRequest, "Address");
		String jobTitle = "Issuer";
		boolean isInvester = false;

		if (NaamOrganisatie.equals("")) {
			isInvester = true;
			jobTitle = "Participant";
		}

		long companyId = themeDisplay.getCompanyId();
		User user = null;
		try{
			user = UserLocalServiceUtil.addUser(
					20130, //creatorUserId
					companyId, //companyId
					false, //autoPassword
					password1, //password1
					password1, //password2
					false, //autoScreenName
					Voornaam, //screenName
					email, //emailAddress
					0, //facebookId
					null, //openId
					LocaleUtil.getDefault(), //locale
					Voornaam, //firstName
					"", //middleName
					Achternaam, //lastName
					0, //prefixId
					0, //suffixId
					false, //male
					0, //birthdayMonth
					1, //birthdayDay
					1970, //birthdayYear
					jobTitle, //jobTitle
					null, //groupIds
					null, //organizationIds
					null, //roleIds
					null, //userGroupIds
					false, //sendEmail
					null); //serviceContext
		}
		catch (Exception e){
		e.printStackTrace();
		}
		
		Role role = null;
		if (isInvester) {
			role = RoleLocalServiceUtil.getRole(companyId, "Participant");
		} else {
			role = RoleLocalServiceUtil.getRole(companyId, "Issuer");
		}

		if (role != null || user != null) {
			RoleLocalServiceUtil.addUserRole(user.getUserId(), role.getRoleId());

			try {
				if(user.getExpandoBridge().hasAttribute("woonplaats")){
					user.getExpandoBridge().setAttribute("woonplaats", Woonplaats);
				}
				else{
					user.getExpandoBridge().addAttribute("woonplaats");
					user.getExpandoBridge().setAttribute("woonplaats", Woonplaats);
				}

				if (isInvester) {
					if(user.getExpandoBridge().hasAttribute("tel")){
						user.getExpandoBridge().setAttribute("tel", Telefoonnummer);
					}
					else{
						user.getExpandoBridge().addAttribute("tel");
						user.getExpandoBridge().setAttribute("tel", Telefoonnummer);
					}

					if(user.getExpandoBridge().hasAttribute("bsn")){
						user.getExpandoBridge().setAttribute("bsn", BSN);
					}
					else{
						user.getExpandoBridge().addAttribute("bsn");
						user.getExpandoBridge().setAttribute("bsn", BSN);
					}

					if(user.getExpandoBridge().hasAttribute("adres")){
						user.getExpandoBridge().setAttribute("adres", Adres);
					}
					else{
						user.getExpandoBridge().addAttribute("adres");
						user.getExpandoBridge().setAttribute("adres", Adres);
					}

				} else {
					if(user.getExpandoBridge().hasAttribute("bedrijfsnaam")){
						user.getExpandoBridge().setAttribute("bedrijfsnaam", NaamOrganisatie);
					}
					else{
						user.getExpandoBridge().addAttribute("bedrijfsnaam");
						user.getExpandoBridge().setAttribute("bedrijfsnaam", NaamOrganisatie);
					}
					if(user.getExpandoBridge().hasAttribute("kvk")){
						user.getExpandoBridge().setAttribute("kvk", Kvk);
					}
					else{
						user.getExpandoBridge().addAttribute("kvk");
						user.getExpandoBridge().setAttribute("kvk", Kvk);
					}

					if(user.getExpandoBridge().hasAttribute("vog")){
						user.getExpandoBridge().setAttribute("vog", vog);
					}
					else{
						user.getExpandoBridge().addAttribute("vog");
						user.getExpandoBridge().setAttribute("vog", vog);
					}

					if(user.getExpandoBridge().hasAttribute("bkr")){
						user.getExpandoBridge().setAttribute("bkr", BKR);
					}
					else{
						user.getExpandoBridge().addAttribute("bkr");
						user.getExpandoBridge().setAttribute("bkr", BKR);
					}
				}
				UserLocalServiceUtil.updateUser(user);
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
}
