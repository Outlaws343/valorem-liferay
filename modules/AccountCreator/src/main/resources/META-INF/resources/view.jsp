<%@ include file="/init.jsp" %>

<portlet:actionURL name="addEntry" var="addDetailsURL"/>

<div id="first">

    <%
        String url = themeDisplay.getURLPortal() + themeDisplay.getURLCurrent();
        String[] parts = url.split("/");
        String lastSegment = parts[parts.length - 1];

        if (lastSegment.equals("inv")) {

    %>

    <h1> Participant </h1>
    <br/>

    <aui:form name="addEntry" action="${addDetailsURL}" method="post">

        <div class="container left">
            <aui:input name="First name" type="text" required="true"/>
            <aui:input name="Last name" type="text" required="true"/>
            <aui:input name="City" type="text" required="true"/>
            <aui:input name="Address" type="text" required="true"/>
            <aui:input name="BSN" type="number" required="true"/>
        </div>

        <div class="container right">
            <aui:input name="Email" type="text" required="true"/>
            <aui:input name="Phone number" type="number" required="true"/>
            <aui:input name="Password" type="password" required="true"/>
            <aui:input name="Password2" label="Confirm password" type="password" required="true">
                <aui:validator name="equalTo">'#<portlet:namespace/>Password'</aui:validator>
            </aui:input>
            <aui:button type="submit"></aui:button>

        </div>

    </aui:form>

    <%
    } else if (lastSegment.equals("ond")) {

    %>
    <h1> Issuer </h1>
    <br/>

    <aui:form name="addEntry" action="${addDetailsURL}" method="post">
        <div class="container left">
            <aui:input name="First name" type="text" required="true"/>
            <aui:input name="Last name" type="text" required="true"/>
            <aui:input name="Organisation name" type="text" required="true"/>
            <aui:input name="City" type="text" required="true"/>
            <aui:input name="Email" type="text" required="true"/>

        </div>

        <div class="container right">

            <aui:input name="Kvk" type="number" required="true"/>
            <aui:input name="Vog" type="number" required="true"/>
            <aui:input name="BKR" type="number" required="true"/>

            <aui:input name="Password" type="password" required="true"/>
            <aui:input name="Password2" label="Confirm password" type="password" required="true">
                <aui:validator name="equalTo">'#<portlet:namespace/>Password'</aui:validator>
            </aui:input>
            <aui:button type="submit"></aui:button>
        </div>
    </aui:form>

    <%} %>
</div>