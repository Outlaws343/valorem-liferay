/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import com.liferay.portal.kernel.model.BaseModel;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The base model interface for the Investering service. Represents a row in the &quot;Investering&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>database.model.impl.InvesteringModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>database.model.impl.InvesteringImpl</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Investering
 * @generated
 */
@ProviderType
public interface InvesteringModel extends BaseModel<Investering> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a investering model instance should use the {@link Investering} interface instead.
	 */

	/**
	 * Returns the primary key of this investering.
	 *
	 * @return the primary key of this investering
	 */
	public int getPrimaryKey();

	/**
	 * Sets the primary key of this investering.
	 *
	 * @param primaryKey the primary key of this investering
	 */
	public void setPrimaryKey(int primaryKey);

	/**
	 * Returns the investerings ID of this investering.
	 *
	 * @return the investerings ID of this investering
	 */
	public int getInvesteringsId();

	/**
	 * Sets the investerings ID of this investering.
	 *
	 * @param investeringsId the investerings ID of this investering
	 */
	public void setInvesteringsId(int investeringsId);

	/**
	 * Returns the investeringsplan ID of this investering.
	 *
	 * @return the investeringsplan ID of this investering
	 */
	public int getInvesteringsplanId();

	/**
	 * Sets the investeringsplan ID of this investering.
	 *
	 * @param investeringsplanId the investeringsplan ID of this investering
	 */
	public void setInvesteringsplanId(int investeringsplanId);

	/**
	 * Returns the participant ID of this investering.
	 *
	 * @return the participant ID of this investering
	 */
	public int getParticipantId();

	/**
	 * Sets the participant ID of this investering.
	 *
	 * @param participantId the participant ID of this investering
	 */
	public void setParticipantId(int participantId);

	/**
	 * Returns the sto ID of this investering.
	 *
	 * @return the sto ID of this investering
	 */
	public int getSTOId();

	/**
	 * Sets the sto ID of this investering.
	 *
	 * @param STOId the sto ID of this investering
	 */
	public void setSTOId(int STOId);

	/**
	 * Returns the waarde of this investering.
	 *
	 * @return the waarde of this investering
	 */
	public int getWaarde();

	/**
	 * Sets the waarde of this investering.
	 *
	 * @param waarde the waarde of this investering
	 */
	public void setWaarde(int waarde);

}