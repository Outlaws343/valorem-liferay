/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Issuer}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Issuer
 * @generated
 */
public class IssuerWrapper
	extends BaseModelWrapper<Issuer> implements Issuer, ModelWrapper<Issuer> {

	public IssuerWrapper(Issuer issuer) {
		super(issuer);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("issuerId", getIssuerId());
		attributes.put("voornaam", getVoornaam());
		attributes.put("achternaam", getAchternaam());
		attributes.put("bedrijfsnaam", getBedrijfsnaam());
		attributes.put("woonplaats", getWoonplaats());
		attributes.put("email", getEmail());
		attributes.put("kvk", getKvk());
		attributes.put("vog", getVog());
		attributes.put("bkr", getBkr());
		attributes.put("tel_nummer", getTel_nummer());
		attributes.put("beschrijving", getBeschrijving());
		attributes.put("gecontroleerd", isGecontroleerd());
		attributes.put("geaccepteerd", isGeaccepteerd());
		attributes.put("reden", getReden());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer issuerId = (Integer)attributes.get("issuerId");

		if (issuerId != null) {
			setIssuerId(issuerId);
		}

		String voornaam = (String)attributes.get("voornaam");

		if (voornaam != null) {
			setVoornaam(voornaam);
		}

		String achternaam = (String)attributes.get("achternaam");

		if (achternaam != null) {
			setAchternaam(achternaam);
		}

		String bedrijfsnaam = (String)attributes.get("bedrijfsnaam");

		if (bedrijfsnaam != null) {
			setBedrijfsnaam(bedrijfsnaam);
		}

		String woonplaats = (String)attributes.get("woonplaats");

		if (woonplaats != null) {
			setWoonplaats(woonplaats);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		Integer kvk = (Integer)attributes.get("kvk");

		if (kvk != null) {
			setKvk(kvk);
		}

		Integer vog = (Integer)attributes.get("vog");

		if (vog != null) {
			setVog(vog);
		}

		Integer bkr = (Integer)attributes.get("bkr");

		if (bkr != null) {
			setBkr(bkr);
		}

		String tel_nummer = (String)attributes.get("tel_nummer");

		if (tel_nummer != null) {
			setTel_nummer(tel_nummer);
		}

		String beschrijving = (String)attributes.get("beschrijving");

		if (beschrijving != null) {
			setBeschrijving(beschrijving);
		}

		Boolean gecontroleerd = (Boolean)attributes.get("gecontroleerd");

		if (gecontroleerd != null) {
			setGecontroleerd(gecontroleerd);
		}

		Boolean geaccepteerd = (Boolean)attributes.get("geaccepteerd");

		if (geaccepteerd != null) {
			setGeaccepteerd(geaccepteerd);
		}

		String reden = (String)attributes.get("reden");

		if (reden != null) {
			setReden(reden);
		}
	}

	/**
	 * Returns the achternaam of this issuer.
	 *
	 * @return the achternaam of this issuer
	 */
	@Override
	public String getAchternaam() {
		return model.getAchternaam();
	}

	/**
	 * Returns the bedrijfsnaam of this issuer.
	 *
	 * @return the bedrijfsnaam of this issuer
	 */
	@Override
	public String getBedrijfsnaam() {
		return model.getBedrijfsnaam();
	}

	/**
	 * Returns the beschrijving of this issuer.
	 *
	 * @return the beschrijving of this issuer
	 */
	@Override
	public String getBeschrijving() {
		return model.getBeschrijving();
	}

	/**
	 * Returns the bkr of this issuer.
	 *
	 * @return the bkr of this issuer
	 */
	@Override
	public int getBkr() {
		return model.getBkr();
	}

	/**
	 * Returns the email of this issuer.
	 *
	 * @return the email of this issuer
	 */
	@Override
	public String getEmail() {
		return model.getEmail();
	}

	/**
	 * Returns the geaccepteerd of this issuer.
	 *
	 * @return the geaccepteerd of this issuer
	 */
	@Override
	public boolean getGeaccepteerd() {
		return model.getGeaccepteerd();
	}

	/**
	 * Returns the gecontroleerd of this issuer.
	 *
	 * @return the gecontroleerd of this issuer
	 */
	@Override
	public boolean getGecontroleerd() {
		return model.getGecontroleerd();
	}

	/**
	 * Returns the issuer ID of this issuer.
	 *
	 * @return the issuer ID of this issuer
	 */
	@Override
	public int getIssuerId() {
		return model.getIssuerId();
	}

	/**
	 * Returns the kvk of this issuer.
	 *
	 * @return the kvk of this issuer
	 */
	@Override
	public int getKvk() {
		return model.getKvk();
	}

	/**
	 * Returns the primary key of this issuer.
	 *
	 * @return the primary key of this issuer
	 */
	@Override
	public int getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the reden of this issuer.
	 *
	 * @return the reden of this issuer
	 */
	@Override
	public String getReden() {
		return model.getReden();
	}

	/**
	 * Returns the tel_nummer of this issuer.
	 *
	 * @return the tel_nummer of this issuer
	 */
	@Override
	public String getTel_nummer() {
		return model.getTel_nummer();
	}

	/**
	 * Returns the vog of this issuer.
	 *
	 * @return the vog of this issuer
	 */
	@Override
	public int getVog() {
		return model.getVog();
	}

	/**
	 * Returns the voornaam of this issuer.
	 *
	 * @return the voornaam of this issuer
	 */
	@Override
	public String getVoornaam() {
		return model.getVoornaam();
	}

	/**
	 * Returns the woonplaats of this issuer.
	 *
	 * @return the woonplaats of this issuer
	 */
	@Override
	public String getWoonplaats() {
		return model.getWoonplaats();
	}

	/**
	 * Returns <code>true</code> if this issuer is geaccepteerd.
	 *
	 * @return <code>true</code> if this issuer is geaccepteerd; <code>false</code> otherwise
	 */
	@Override
	public boolean isGeaccepteerd() {
		return model.isGeaccepteerd();
	}

	/**
	 * Returns <code>true</code> if this issuer is gecontroleerd.
	 *
	 * @return <code>true</code> if this issuer is gecontroleerd; <code>false</code> otherwise
	 */
	@Override
	public boolean isGecontroleerd() {
		return model.isGecontroleerd();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the achternaam of this issuer.
	 *
	 * @param achternaam the achternaam of this issuer
	 */
	@Override
	public void setAchternaam(String achternaam) {
		model.setAchternaam(achternaam);
	}

	/**
	 * Sets the bedrijfsnaam of this issuer.
	 *
	 * @param bedrijfsnaam the bedrijfsnaam of this issuer
	 */
	@Override
	public void setBedrijfsnaam(String bedrijfsnaam) {
		model.setBedrijfsnaam(bedrijfsnaam);
	}

	/**
	 * Sets the beschrijving of this issuer.
	 *
	 * @param beschrijving the beschrijving of this issuer
	 */
	@Override
	public void setBeschrijving(String beschrijving) {
		model.setBeschrijving(beschrijving);
	}

	/**
	 * Sets the bkr of this issuer.
	 *
	 * @param bkr the bkr of this issuer
	 */
	@Override
	public void setBkr(int bkr) {
		model.setBkr(bkr);
	}

	/**
	 * Sets the email of this issuer.
	 *
	 * @param email the email of this issuer
	 */
	@Override
	public void setEmail(String email) {
		model.setEmail(email);
	}

	/**
	 * Sets whether this issuer is geaccepteerd.
	 *
	 * @param geaccepteerd the geaccepteerd of this issuer
	 */
	@Override
	public void setGeaccepteerd(boolean geaccepteerd) {
		model.setGeaccepteerd(geaccepteerd);
	}

	/**
	 * Sets whether this issuer is gecontroleerd.
	 *
	 * @param gecontroleerd the gecontroleerd of this issuer
	 */
	@Override
	public void setGecontroleerd(boolean gecontroleerd) {
		model.setGecontroleerd(gecontroleerd);
	}

	/**
	 * Sets the issuer ID of this issuer.
	 *
	 * @param issuerId the issuer ID of this issuer
	 */
	@Override
	public void setIssuerId(int issuerId) {
		model.setIssuerId(issuerId);
	}

	/**
	 * Sets the kvk of this issuer.
	 *
	 * @param kvk the kvk of this issuer
	 */
	@Override
	public void setKvk(int kvk) {
		model.setKvk(kvk);
	}

	/**
	 * Sets the primary key of this issuer.
	 *
	 * @param primaryKey the primary key of this issuer
	 */
	@Override
	public void setPrimaryKey(int primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the reden of this issuer.
	 *
	 * @param reden the reden of this issuer
	 */
	@Override
	public void setReden(String reden) {
		model.setReden(reden);
	}

	/**
	 * Sets the tel_nummer of this issuer.
	 *
	 * @param tel_nummer the tel_nummer of this issuer
	 */
	@Override
	public void setTel_nummer(String tel_nummer) {
		model.setTel_nummer(tel_nummer);
	}

	/**
	 * Sets the vog of this issuer.
	 *
	 * @param vog the vog of this issuer
	 */
	@Override
	public void setVog(int vog) {
		model.setVog(vog);
	}

	/**
	 * Sets the voornaam of this issuer.
	 *
	 * @param voornaam the voornaam of this issuer
	 */
	@Override
	public void setVoornaam(String voornaam) {
		model.setVoornaam(voornaam);
	}

	/**
	 * Sets the woonplaats of this issuer.
	 *
	 * @param woonplaats the woonplaats of this issuer
	 */
	@Override
	public void setWoonplaats(String woonplaats) {
		model.setWoonplaats(woonplaats);
	}

	@Override
	protected IssuerWrapper wrap(Issuer issuer) {
		return new IssuerWrapper(issuer);
	}

}