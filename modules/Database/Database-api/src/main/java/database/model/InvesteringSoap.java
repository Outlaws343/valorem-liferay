/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link database.service.http.InvesteringServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class InvesteringSoap implements Serializable {

	public static InvesteringSoap toSoapModel(Investering model) {
		InvesteringSoap soapModel = new InvesteringSoap();

		soapModel.setInvesteringsId(model.getInvesteringsId());
		soapModel.setInvesteringsplanId(model.getInvesteringsplanId());
		soapModel.setParticipantId(model.getParticipantId());
		soapModel.setSTOId(model.getSTOId());
		soapModel.setWaarde(model.getWaarde());

		return soapModel;
	}

	public static InvesteringSoap[] toSoapModels(Investering[] models) {
		InvesteringSoap[] soapModels = new InvesteringSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static InvesteringSoap[][] toSoapModels(Investering[][] models) {
		InvesteringSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new InvesteringSoap[models.length][models[0].length];
		}
		else {
			soapModels = new InvesteringSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static InvesteringSoap[] toSoapModels(List<Investering> models) {
		List<InvesteringSoap> soapModels = new ArrayList<InvesteringSoap>(
			models.size());

		for (Investering model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new InvesteringSoap[soapModels.size()]);
	}

	public InvesteringSoap() {
	}

	public int getPrimaryKey() {
		return _investeringsId;
	}

	public void setPrimaryKey(int pk) {
		setInvesteringsId(pk);
	}

	public int getInvesteringsId() {
		return _investeringsId;
	}

	public void setInvesteringsId(int investeringsId) {
		_investeringsId = investeringsId;
	}

	public int getInvesteringsplanId() {
		return _investeringsplanId;
	}

	public void setInvesteringsplanId(int investeringsplanId) {
		_investeringsplanId = investeringsplanId;
	}

	public int getParticipantId() {
		return _participantId;
	}

	public void setParticipantId(int participantId) {
		_participantId = participantId;
	}

	public int getSTOId() {
		return _STOId;
	}

	public void setSTOId(int STOId) {
		_STOId = STOId;
	}

	public int getWaarde() {
		return _waarde;
	}

	public void setWaarde(int waarde) {
		_waarde = waarde;
	}

	private int _investeringsId;
	private int _investeringsplanId;
	private int _participantId;
	private int _STOId;
	private int _waarde;

}