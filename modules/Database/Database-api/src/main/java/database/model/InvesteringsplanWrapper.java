/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Investeringsplan}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Investeringsplan
 * @generated
 */
public class InvesteringsplanWrapper
	extends BaseModelWrapper<Investeringsplan>
	implements Investeringsplan, ModelWrapper<Investeringsplan> {

	public InvesteringsplanWrapper(Investeringsplan investeringsplan) {
		super(investeringsplan);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("investeringsplanId", getInvesteringsplanId());
		attributes.put("issuerId", getIssuerId());
		attributes.put("naam", getNaam());
		attributes.put("status", getStatus());
		attributes.put("voortgang", getVoortgang());
		attributes.put("doel", getDoel());
		attributes.put("beschrijving", getBeschrijving());
		attributes.put("begindatum", getBegindatum());
		attributes.put("gecontroleerd", isGecontroleerd());
		attributes.put("geaccepteerd", isGeaccepteerd());
		attributes.put("reden", getReden());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer investeringsplanId = (Integer)attributes.get(
			"investeringsplanId");

		if (investeringsplanId != null) {
			setInvesteringsplanId(investeringsplanId);
		}

		Integer issuerId = (Integer)attributes.get("issuerId");

		if (issuerId != null) {
			setIssuerId(issuerId);
		}

		String naam = (String)attributes.get("naam");

		if (naam != null) {
			setNaam(naam);
		}

		String status = (String)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Integer voortgang = (Integer)attributes.get("voortgang");

		if (voortgang != null) {
			setVoortgang(voortgang);
		}

		Integer doel = (Integer)attributes.get("doel");

		if (doel != null) {
			setDoel(doel);
		}

		String beschrijving = (String)attributes.get("beschrijving");

		if (beschrijving != null) {
			setBeschrijving(beschrijving);
		}

		Date begindatum = (Date)attributes.get("begindatum");

		if (begindatum != null) {
			setBegindatum(begindatum);
		}

		Boolean gecontroleerd = (Boolean)attributes.get("gecontroleerd");

		if (gecontroleerd != null) {
			setGecontroleerd(gecontroleerd);
		}

		Boolean geaccepteerd = (Boolean)attributes.get("geaccepteerd");

		if (geaccepteerd != null) {
			setGeaccepteerd(geaccepteerd);
		}

		String reden = (String)attributes.get("reden");

		if (reden != null) {
			setReden(reden);
		}
	}

	/**
	 * Returns the begindatum of this investeringsplan.
	 *
	 * @return the begindatum of this investeringsplan
	 */
	@Override
	public Date getBegindatum() {
		return model.getBegindatum();
	}

	/**
	 * Returns the beschrijving of this investeringsplan.
	 *
	 * @return the beschrijving of this investeringsplan
	 */
	@Override
	public String getBeschrijving() {
		return model.getBeschrijving();
	}

	/**
	 * Returns the doel of this investeringsplan.
	 *
	 * @return the doel of this investeringsplan
	 */
	@Override
	public int getDoel() {
		return model.getDoel();
	}

	/**
	 * Returns the geaccepteerd of this investeringsplan.
	 *
	 * @return the geaccepteerd of this investeringsplan
	 */
	@Override
	public boolean getGeaccepteerd() {
		return model.getGeaccepteerd();
	}

	/**
	 * Returns the gecontroleerd of this investeringsplan.
	 *
	 * @return the gecontroleerd of this investeringsplan
	 */
	@Override
	public boolean getGecontroleerd() {
		return model.getGecontroleerd();
	}

	/**
	 * Returns the investeringsplan ID of this investeringsplan.
	 *
	 * @return the investeringsplan ID of this investeringsplan
	 */
	@Override
	public int getInvesteringsplanId() {
		return model.getInvesteringsplanId();
	}

	/**
	 * Returns the issuer ID of this investeringsplan.
	 *
	 * @return the issuer ID of this investeringsplan
	 */
	@Override
	public int getIssuerId() {
		return model.getIssuerId();
	}

	/**
	 * Returns the naam of this investeringsplan.
	 *
	 * @return the naam of this investeringsplan
	 */
	@Override
	public String getNaam() {
		return model.getNaam();
	}

	/**
	 * Returns the primary key of this investeringsplan.
	 *
	 * @return the primary key of this investeringsplan
	 */
	@Override
	public int getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the reden of this investeringsplan.
	 *
	 * @return the reden of this investeringsplan
	 */
	@Override
	public String getReden() {
		return model.getReden();
	}

	/**
	 * Returns the status of this investeringsplan.
	 *
	 * @return the status of this investeringsplan
	 */
	@Override
	public String getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the voortgang of this investeringsplan.
	 *
	 * @return the voortgang of this investeringsplan
	 */
	@Override
	public int getVoortgang() {
		return model.getVoortgang();
	}

	/**
	 * Returns <code>true</code> if this investeringsplan is geaccepteerd.
	 *
	 * @return <code>true</code> if this investeringsplan is geaccepteerd; <code>false</code> otherwise
	 */
	@Override
	public boolean isGeaccepteerd() {
		return model.isGeaccepteerd();
	}

	/**
	 * Returns <code>true</code> if this investeringsplan is gecontroleerd.
	 *
	 * @return <code>true</code> if this investeringsplan is gecontroleerd; <code>false</code> otherwise
	 */
	@Override
	public boolean isGecontroleerd() {
		return model.isGecontroleerd();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the begindatum of this investeringsplan.
	 *
	 * @param begindatum the begindatum of this investeringsplan
	 */
	@Override
	public void setBegindatum(Date begindatum) {
		model.setBegindatum(begindatum);
	}

	/**
	 * Sets the beschrijving of this investeringsplan.
	 *
	 * @param beschrijving the beschrijving of this investeringsplan
	 */
	@Override
	public void setBeschrijving(String beschrijving) {
		model.setBeschrijving(beschrijving);
	}

	/**
	 * Sets the doel of this investeringsplan.
	 *
	 * @param doel the doel of this investeringsplan
	 */
	@Override
	public void setDoel(int doel) {
		model.setDoel(doel);
	}

	/**
	 * Sets whether this investeringsplan is geaccepteerd.
	 *
	 * @param geaccepteerd the geaccepteerd of this investeringsplan
	 */
	@Override
	public void setGeaccepteerd(boolean geaccepteerd) {
		model.setGeaccepteerd(geaccepteerd);
	}

	/**
	 * Sets whether this investeringsplan is gecontroleerd.
	 *
	 * @param gecontroleerd the gecontroleerd of this investeringsplan
	 */
	@Override
	public void setGecontroleerd(boolean gecontroleerd) {
		model.setGecontroleerd(gecontroleerd);
	}

	/**
	 * Sets the investeringsplan ID of this investeringsplan.
	 *
	 * @param investeringsplanId the investeringsplan ID of this investeringsplan
	 */
	@Override
	public void setInvesteringsplanId(int investeringsplanId) {
		model.setInvesteringsplanId(investeringsplanId);
	}

	/**
	 * Sets the issuer ID of this investeringsplan.
	 *
	 * @param issuerId the issuer ID of this investeringsplan
	 */
	@Override
	public void setIssuerId(int issuerId) {
		model.setIssuerId(issuerId);
	}

	/**
	 * Sets the naam of this investeringsplan.
	 *
	 * @param naam the naam of this investeringsplan
	 */
	@Override
	public void setNaam(String naam) {
		model.setNaam(naam);
	}

	/**
	 * Sets the primary key of this investeringsplan.
	 *
	 * @param primaryKey the primary key of this investeringsplan
	 */
	@Override
	public void setPrimaryKey(int primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the reden of this investeringsplan.
	 *
	 * @param reden the reden of this investeringsplan
	 */
	@Override
	public void setReden(String reden) {
		model.setReden(reden);
	}

	/**
	 * Sets the status of this investeringsplan.
	 *
	 * @param status the status of this investeringsplan
	 */
	@Override
	public void setStatus(String status) {
		model.setStatus(status);
	}

	/**
	 * Sets the voortgang of this investeringsplan.
	 *
	 * @param voortgang the voortgang of this investeringsplan
	 */
	@Override
	public void setVoortgang(int voortgang) {
		model.setVoortgang(voortgang);
	}

	@Override
	protected InvesteringsplanWrapper wrap(Investeringsplan investeringsplan) {
		return new InvesteringsplanWrapper(investeringsplan);
	}

}