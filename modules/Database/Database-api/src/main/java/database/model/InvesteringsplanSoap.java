/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link database.service.http.InvesteringsplanServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class InvesteringsplanSoap implements Serializable {

	public static InvesteringsplanSoap toSoapModel(Investeringsplan model) {
		InvesteringsplanSoap soapModel = new InvesteringsplanSoap();

		soapModel.setInvesteringsplanId(model.getInvesteringsplanId());
		soapModel.setIssuerId(model.getIssuerId());
		soapModel.setNaam(model.getNaam());
		soapModel.setStatus(model.getStatus());
		soapModel.setVoortgang(model.getVoortgang());
		soapModel.setDoel(model.getDoel());
		soapModel.setBeschrijving(model.getBeschrijving());
		soapModel.setBegindatum(model.getBegindatum());
		soapModel.setGecontroleerd(model.isGecontroleerd());
		soapModel.setGeaccepteerd(model.isGeaccepteerd());
		soapModel.setReden(model.getReden());

		return soapModel;
	}

	public static InvesteringsplanSoap[] toSoapModels(
		Investeringsplan[] models) {

		InvesteringsplanSoap[] soapModels =
			new InvesteringsplanSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static InvesteringsplanSoap[][] toSoapModels(
		Investeringsplan[][] models) {

		InvesteringsplanSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new InvesteringsplanSoap[models.length][models[0].length];
		}
		else {
			soapModels = new InvesteringsplanSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static InvesteringsplanSoap[] toSoapModels(
		List<Investeringsplan> models) {

		List<InvesteringsplanSoap> soapModels =
			new ArrayList<InvesteringsplanSoap>(models.size());

		for (Investeringsplan model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new InvesteringsplanSoap[soapModels.size()]);
	}

	public InvesteringsplanSoap() {
	}

	public int getPrimaryKey() {
		return _investeringsplanId;
	}

	public void setPrimaryKey(int pk) {
		setInvesteringsplanId(pk);
	}

	public int getInvesteringsplanId() {
		return _investeringsplanId;
	}

	public void setInvesteringsplanId(int investeringsplanId) {
		_investeringsplanId = investeringsplanId;
	}

	public int getIssuerId() {
		return _issuerId;
	}

	public void setIssuerId(int issuerId) {
		_issuerId = issuerId;
	}

	public String getNaam() {
		return _naam;
	}

	public void setNaam(String naam) {
		_naam = naam;
	}

	public String getStatus() {
		return _status;
	}

	public void setStatus(String status) {
		_status = status;
	}

	public int getVoortgang() {
		return _voortgang;
	}

	public void setVoortgang(int voortgang) {
		_voortgang = voortgang;
	}

	public int getDoel() {
		return _doel;
	}

	public void setDoel(int doel) {
		_doel = doel;
	}

	public String getBeschrijving() {
		return _beschrijving;
	}

	public void setBeschrijving(String beschrijving) {
		_beschrijving = beschrijving;
	}

	public Date getBegindatum() {
		return _begindatum;
	}

	public void setBegindatum(Date begindatum) {
		_begindatum = begindatum;
	}

	public boolean getGecontroleerd() {
		return _gecontroleerd;
	}

	public boolean isGecontroleerd() {
		return _gecontroleerd;
	}

	public void setGecontroleerd(boolean gecontroleerd) {
		_gecontroleerd = gecontroleerd;
	}

	public boolean getGeaccepteerd() {
		return _geaccepteerd;
	}

	public boolean isGeaccepteerd() {
		return _geaccepteerd;
	}

	public void setGeaccepteerd(boolean geaccepteerd) {
		_geaccepteerd = geaccepteerd;
	}

	public String getReden() {
		return _reden;
	}

	public void setReden(String reden) {
		_reden = reden;
	}

	private int _investeringsplanId;
	private int _issuerId;
	private String _naam;
	private String _status;
	private int _voortgang;
	private int _doel;
	private String _beschrijving;
	private Date _begindatum;
	private boolean _gecontroleerd;
	private boolean _geaccepteerd;
	private String _reden;

}