/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link database.service.http.ParticipantServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ParticipantSoap implements Serializable {

	public static ParticipantSoap toSoapModel(Participant model) {
		ParticipantSoap soapModel = new ParticipantSoap();

		soapModel.setParticipantId(model.getParticipantId());
		soapModel.setVoornaam(model.getVoornaam());
		soapModel.setAchternaam(model.getAchternaam());
		soapModel.setWoonplaats(model.getWoonplaats());
		soapModel.setAdres(model.getAdres());
		soapModel.setBsn(model.getBsn());
		soapModel.setEmail(model.getEmail());
		soapModel.setTel_nummer(model.getTel_nummer());
		soapModel.setVerificatie(model.isVerificatie());

		return soapModel;
	}

	public static ParticipantSoap[] toSoapModels(Participant[] models) {
		ParticipantSoap[] soapModels = new ParticipantSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ParticipantSoap[][] toSoapModels(Participant[][] models) {
		ParticipantSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ParticipantSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ParticipantSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ParticipantSoap[] toSoapModels(List<Participant> models) {
		List<ParticipantSoap> soapModels = new ArrayList<ParticipantSoap>(
			models.size());

		for (Participant model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ParticipantSoap[soapModels.size()]);
	}

	public ParticipantSoap() {
	}

	public int getPrimaryKey() {
		return _participantId;
	}

	public void setPrimaryKey(int pk) {
		setParticipantId(pk);
	}

	public int getParticipantId() {
		return _participantId;
	}

	public void setParticipantId(int participantId) {
		_participantId = participantId;
	}

	public String getVoornaam() {
		return _voornaam;
	}

	public void setVoornaam(String voornaam) {
		_voornaam = voornaam;
	}

	public String getAchternaam() {
		return _achternaam;
	}

	public void setAchternaam(String achternaam) {
		_achternaam = achternaam;
	}

	public String getWoonplaats() {
		return _woonplaats;
	}

	public void setWoonplaats(String woonplaats) {
		_woonplaats = woonplaats;
	}

	public String getAdres() {
		return _adres;
	}

	public void setAdres(String adres) {
		_adres = adres;
	}

	public int getBsn() {
		return _bsn;
	}

	public void setBsn(int bsn) {
		_bsn = bsn;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getTel_nummer() {
		return _tel_nummer;
	}

	public void setTel_nummer(String tel_nummer) {
		_tel_nummer = tel_nummer;
	}

	public boolean getVerificatie() {
		return _verificatie;
	}

	public boolean isVerificatie() {
		return _verificatie;
	}

	public void setVerificatie(boolean verificatie) {
		_verificatie = verificatie;
	}

	private int _participantId;
	private String _voornaam;
	private String _achternaam;
	private String _woonplaats;
	private String _adres;
	private int _bsn;
	private String _email;
	private String _tel_nummer;
	private boolean _verificatie;

}