/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link database.service.http.ControleServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ControleSoap implements Serializable {

	public static ControleSoap toSoapModel(Controle model) {
		ControleSoap soapModel = new ControleSoap();

		soapModel.setIssuerId(model.getIssuerId());
		soapModel.setAkkoord(model.isAkkoord());
		soapModel.setReden(model.getReden());

		return soapModel;
	}

	public static ControleSoap[] toSoapModels(Controle[] models) {
		ControleSoap[] soapModels = new ControleSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ControleSoap[][] toSoapModels(Controle[][] models) {
		ControleSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ControleSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ControleSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ControleSoap[] toSoapModels(List<Controle> models) {
		List<ControleSoap> soapModels = new ArrayList<ControleSoap>(
			models.size());

		for (Controle model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ControleSoap[soapModels.size()]);
	}

	public ControleSoap() {
	}

	public int getPrimaryKey() {
		return _issuerId;
	}

	public void setPrimaryKey(int pk) {
		setIssuerId(pk);
	}

	public int getIssuerId() {
		return _issuerId;
	}

	public void setIssuerId(int issuerId) {
		_issuerId = issuerId;
	}

	public boolean getAkkoord() {
		return _akkoord;
	}

	public boolean isAkkoord() {
		return _akkoord;
	}

	public void setAkkoord(boolean akkoord) {
		_akkoord = akkoord;
	}

	public String getReden() {
		return _reden;
	}

	public void setReden(String reden) {
		_reden = reden;
	}

	private int _issuerId;
	private boolean _akkoord;
	private String _reden;

}