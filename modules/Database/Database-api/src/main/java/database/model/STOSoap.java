/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link database.service.http.STOServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class STOSoap implements Serializable {

	public static STOSoap toSoapModel(STO model) {
		STOSoap soapModel = new STOSoap();

		soapModel.setSTOId(model.getSTOId());
		soapModel.setWaarde(model.getWaarde());
		soapModel.setParticipantId(model.getParticipantId());

		return soapModel;
	}

	public static STOSoap[] toSoapModels(STO[] models) {
		STOSoap[] soapModels = new STOSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static STOSoap[][] toSoapModels(STO[][] models) {
		STOSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new STOSoap[models.length][models[0].length];
		}
		else {
			soapModels = new STOSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static STOSoap[] toSoapModels(List<STO> models) {
		List<STOSoap> soapModels = new ArrayList<STOSoap>(models.size());

		for (STO model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new STOSoap[soapModels.size()]);
	}

	public STOSoap() {
	}

	public int getPrimaryKey() {
		return _STOId;
	}

	public void setPrimaryKey(int pk) {
		setSTOId(pk);
	}

	public int getSTOId() {
		return _STOId;
	}

	public void setSTOId(int STOId) {
		_STOId = STOId;
	}

	public String getWaarde() {
		return _waarde;
	}

	public void setWaarde(String waarde) {
		_waarde = waarde;
	}

	public int getParticipantId() {
		return _participantId;
	}

	public void setParticipantId(int participantId) {
		_participantId = participantId;
	}

	private int _STOId;
	private String _waarde;
	private int _participantId;

}