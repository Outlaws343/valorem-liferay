/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link database.service.http.IssuerServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class IssuerSoap implements Serializable {

	public static IssuerSoap toSoapModel(Issuer model) {
		IssuerSoap soapModel = new IssuerSoap();

		soapModel.setIssuerId(model.getIssuerId());
		soapModel.setVoornaam(model.getVoornaam());
		soapModel.setAchternaam(model.getAchternaam());
		soapModel.setBedrijfsnaam(model.getBedrijfsnaam());
		soapModel.setWoonplaats(model.getWoonplaats());
		soapModel.setEmail(model.getEmail());
		soapModel.setKvk(model.getKvk());
		soapModel.setVog(model.getVog());
		soapModel.setBkr(model.getBkr());
		soapModel.setTel_nummer(model.getTel_nummer());
		soapModel.setBeschrijving(model.getBeschrijving());
		soapModel.setGecontroleerd(model.isGecontroleerd());
		soapModel.setGeaccepteerd(model.isGeaccepteerd());
		soapModel.setReden(model.getReden());

		return soapModel;
	}

	public static IssuerSoap[] toSoapModels(Issuer[] models) {
		IssuerSoap[] soapModels = new IssuerSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static IssuerSoap[][] toSoapModels(Issuer[][] models) {
		IssuerSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new IssuerSoap[models.length][models[0].length];
		}
		else {
			soapModels = new IssuerSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static IssuerSoap[] toSoapModels(List<Issuer> models) {
		List<IssuerSoap> soapModels = new ArrayList<IssuerSoap>(models.size());

		for (Issuer model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new IssuerSoap[soapModels.size()]);
	}

	public IssuerSoap() {
	}

	public int getPrimaryKey() {
		return _issuerId;
	}

	public void setPrimaryKey(int pk) {
		setIssuerId(pk);
	}

	public int getIssuerId() {
		return _issuerId;
	}

	public void setIssuerId(int issuerId) {
		_issuerId = issuerId;
	}

	public String getVoornaam() {
		return _voornaam;
	}

	public void setVoornaam(String voornaam) {
		_voornaam = voornaam;
	}

	public String getAchternaam() {
		return _achternaam;
	}

	public void setAchternaam(String achternaam) {
		_achternaam = achternaam;
	}

	public String getBedrijfsnaam() {
		return _bedrijfsnaam;
	}

	public void setBedrijfsnaam(String bedrijfsnaam) {
		_bedrijfsnaam = bedrijfsnaam;
	}

	public String getWoonplaats() {
		return _woonplaats;
	}

	public void setWoonplaats(String woonplaats) {
		_woonplaats = woonplaats;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public int getKvk() {
		return _kvk;
	}

	public void setKvk(int kvk) {
		_kvk = kvk;
	}

	public int getVog() {
		return _vog;
	}

	public void setVog(int vog) {
		_vog = vog;
	}

	public int getBkr() {
		return _bkr;
	}

	public void setBkr(int bkr) {
		_bkr = bkr;
	}

	public String getTel_nummer() {
		return _tel_nummer;
	}

	public void setTel_nummer(String tel_nummer) {
		_tel_nummer = tel_nummer;
	}

	public String getBeschrijving() {
		return _beschrijving;
	}

	public void setBeschrijving(String beschrijving) {
		_beschrijving = beschrijving;
	}

	public boolean getGecontroleerd() {
		return _gecontroleerd;
	}

	public boolean isGecontroleerd() {
		return _gecontroleerd;
	}

	public void setGecontroleerd(boolean gecontroleerd) {
		_gecontroleerd = gecontroleerd;
	}

	public boolean getGeaccepteerd() {
		return _geaccepteerd;
	}

	public boolean isGeaccepteerd() {
		return _geaccepteerd;
	}

	public void setGeaccepteerd(boolean geaccepteerd) {
		_geaccepteerd = geaccepteerd;
	}

	public String getReden() {
		return _reden;
	}

	public void setReden(String reden) {
		_reden = reden;
	}

	private int _issuerId;
	private String _voornaam;
	private String _achternaam;
	private String _bedrijfsnaam;
	private String _woonplaats;
	private String _email;
	private int _kvk;
	private int _vog;
	private int _bkr;
	private String _tel_nummer;
	private String _beschrijving;
	private boolean _gecontroleerd;
	private boolean _geaccepteerd;
	private String _reden;

}