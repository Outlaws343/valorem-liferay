/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Milestone}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Milestone
 * @generated
 */
public class MilestoneWrapper
	extends BaseModelWrapper<Milestone>
	implements Milestone, ModelWrapper<Milestone> {

	public MilestoneWrapper(Milestone milestone) {
		super(milestone);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("investeringsplanId", getInvesteringsplanId());
		attributes.put("naam", getNaam());
		attributes.put("milestone_percentage", getMilestone_percentage());
		attributes.put("einddatum", getEinddatum());
		attributes.put("beschrijving", getBeschrijving());
		attributes.put("gecontroleerd", isGecontroleerd());
		attributes.put("geaccepteerd", isGeaccepteerd());
		attributes.put(
			"inleverings_beschrijving", getInleverings_beschrijving());
		attributes.put("reden", getReden());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer investeringsplanId = (Integer)attributes.get(
			"investeringsplanId");

		if (investeringsplanId != null) {
			setInvesteringsplanId(investeringsplanId);
		}

		String naam = (String)attributes.get("naam");

		if (naam != null) {
			setNaam(naam);
		}

		Integer milestone_percentage = (Integer)attributes.get(
			"milestone_percentage");

		if (milestone_percentage != null) {
			setMilestone_percentage(milestone_percentage);
		}

		Date einddatum = (Date)attributes.get("einddatum");

		if (einddatum != null) {
			setEinddatum(einddatum);
		}

		String beschrijving = (String)attributes.get("beschrijving");

		if (beschrijving != null) {
			setBeschrijving(beschrijving);
		}

		Boolean gecontroleerd = (Boolean)attributes.get("gecontroleerd");

		if (gecontroleerd != null) {
			setGecontroleerd(gecontroleerd);
		}

		Boolean geaccepteerd = (Boolean)attributes.get("geaccepteerd");

		if (geaccepteerd != null) {
			setGeaccepteerd(geaccepteerd);
		}

		String inleverings_beschrijving = (String)attributes.get(
			"inleverings_beschrijving");

		if (inleverings_beschrijving != null) {
			setInleverings_beschrijving(inleverings_beschrijving);
		}

		String reden = (String)attributes.get("reden");

		if (reden != null) {
			setReden(reden);
		}
	}

	/**
	 * Returns the beschrijving of this milestone.
	 *
	 * @return the beschrijving of this milestone
	 */
	@Override
	public String getBeschrijving() {
		return model.getBeschrijving();
	}

	/**
	 * Returns the einddatum of this milestone.
	 *
	 * @return the einddatum of this milestone
	 */
	@Override
	public Date getEinddatum() {
		return model.getEinddatum();
	}

	/**
	 * Returns the geaccepteerd of this milestone.
	 *
	 * @return the geaccepteerd of this milestone
	 */
	@Override
	public boolean getGeaccepteerd() {
		return model.getGeaccepteerd();
	}

	/**
	 * Returns the gecontroleerd of this milestone.
	 *
	 * @return the gecontroleerd of this milestone
	 */
	@Override
	public boolean getGecontroleerd() {
		return model.getGecontroleerd();
	}

	/**
	 * Returns the inleverings_beschrijving of this milestone.
	 *
	 * @return the inleverings_beschrijving of this milestone
	 */
	@Override
	public String getInleverings_beschrijving() {
		return model.getInleverings_beschrijving();
	}

	/**
	 * Returns the investeringsplan ID of this milestone.
	 *
	 * @return the investeringsplan ID of this milestone
	 */
	@Override
	public int getInvesteringsplanId() {
		return model.getInvesteringsplanId();
	}

	/**
	 * Returns the milestone_percentage of this milestone.
	 *
	 * @return the milestone_percentage of this milestone
	 */
	@Override
	public int getMilestone_percentage() {
		return model.getMilestone_percentage();
	}

	/**
	 * Returns the naam of this milestone.
	 *
	 * @return the naam of this milestone
	 */
	@Override
	public String getNaam() {
		return model.getNaam();
	}

	/**
	 * Returns the primary key of this milestone.
	 *
	 * @return the primary key of this milestone
	 */
	@Override
	public database.service.persistence.MilestonePK getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the reden of this milestone.
	 *
	 * @return the reden of this milestone
	 */
	@Override
	public String getReden() {
		return model.getReden();
	}

	/**
	 * Returns <code>true</code> if this milestone is geaccepteerd.
	 *
	 * @return <code>true</code> if this milestone is geaccepteerd; <code>false</code> otherwise
	 */
	@Override
	public boolean isGeaccepteerd() {
		return model.isGeaccepteerd();
	}

	/**
	 * Returns <code>true</code> if this milestone is gecontroleerd.
	 *
	 * @return <code>true</code> if this milestone is gecontroleerd; <code>false</code> otherwise
	 */
	@Override
	public boolean isGecontroleerd() {
		return model.isGecontroleerd();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the beschrijving of this milestone.
	 *
	 * @param beschrijving the beschrijving of this milestone
	 */
	@Override
	public void setBeschrijving(String beschrijving) {
		model.setBeschrijving(beschrijving);
	}

	/**
	 * Sets the einddatum of this milestone.
	 *
	 * @param einddatum the einddatum of this milestone
	 */
	@Override
	public void setEinddatum(Date einddatum) {
		model.setEinddatum(einddatum);
	}

	/**
	 * Sets whether this milestone is geaccepteerd.
	 *
	 * @param geaccepteerd the geaccepteerd of this milestone
	 */
	@Override
	public void setGeaccepteerd(boolean geaccepteerd) {
		model.setGeaccepteerd(geaccepteerd);
	}

	/**
	 * Sets whether this milestone is gecontroleerd.
	 *
	 * @param gecontroleerd the gecontroleerd of this milestone
	 */
	@Override
	public void setGecontroleerd(boolean gecontroleerd) {
		model.setGecontroleerd(gecontroleerd);
	}

	/**
	 * Sets the inleverings_beschrijving of this milestone.
	 *
	 * @param inleverings_beschrijving the inleverings_beschrijving of this milestone
	 */
	@Override
	public void setInleverings_beschrijving(String inleverings_beschrijving) {
		model.setInleverings_beschrijving(inleverings_beschrijving);
	}

	/**
	 * Sets the investeringsplan ID of this milestone.
	 *
	 * @param investeringsplanId the investeringsplan ID of this milestone
	 */
	@Override
	public void setInvesteringsplanId(int investeringsplanId) {
		model.setInvesteringsplanId(investeringsplanId);
	}

	/**
	 * Sets the milestone_percentage of this milestone.
	 *
	 * @param milestone_percentage the milestone_percentage of this milestone
	 */
	@Override
	public void setMilestone_percentage(int milestone_percentage) {
		model.setMilestone_percentage(milestone_percentage);
	}

	/**
	 * Sets the naam of this milestone.
	 *
	 * @param naam the naam of this milestone
	 */
	@Override
	public void setNaam(String naam) {
		model.setNaam(naam);
	}

	/**
	 * Sets the primary key of this milestone.
	 *
	 * @param primaryKey the primary key of this milestone
	 */
	@Override
	public void setPrimaryKey(
		database.service.persistence.MilestonePK primaryKey) {

		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the reden of this milestone.
	 *
	 * @param reden the reden of this milestone
	 */
	@Override
	public void setReden(String reden) {
		model.setReden(reden);
	}

	@Override
	protected MilestoneWrapper wrap(Milestone milestone) {
		return new MilestoneWrapper(milestone);
	}

}