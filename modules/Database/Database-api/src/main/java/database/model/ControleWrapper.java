/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Controle}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Controle
 * @generated
 */
public class ControleWrapper
	extends BaseModelWrapper<Controle>
	implements Controle, ModelWrapper<Controle> {

	public ControleWrapper(Controle controle) {
		super(controle);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("issuerId", getIssuerId());
		attributes.put("akkoord", isAkkoord());
		attributes.put("reden", getReden());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer issuerId = (Integer)attributes.get("issuerId");

		if (issuerId != null) {
			setIssuerId(issuerId);
		}

		Boolean akkoord = (Boolean)attributes.get("akkoord");

		if (akkoord != null) {
			setAkkoord(akkoord);
		}

		String reden = (String)attributes.get("reden");

		if (reden != null) {
			setReden(reden);
		}
	}

	/**
	 * Returns the akkoord of this controle.
	 *
	 * @return the akkoord of this controle
	 */
	@Override
	public boolean getAkkoord() {
		return model.getAkkoord();
	}

	/**
	 * Returns the issuer ID of this controle.
	 *
	 * @return the issuer ID of this controle
	 */
	@Override
	public int getIssuerId() {
		return model.getIssuerId();
	}

	/**
	 * Returns the primary key of this controle.
	 *
	 * @return the primary key of this controle
	 */
	@Override
	public int getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the reden of this controle.
	 *
	 * @return the reden of this controle
	 */
	@Override
	public String getReden() {
		return model.getReden();
	}

	/**
	 * Returns <code>true</code> if this controle is akkoord.
	 *
	 * @return <code>true</code> if this controle is akkoord; <code>false</code> otherwise
	 */
	@Override
	public boolean isAkkoord() {
		return model.isAkkoord();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets whether this controle is akkoord.
	 *
	 * @param akkoord the akkoord of this controle
	 */
	@Override
	public void setAkkoord(boolean akkoord) {
		model.setAkkoord(akkoord);
	}

	/**
	 * Sets the issuer ID of this controle.
	 *
	 * @param issuerId the issuer ID of this controle
	 */
	@Override
	public void setIssuerId(int issuerId) {
		model.setIssuerId(issuerId);
	}

	/**
	 * Sets the primary key of this controle.
	 *
	 * @param primaryKey the primary key of this controle
	 */
	@Override
	public void setPrimaryKey(int primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the reden of this controle.
	 *
	 * @param reden the reden of this controle
	 */
	@Override
	public void setReden(String reden) {
		model.setReden(reden);
	}

	@Override
	protected ControleWrapper wrap(Controle controle) {
		return new ControleWrapper(controle);
	}

}