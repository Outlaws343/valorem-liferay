/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link STO}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see STO
 * @generated
 */
public class STOWrapper
	extends BaseModelWrapper<STO> implements ModelWrapper<STO>, STO {

	public STOWrapper(STO sto) {
		super(sto);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("STOId", getSTOId());
		attributes.put("waarde", getWaarde());
		attributes.put("participantId", getParticipantId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer STOId = (Integer)attributes.get("STOId");

		if (STOId != null) {
			setSTOId(STOId);
		}

		String waarde = (String)attributes.get("waarde");

		if (waarde != null) {
			setWaarde(waarde);
		}

		Integer participantId = (Integer)attributes.get("participantId");

		if (participantId != null) {
			setParticipantId(participantId);
		}
	}

	/**
	 * Returns the participant ID of this sto.
	 *
	 * @return the participant ID of this sto
	 */
	@Override
	public int getParticipantId() {
		return model.getParticipantId();
	}

	/**
	 * Returns the primary key of this sto.
	 *
	 * @return the primary key of this sto
	 */
	@Override
	public int getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the sto ID of this sto.
	 *
	 * @return the sto ID of this sto
	 */
	@Override
	public int getSTOId() {
		return model.getSTOId();
	}

	/**
	 * Returns the waarde of this sto.
	 *
	 * @return the waarde of this sto
	 */
	@Override
	public String getWaarde() {
		return model.getWaarde();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the participant ID of this sto.
	 *
	 * @param participantId the participant ID of this sto
	 */
	@Override
	public void setParticipantId(int participantId) {
		model.setParticipantId(participantId);
	}

	/**
	 * Sets the primary key of this sto.
	 *
	 * @param primaryKey the primary key of this sto
	 */
	@Override
	public void setPrimaryKey(int primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the sto ID of this sto.
	 *
	 * @param STOId the sto ID of this sto
	 */
	@Override
	public void setSTOId(int STOId) {
		model.setSTOId(STOId);
	}

	/**
	 * Sets the waarde of this sto.
	 *
	 * @param waarde the waarde of this sto
	 */
	@Override
	public void setWaarde(String waarde) {
		model.setWaarde(waarde);
	}

	@Override
	protected STOWrapper wrap(STO sto) {
		return new STOWrapper(sto);
	}

}