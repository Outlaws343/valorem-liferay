/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Investering}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Investering
 * @generated
 */
public class InvesteringWrapper
	extends BaseModelWrapper<Investering>
	implements Investering, ModelWrapper<Investering> {

	public InvesteringWrapper(Investering investering) {
		super(investering);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("investeringsId", getInvesteringsId());
		attributes.put("investeringsplanId", getInvesteringsplanId());
		attributes.put("participantId", getParticipantId());
		attributes.put("STOId", getSTOId());
		attributes.put("waarde", getWaarde());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer investeringsId = (Integer)attributes.get("investeringsId");

		if (investeringsId != null) {
			setInvesteringsId(investeringsId);
		}

		Integer investeringsplanId = (Integer)attributes.get(
			"investeringsplanId");

		if (investeringsplanId != null) {
			setInvesteringsplanId(investeringsplanId);
		}

		Integer participantId = (Integer)attributes.get("participantId");

		if (participantId != null) {
			setParticipantId(participantId);
		}

		Integer STOId = (Integer)attributes.get("STOId");

		if (STOId != null) {
			setSTOId(STOId);
		}

		Integer waarde = (Integer)attributes.get("waarde");

		if (waarde != null) {
			setWaarde(waarde);
		}
	}

	/**
	 * Returns the investerings ID of this investering.
	 *
	 * @return the investerings ID of this investering
	 */
	@Override
	public int getInvesteringsId() {
		return model.getInvesteringsId();
	}

	/**
	 * Returns the investeringsplan ID of this investering.
	 *
	 * @return the investeringsplan ID of this investering
	 */
	@Override
	public int getInvesteringsplanId() {
		return model.getInvesteringsplanId();
	}

	/**
	 * Returns the participant ID of this investering.
	 *
	 * @return the participant ID of this investering
	 */
	@Override
	public int getParticipantId() {
		return model.getParticipantId();
	}

	/**
	 * Returns the primary key of this investering.
	 *
	 * @return the primary key of this investering
	 */
	@Override
	public int getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the sto ID of this investering.
	 *
	 * @return the sto ID of this investering
	 */
	@Override
	public int getSTOId() {
		return model.getSTOId();
	}

	/**
	 * Returns the waarde of this investering.
	 *
	 * @return the waarde of this investering
	 */
	@Override
	public int getWaarde() {
		return model.getWaarde();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the investerings ID of this investering.
	 *
	 * @param investeringsId the investerings ID of this investering
	 */
	@Override
	public void setInvesteringsId(int investeringsId) {
		model.setInvesteringsId(investeringsId);
	}

	/**
	 * Sets the investeringsplan ID of this investering.
	 *
	 * @param investeringsplanId the investeringsplan ID of this investering
	 */
	@Override
	public void setInvesteringsplanId(int investeringsplanId) {
		model.setInvesteringsplanId(investeringsplanId);
	}

	/**
	 * Sets the participant ID of this investering.
	 *
	 * @param participantId the participant ID of this investering
	 */
	@Override
	public void setParticipantId(int participantId) {
		model.setParticipantId(participantId);
	}

	/**
	 * Sets the primary key of this investering.
	 *
	 * @param primaryKey the primary key of this investering
	 */
	@Override
	public void setPrimaryKey(int primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the sto ID of this investering.
	 *
	 * @param STOId the sto ID of this investering
	 */
	@Override
	public void setSTOId(int STOId) {
		model.setSTOId(STOId);
	}

	/**
	 * Sets the waarde of this investering.
	 *
	 * @param waarde the waarde of this investering
	 */
	@Override
	public void setWaarde(int waarde) {
		model.setWaarde(waarde);
	}

	@Override
	protected InvesteringWrapper wrap(Investering investering) {
		return new InvesteringWrapper(investering);
	}

}