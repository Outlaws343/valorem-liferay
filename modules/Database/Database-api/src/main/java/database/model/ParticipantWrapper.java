/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Participant}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Participant
 * @generated
 */
public class ParticipantWrapper
	extends BaseModelWrapper<Participant>
	implements ModelWrapper<Participant>, Participant {

	public ParticipantWrapper(Participant participant) {
		super(participant);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("participantId", getParticipantId());
		attributes.put("voornaam", getVoornaam());
		attributes.put("achternaam", getAchternaam());
		attributes.put("woonplaats", getWoonplaats());
		attributes.put("adres", getAdres());
		attributes.put("bsn", getBsn());
		attributes.put("email", getEmail());
		attributes.put("tel_nummer", getTel_nummer());
		attributes.put("verificatie", isVerificatie());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer participantId = (Integer)attributes.get("participantId");

		if (participantId != null) {
			setParticipantId(participantId);
		}

		String voornaam = (String)attributes.get("voornaam");

		if (voornaam != null) {
			setVoornaam(voornaam);
		}

		String achternaam = (String)attributes.get("achternaam");

		if (achternaam != null) {
			setAchternaam(achternaam);
		}

		String woonplaats = (String)attributes.get("woonplaats");

		if (woonplaats != null) {
			setWoonplaats(woonplaats);
		}

		String adres = (String)attributes.get("adres");

		if (adres != null) {
			setAdres(adres);
		}

		Integer bsn = (Integer)attributes.get("bsn");

		if (bsn != null) {
			setBsn(bsn);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String tel_nummer = (String)attributes.get("tel_nummer");

		if (tel_nummer != null) {
			setTel_nummer(tel_nummer);
		}

		Boolean verificatie = (Boolean)attributes.get("verificatie");

		if (verificatie != null) {
			setVerificatie(verificatie);
		}
	}

	/**
	 * Returns the achternaam of this participant.
	 *
	 * @return the achternaam of this participant
	 */
	@Override
	public String getAchternaam() {
		return model.getAchternaam();
	}

	/**
	 * Returns the adres of this participant.
	 *
	 * @return the adres of this participant
	 */
	@Override
	public String getAdres() {
		return model.getAdres();
	}

	/**
	 * Returns the bsn of this participant.
	 *
	 * @return the bsn of this participant
	 */
	@Override
	public int getBsn() {
		return model.getBsn();
	}

	/**
	 * Returns the email of this participant.
	 *
	 * @return the email of this participant
	 */
	@Override
	public String getEmail() {
		return model.getEmail();
	}

	/**
	 * Returns the participant ID of this participant.
	 *
	 * @return the participant ID of this participant
	 */
	@Override
	public int getParticipantId() {
		return model.getParticipantId();
	}

	/**
	 * Returns the primary key of this participant.
	 *
	 * @return the primary key of this participant
	 */
	@Override
	public int getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the tel_nummer of this participant.
	 *
	 * @return the tel_nummer of this participant
	 */
	@Override
	public String getTel_nummer() {
		return model.getTel_nummer();
	}

	/**
	 * Returns the verificatie of this participant.
	 *
	 * @return the verificatie of this participant
	 */
	@Override
	public boolean getVerificatie() {
		return model.getVerificatie();
	}

	/**
	 * Returns the voornaam of this participant.
	 *
	 * @return the voornaam of this participant
	 */
	@Override
	public String getVoornaam() {
		return model.getVoornaam();
	}

	/**
	 * Returns the woonplaats of this participant.
	 *
	 * @return the woonplaats of this participant
	 */
	@Override
	public String getWoonplaats() {
		return model.getWoonplaats();
	}

	/**
	 * Returns <code>true</code> if this participant is verificatie.
	 *
	 * @return <code>true</code> if this participant is verificatie; <code>false</code> otherwise
	 */
	@Override
	public boolean isVerificatie() {
		return model.isVerificatie();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the achternaam of this participant.
	 *
	 * @param achternaam the achternaam of this participant
	 */
	@Override
	public void setAchternaam(String achternaam) {
		model.setAchternaam(achternaam);
	}

	/**
	 * Sets the adres of this participant.
	 *
	 * @param adres the adres of this participant
	 */
	@Override
	public void setAdres(String adres) {
		model.setAdres(adres);
	}

	/**
	 * Sets the bsn of this participant.
	 *
	 * @param bsn the bsn of this participant
	 */
	@Override
	public void setBsn(int bsn) {
		model.setBsn(bsn);
	}

	/**
	 * Sets the email of this participant.
	 *
	 * @param email the email of this participant
	 */
	@Override
	public void setEmail(String email) {
		model.setEmail(email);
	}

	/**
	 * Sets the participant ID of this participant.
	 *
	 * @param participantId the participant ID of this participant
	 */
	@Override
	public void setParticipantId(int participantId) {
		model.setParticipantId(participantId);
	}

	/**
	 * Sets the primary key of this participant.
	 *
	 * @param primaryKey the primary key of this participant
	 */
	@Override
	public void setPrimaryKey(int primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the tel_nummer of this participant.
	 *
	 * @param tel_nummer the tel_nummer of this participant
	 */
	@Override
	public void setTel_nummer(String tel_nummer) {
		model.setTel_nummer(tel_nummer);
	}

	/**
	 * Sets whether this participant is verificatie.
	 *
	 * @param verificatie the verificatie of this participant
	 */
	@Override
	public void setVerificatie(boolean verificatie) {
		model.setVerificatie(verificatie);
	}

	/**
	 * Sets the voornaam of this participant.
	 *
	 * @param voornaam the voornaam of this participant
	 */
	@Override
	public void setVoornaam(String voornaam) {
		model.setVoornaam(voornaam);
	}

	/**
	 * Sets the woonplaats of this participant.
	 *
	 * @param woonplaats the woonplaats of this participant
	 */
	@Override
	public void setWoonplaats(String woonplaats) {
		model.setWoonplaats(woonplaats);
	}

	@Override
	protected ParticipantWrapper wrap(Participant participant) {
		return new ParticipantWrapper(participant);
	}

}