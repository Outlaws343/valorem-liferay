/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model;

import database.service.persistence.MilestonePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link database.service.http.MilestoneServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MilestoneSoap implements Serializable {

	public static MilestoneSoap toSoapModel(Milestone model) {
		MilestoneSoap soapModel = new MilestoneSoap();

		soapModel.setInvesteringsplanId(model.getInvesteringsplanId());
		soapModel.setNaam(model.getNaam());
		soapModel.setMilestone_percentage(model.getMilestone_percentage());
		soapModel.setEinddatum(model.getEinddatum());
		soapModel.setBeschrijving(model.getBeschrijving());
		soapModel.setGecontroleerd(model.isGecontroleerd());
		soapModel.setGeaccepteerd(model.isGeaccepteerd());
		soapModel.setInleverings_beschrijving(
			model.getInleverings_beschrijving());
		soapModel.setReden(model.getReden());

		return soapModel;
	}

	public static MilestoneSoap[] toSoapModels(Milestone[] models) {
		MilestoneSoap[] soapModels = new MilestoneSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MilestoneSoap[][] toSoapModels(Milestone[][] models) {
		MilestoneSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MilestoneSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MilestoneSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MilestoneSoap[] toSoapModels(List<Milestone> models) {
		List<MilestoneSoap> soapModels = new ArrayList<MilestoneSoap>(
			models.size());

		for (Milestone model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MilestoneSoap[soapModels.size()]);
	}

	public MilestoneSoap() {
	}

	public MilestonePK getPrimaryKey() {
		return new MilestonePK(_investeringsplanId, _naam);
	}

	public void setPrimaryKey(MilestonePK pk) {
		setInvesteringsplanId(pk.investeringsplanId);
		setNaam(pk.naam);
	}

	public int getInvesteringsplanId() {
		return _investeringsplanId;
	}

	public void setInvesteringsplanId(int investeringsplanId) {
		_investeringsplanId = investeringsplanId;
	}

	public String getNaam() {
		return _naam;
	}

	public void setNaam(String naam) {
		_naam = naam;
	}

	public int getMilestone_percentage() {
		return _milestone_percentage;
	}

	public void setMilestone_percentage(int milestone_percentage) {
		_milestone_percentage = milestone_percentage;
	}

	public Date getEinddatum() {
		return _einddatum;
	}

	public void setEinddatum(Date einddatum) {
		_einddatum = einddatum;
	}

	public String getBeschrijving() {
		return _beschrijving;
	}

	public void setBeschrijving(String beschrijving) {
		_beschrijving = beschrijving;
	}

	public boolean getGecontroleerd() {
		return _gecontroleerd;
	}

	public boolean isGecontroleerd() {
		return _gecontroleerd;
	}

	public void setGecontroleerd(boolean gecontroleerd) {
		_gecontroleerd = gecontroleerd;
	}

	public boolean getGeaccepteerd() {
		return _geaccepteerd;
	}

	public boolean isGeaccepteerd() {
		return _geaccepteerd;
	}

	public void setGeaccepteerd(boolean geaccepteerd) {
		_geaccepteerd = geaccepteerd;
	}

	public String getInleverings_beschrijving() {
		return _inleverings_beschrijving;
	}

	public void setInleverings_beschrijving(String inleverings_beschrijving) {
		_inleverings_beschrijving = inleverings_beschrijving;
	}

	public String getReden() {
		return _reden;
	}

	public void setReden(String reden) {
		_reden = reden;
	}

	private int _investeringsplanId;
	private String _naam;
	private int _milestone_percentage;
	private Date _einddatum;
	private String _beschrijving;
	private boolean _gecontroleerd;
	private boolean _geaccepteerd;
	private String _inleverings_beschrijving;
	private String _reden;

}