/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MilestoneService}.
 *
 * @author Brian Wing Shun Chan
 * @see MilestoneService
 * @generated
 */
public class MilestoneServiceWrapper
	implements MilestoneService, ServiceWrapper<MilestoneService> {

	public MilestoneServiceWrapper(MilestoneService milestoneService) {
		_milestoneService = milestoneService;
	}

	@Override
	public database.model.Milestone addMilestone(
		String naam, int investeringsplanId, java.util.Date eindDatum,
		int percentage, String beschrijving) {

		return _milestoneService.addMilestone(
			naam, investeringsplanId, eindDatum, percentage, beschrijving);
	}

	@Override
	public database.model.Milestone completeNextMilestone(
		int investeringsplanId, String beschrijving) {

		return _milestoneService.completeNextMilestone(
			investeringsplanId, beschrijving);
	}

	@Override
	public java.util.List<database.model.Milestone> getAcceptedMilestones() {
		return _milestoneService.getAcceptedMilestones();
	}

	@Override
	public java.util.List<database.model.Milestone> getDeniedMilestones() {
		return _milestoneService.getDeniedMilestones();
	}

	@Override
	public java.util.List<database.model.Milestone>
		getMilestonesByInvestmentPlanId(int investmentPlanId) {

		return _milestoneService.getMilestonesByInvestmentPlanId(
			investmentPlanId);
	}

	@Override
	public java.util.List<database.model.Milestone> getNotCheckedMilestones() {
		return _milestoneService.getNotCheckedMilestones();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _milestoneService.getOSGiServiceIdentifier();
	}

	@Override
	public database.model.Milestone updateMilestone(
		int investeringsplanId, String naam, boolean geaccepteerd,
		String reden) {

		return _milestoneService.updateMilestone(
			investeringsplanId, naam, geaccepteerd, reden);
	}

	@Override
	public MilestoneService getWrappedService() {
		return _milestoneService;
	}

	@Override
	public void setWrappedService(MilestoneService milestoneService) {
		_milestoneService = milestoneService;
	}

	private MilestoneService _milestoneService;

}