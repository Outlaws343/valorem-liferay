/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MilestonePK implements Comparable<MilestonePK>, Serializable {

	public int investeringsplanId;
	public String naam;

	public MilestonePK() {
	}

	public MilestonePK(int investeringsplanId, String naam) {
		this.investeringsplanId = investeringsplanId;
		this.naam = naam;
	}

	public int getInvesteringsplanId() {
		return investeringsplanId;
	}

	public void setInvesteringsplanId(int investeringsplanId) {
		this.investeringsplanId = investeringsplanId;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	@Override
	public int compareTo(MilestonePK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (investeringsplanId < pk.investeringsplanId) {
			value = -1;
		}
		else if (investeringsplanId > pk.investeringsplanId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = naam.compareTo(pk.naam);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MilestonePK)) {
			return false;
		}

		MilestonePK pk = (MilestonePK)obj;

		if ((investeringsplanId == pk.investeringsplanId) &&
			naam.equals(pk.naam)) {

			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, investeringsplanId);
		hashCode = HashUtil.hash(hashCode, naam);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(6);

		sb.append("{");

		sb.append("investeringsplanId=");

		sb.append(investeringsplanId);
		sb.append(", naam=");

		sb.append(naam);

		sb.append("}");

		return sb.toString();
	}

}