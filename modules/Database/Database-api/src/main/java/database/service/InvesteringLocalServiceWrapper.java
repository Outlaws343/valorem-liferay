/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InvesteringLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringLocalService
 * @generated
 */
public class InvesteringLocalServiceWrapper
	implements InvesteringLocalService,
			   ServiceWrapper<InvesteringLocalService> {

	public InvesteringLocalServiceWrapper(
		InvesteringLocalService investeringLocalService) {

		_investeringLocalService = investeringLocalService;
	}

	/**
	 * Adds the investering to the database. Also notifies the appropriate model listeners.
	 *
	 * @param investering the investering
	 * @return the investering that was added
	 */
	@Override
	public database.model.Investering addInvestering(
		database.model.Investering investering) {

		return _investeringLocalService.addInvestering(investering);
	}

	/**
	 * Creates a new investering with the primary key. Does not add the investering to the database.
	 *
	 * @param investeringsId the primary key for the new investering
	 * @return the new investering
	 */
	@Override
	public database.model.Investering createInvestering(int investeringsId) {
		return _investeringLocalService.createInvestering(investeringsId);
	}

	/**
	 * Deletes the investering with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering that was removed
	 * @throws PortalException if a investering with the primary key could not be found
	 */
	@Override
	public database.model.Investering deleteInvestering(int investeringsId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _investeringLocalService.deleteInvestering(investeringsId);
	}

	/**
	 * Deletes the investering from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investering the investering
	 * @return the investering that was removed
	 */
	@Override
	public database.model.Investering deleteInvestering(
		database.model.Investering investering) {

		return _investeringLocalService.deleteInvestering(investering);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _investeringLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _investeringLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _investeringLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _investeringLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _investeringLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _investeringLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _investeringLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public database.model.Investering fetchInvestering(int investeringsId) {
		return _investeringLocalService.fetchInvestering(investeringsId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _investeringLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _investeringLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the investering with the primary key.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering
	 * @throws PortalException if a investering with the primary key could not be found
	 */
	@Override
	public database.model.Investering getInvestering(int investeringsId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _investeringLocalService.getInvestering(investeringsId);
	}

	/**
	 * Returns a range of all the investerings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investerings
	 * @param end the upper bound of the range of investerings (not inclusive)
	 * @return the range of investerings
	 */
	@Override
	public java.util.List<database.model.Investering> getInvesterings(
		int start, int end) {

		return _investeringLocalService.getInvesterings(start, end);
	}

	/**
	 * Returns the number of investerings.
	 *
	 * @return the number of investerings
	 */
	@Override
	public int getInvesteringsCount() {
		return _investeringLocalService.getInvesteringsCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _investeringLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _investeringLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the investering in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param investering the investering
	 * @return the investering that was updated
	 */
	@Override
	public database.model.Investering updateInvestering(
		database.model.Investering investering) {

		return _investeringLocalService.updateInvestering(investering);
	}

	@Override
	public InvesteringLocalService getWrappedService() {
		return _investeringLocalService;
	}

	@Override
	public void setWrappedService(
		InvesteringLocalService investeringLocalService) {

		_investeringLocalService = investeringLocalService;
	}

	private InvesteringLocalService _investeringLocalService;

}