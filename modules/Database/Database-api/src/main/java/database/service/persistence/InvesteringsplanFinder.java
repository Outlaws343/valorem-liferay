/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import org.osgi.annotation.versioning.ProviderType;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public interface InvesteringsplanFinder {

	public java.util.List<database.model.Investeringsplan>
		getAllInvesteringsplannen();

	public database.model.Investeringsplan getInvesteringsplanById(
		int investeringsplanId);

	public java.util.List<database.model.Investeringsplan>
		getNotCheckedInvesteringsplan();

	public java.util.List<database.model.Investeringsplan>
		getAcceptedInvesteringsplan();

	public java.util.List<database.model.Investeringsplan>
		getDeniedInvesteringsplan();

	public java.util.List<database.model.Investeringsplan>
		getInvesteringsplansByIssuerId(int issuerId);

	public database.model.Investeringsplan addInvesteringsplan(
		String naam, int doel, java.util.Date beginDatum, String beschrijving);

	public database.model.Investeringsplan updateInvesteringsplan(
		int investeringsplanId, boolean geaccepteerd, String reden);

	public database.model.Investeringsplan updateInvesteringsplanProgress(
		int investeringsplanId, int voortgang);

}