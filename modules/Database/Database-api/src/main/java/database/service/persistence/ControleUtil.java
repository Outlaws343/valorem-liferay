/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import database.model.Controle;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the controle service. This utility wraps <code>database.service.persistence.impl.ControlePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ControlePersistence
 * @generated
 */
public class ControleUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Controle controle) {
		getPersistence().clearCache(controle);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Controle> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Controle> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Controle> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Controle> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Controle> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Controle update(Controle controle) {
		return getPersistence().update(controle);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Controle update(
		Controle controle, ServiceContext serviceContext) {

		return getPersistence().update(controle, serviceContext);
	}

	/**
	 * Caches the controle in the entity cache if it is enabled.
	 *
	 * @param controle the controle
	 */
	public static void cacheResult(Controle controle) {
		getPersistence().cacheResult(controle);
	}

	/**
	 * Caches the controles in the entity cache if it is enabled.
	 *
	 * @param controles the controles
	 */
	public static void cacheResult(List<Controle> controles) {
		getPersistence().cacheResult(controles);
	}

	/**
	 * Creates a new controle with the primary key. Does not add the controle to the database.
	 *
	 * @param issuerId the primary key for the new controle
	 * @return the new controle
	 */
	public static Controle create(int issuerId) {
		return getPersistence().create(issuerId);
	}

	/**
	 * Removes the controle with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle that was removed
	 * @throws NoSuchControleException if a controle with the primary key could not be found
	 */
	public static Controle remove(int issuerId)
		throws database.exception.NoSuchControleException {

		return getPersistence().remove(issuerId);
	}

	public static Controle updateImpl(Controle controle) {
		return getPersistence().updateImpl(controle);
	}

	/**
	 * Returns the controle with the primary key or throws a <code>NoSuchControleException</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle
	 * @throws NoSuchControleException if a controle with the primary key could not be found
	 */
	public static Controle findByPrimaryKey(int issuerId)
		throws database.exception.NoSuchControleException {

		return getPersistence().findByPrimaryKey(issuerId);
	}

	/**
	 * Returns the controle with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle, or <code>null</code> if a controle with the primary key could not be found
	 */
	public static Controle fetchByPrimaryKey(int issuerId) {
		return getPersistence().fetchByPrimaryKey(issuerId);
	}

	/**
	 * Returns all the controles.
	 *
	 * @return the controles
	 */
	public static List<Controle> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the controles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ControleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of controles
	 * @param end the upper bound of the range of controles (not inclusive)
	 * @return the range of controles
	 */
	public static List<Controle> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the controles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ControleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of controles
	 * @param end the upper bound of the range of controles (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of controles
	 */
	public static List<Controle> findAll(
		int start, int end, OrderByComparator<Controle> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the controles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ControleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of controles
	 * @param end the upper bound of the range of controles (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of controles
	 */
	public static List<Controle> findAll(
		int start, int end, OrderByComparator<Controle> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the controles from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of controles.
	 *
	 * @return the number of controles
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ControlePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ControlePersistence, ControlePersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(ControlePersistence.class);

		ServiceTracker<ControlePersistence, ControlePersistence>
			serviceTracker =
				new ServiceTracker<ControlePersistence, ControlePersistence>(
					bundle.getBundleContext(), ControlePersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}