/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Investering. This utility wraps
 * <code>database.service.impl.InvesteringLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringLocalService
 * @generated
 */
public class InvesteringLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>database.service.impl.InvesteringLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the investering to the database. Also notifies the appropriate model listeners.
	 *
	 * @param investering the investering
	 * @return the investering that was added
	 */
	public static database.model.Investering addInvestering(
		database.model.Investering investering) {

		return getService().addInvestering(investering);
	}

	/**
	 * Creates a new investering with the primary key. Does not add the investering to the database.
	 *
	 * @param investeringsId the primary key for the new investering
	 * @return the new investering
	 */
	public static database.model.Investering createInvestering(
		int investeringsId) {

		return getService().createInvestering(investeringsId);
	}

	/**
	 * Deletes the investering with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering that was removed
	 * @throws PortalException if a investering with the primary key could not be found
	 */
	public static database.model.Investering deleteInvestering(
			int investeringsId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteInvestering(investeringsId);
	}

	/**
	 * Deletes the investering from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investering the investering
	 * @return the investering that was removed
	 */
	public static database.model.Investering deleteInvestering(
		database.model.Investering investering) {

		return getService().deleteInvestering(investering);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static database.model.Investering fetchInvestering(
		int investeringsId) {

		return getService().fetchInvestering(investeringsId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the investering with the primary key.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering
	 * @throws PortalException if a investering with the primary key could not be found
	 */
	public static database.model.Investering getInvestering(int investeringsId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getInvestering(investeringsId);
	}

	/**
	 * Returns a range of all the investerings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investerings
	 * @param end the upper bound of the range of investerings (not inclusive)
	 * @return the range of investerings
	 */
	public static java.util.List<database.model.Investering> getInvesterings(
		int start, int end) {

		return getService().getInvesterings(start, end);
	}

	/**
	 * Returns the number of investerings.
	 *
	 * @return the number of investerings
	 */
	public static int getInvesteringsCount() {
		return getService().getInvesteringsCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the investering in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param investering the investering
	 * @return the investering that was updated
	 */
	public static database.model.Investering updateInvestering(
		database.model.Investering investering) {

		return getService().updateInvestering(investering);
	}

	public static InvesteringLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<InvesteringLocalService, InvesteringLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(InvesteringLocalService.class);

		ServiceTracker<InvesteringLocalService, InvesteringLocalService>
			serviceTracker =
				new ServiceTracker
					<InvesteringLocalService, InvesteringLocalService>(
						bundle.getBundleContext(),
						InvesteringLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}