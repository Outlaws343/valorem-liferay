/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InvesteringsplanLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringsplanLocalService
 * @generated
 */
public class InvesteringsplanLocalServiceWrapper
	implements InvesteringsplanLocalService,
			   ServiceWrapper<InvesteringsplanLocalService> {

	public InvesteringsplanLocalServiceWrapper(
		InvesteringsplanLocalService investeringsplanLocalService) {

		_investeringsplanLocalService = investeringsplanLocalService;
	}

	/**
	 * Adds the investeringsplan to the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplan the investeringsplan
	 * @return the investeringsplan that was added
	 */
	@Override
	public database.model.Investeringsplan addInvesteringsplan(
		database.model.Investeringsplan investeringsplan) {

		return _investeringsplanLocalService.addInvesteringsplan(
			investeringsplan);
	}

	/**
	 * Creates a new investeringsplan with the primary key. Does not add the investeringsplan to the database.
	 *
	 * @param investeringsplanId the primary key for the new investeringsplan
	 * @return the new investeringsplan
	 */
	@Override
	public database.model.Investeringsplan createInvesteringsplan(
		int investeringsplanId) {

		return _investeringsplanLocalService.createInvesteringsplan(
			investeringsplanId);
	}

	/**
	 * Deletes the investeringsplan with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan that was removed
	 * @throws PortalException if a investeringsplan with the primary key could not be found
	 */
	@Override
	public database.model.Investeringsplan deleteInvesteringsplan(
			int investeringsplanId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _investeringsplanLocalService.deleteInvesteringsplan(
			investeringsplanId);
	}

	/**
	 * Deletes the investeringsplan from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplan the investeringsplan
	 * @return the investeringsplan that was removed
	 */
	@Override
	public database.model.Investeringsplan deleteInvesteringsplan(
		database.model.Investeringsplan investeringsplan) {

		return _investeringsplanLocalService.deleteInvesteringsplan(
			investeringsplan);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _investeringsplanLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _investeringsplanLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _investeringsplanLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _investeringsplanLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _investeringsplanLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _investeringsplanLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _investeringsplanLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public database.model.Investeringsplan fetchInvesteringsplan(
		int investeringsplanId) {

		return _investeringsplanLocalService.fetchInvesteringsplan(
			investeringsplanId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _investeringsplanLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _investeringsplanLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the investeringsplan with the primary key.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan
	 * @throws PortalException if a investeringsplan with the primary key could not be found
	 */
	@Override
	public database.model.Investeringsplan getInvesteringsplan(
			int investeringsplanId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _investeringsplanLocalService.getInvesteringsplan(
			investeringsplanId);
	}

	/**
	 * Returns a range of all the investeringsplans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investeringsplans
	 * @param end the upper bound of the range of investeringsplans (not inclusive)
	 * @return the range of investeringsplans
	 */
	@Override
	public java.util.List<database.model.Investeringsplan> getInvesteringsplans(
		int start, int end) {

		return _investeringsplanLocalService.getInvesteringsplans(start, end);
	}

	/**
	 * Returns the number of investeringsplans.
	 *
	 * @return the number of investeringsplans
	 */
	@Override
	public int getInvesteringsplansCount() {
		return _investeringsplanLocalService.getInvesteringsplansCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _investeringsplanLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _investeringsplanLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the investeringsplan in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplan the investeringsplan
	 * @return the investeringsplan that was updated
	 */
	@Override
	public database.model.Investeringsplan updateInvesteringsplan(
		database.model.Investeringsplan investeringsplan) {

		return _investeringsplanLocalService.updateInvesteringsplan(
			investeringsplan);
	}

	@Override
	public InvesteringsplanLocalService getWrappedService() {
		return _investeringsplanLocalService;
	}

	@Override
	public void setWrappedService(
		InvesteringsplanLocalService investeringsplanLocalService) {

		_investeringsplanLocalService = investeringsplanLocalService;
	}

	private InvesteringsplanLocalService _investeringsplanLocalService;

}