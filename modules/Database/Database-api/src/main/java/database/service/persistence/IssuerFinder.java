/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import org.osgi.annotation.versioning.ProviderType;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public interface IssuerFinder {

	public java.util.List<database.model.Issuer> getNotCheckedIssuers();

	public java.util.List<database.model.Issuer> getAcceptedIssuers();

	public java.util.List<database.model.Issuer> getDeniedIssuers();

	public database.model.Issuer addIssuer(
		String voornaam, String achternaam, String bedrijfsnaam,
		String woonplaats, String email, int kvk, int vog, int bkr,
		String tel_nummer);

	public database.model.Issuer updateIssuer(
		int issuerId, boolean geaccepteerd, String reden);

}