/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Milestone. This utility wraps
 * <code>database.service.impl.MilestoneServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see MilestoneService
 * @generated
 */
public class MilestoneServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>database.service.impl.MilestoneServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static database.model.Milestone addMilestone(
		String naam, int investeringsplanId, java.util.Date eindDatum,
		int percentage, String beschrijving) {

		return getService().addMilestone(
			naam, investeringsplanId, eindDatum, percentage, beschrijving);
	}

	public static database.model.Milestone completeNextMilestone(
		int investeringsplanId, String beschrijving) {

		return getService().completeNextMilestone(
			investeringsplanId, beschrijving);
	}

	public static java.util.List<database.model.Milestone>
		getAcceptedMilestones() {

		return getService().getAcceptedMilestones();
	}

	public static java.util.List<database.model.Milestone>
		getDeniedMilestones() {

		return getService().getDeniedMilestones();
	}

	public static java.util.List<database.model.Milestone>
		getMilestonesByInvestmentPlanId(int investmentPlanId) {

		return getService().getMilestonesByInvestmentPlanId(investmentPlanId);
	}

	public static java.util.List<database.model.Milestone>
		getNotCheckedMilestones() {

		return getService().getNotCheckedMilestones();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static database.model.Milestone updateMilestone(
		int investeringsplanId, String naam, boolean geaccepteerd,
		String reden) {

		return getService().updateMilestone(
			investeringsplanId, naam, geaccepteerd, reden);
	}

	public static MilestoneService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MilestoneService, MilestoneService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(MilestoneService.class);

		ServiceTracker<MilestoneService, MilestoneService> serviceTracker =
			new ServiceTracker<MilestoneService, MilestoneService>(
				bundle.getBundleContext(), MilestoneService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}