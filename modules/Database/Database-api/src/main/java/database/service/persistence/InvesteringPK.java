/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class InvesteringPK implements Comparable<InvesteringPK>, Serializable {

	public String issuerId;
	public String participantId;

	public int STOID;

	public InvesteringPK() {
	}

	public InvesteringPK(String issuerId, String participantId, int STOID) {
		this.issuerId = issuerId;
		this.participantId = participantId;
		this.STOID = STOID;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getParticipantId() {
		return participantId;
	}

	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}

	public int getSTOID() {
		return STOID;
	}

	public void setSTOID(int STOID) {
		this.STOID = STOID;
	}

	@Override
	public int compareTo(InvesteringPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		value = issuerId.compareTo(pk.issuerId);

		if (value != 0) {
			return value;
		}

		value = participantId.compareTo(pk.participantId);

		if (value != 0) {
			return value;
		}

		if (STOID < pk.STOID) {
			value = -1;
		}
		else if (STOID > pk.STOID) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InvesteringPK)) {
			return false;
		}

		InvesteringPK pk = (InvesteringPK)obj;

		if (issuerId.equals(pk.issuerId) &&
			participantId.equals(pk.participantId) && (STOID == pk.STOID)) {

			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, issuerId);
		hashCode = HashUtil.hash(hashCode, participantId);
		hashCode = HashUtil.hash(hashCode, STOID);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(8);

		sb.append("{");

		sb.append("issuerId=");

		sb.append(issuerId);
		sb.append(", participantId=");

		sb.append(participantId);
		sb.append(", STOID=");

		sb.append(STOID);

		sb.append("}");

		return sb.toString();
	}

}