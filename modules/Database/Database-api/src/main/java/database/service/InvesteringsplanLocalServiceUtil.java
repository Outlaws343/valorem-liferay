/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Investeringsplan. This utility wraps
 * <code>database.service.impl.InvesteringsplanLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringsplanLocalService
 * @generated
 */
public class InvesteringsplanLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>database.service.impl.InvesteringsplanLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the investeringsplan to the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplan the investeringsplan
	 * @return the investeringsplan that was added
	 */
	public static database.model.Investeringsplan addInvesteringsplan(
		database.model.Investeringsplan investeringsplan) {

		return getService().addInvesteringsplan(investeringsplan);
	}

	/**
	 * Creates a new investeringsplan with the primary key. Does not add the investeringsplan to the database.
	 *
	 * @param investeringsplanId the primary key for the new investeringsplan
	 * @return the new investeringsplan
	 */
	public static database.model.Investeringsplan createInvesteringsplan(
		int investeringsplanId) {

		return getService().createInvesteringsplan(investeringsplanId);
	}

	/**
	 * Deletes the investeringsplan with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan that was removed
	 * @throws PortalException if a investeringsplan with the primary key could not be found
	 */
	public static database.model.Investeringsplan deleteInvesteringsplan(
			int investeringsplanId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteInvesteringsplan(investeringsplanId);
	}

	/**
	 * Deletes the investeringsplan from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplan the investeringsplan
	 * @return the investeringsplan that was removed
	 */
	public static database.model.Investeringsplan deleteInvesteringsplan(
		database.model.Investeringsplan investeringsplan) {

		return getService().deleteInvesteringsplan(investeringsplan);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static database.model.Investeringsplan fetchInvesteringsplan(
		int investeringsplanId) {

		return getService().fetchInvesteringsplan(investeringsplanId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the investeringsplan with the primary key.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan
	 * @throws PortalException if a investeringsplan with the primary key could not be found
	 */
	public static database.model.Investeringsplan getInvesteringsplan(
			int investeringsplanId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getInvesteringsplan(investeringsplanId);
	}

	/**
	 * Returns a range of all the investeringsplans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investeringsplans
	 * @param end the upper bound of the range of investeringsplans (not inclusive)
	 * @return the range of investeringsplans
	 */
	public static java.util.List<database.model.Investeringsplan>
		getInvesteringsplans(int start, int end) {

		return getService().getInvesteringsplans(start, end);
	}

	/**
	 * Returns the number of investeringsplans.
	 *
	 * @return the number of investeringsplans
	 */
	public static int getInvesteringsplansCount() {
		return getService().getInvesteringsplansCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the investeringsplan in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplan the investeringsplan
	 * @return the investeringsplan that was updated
	 */
	public static database.model.Investeringsplan updateInvesteringsplan(
		database.model.Investeringsplan investeringsplan) {

		return getService().updateInvesteringsplan(investeringsplan);
	}

	public static InvesteringsplanLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<InvesteringsplanLocalService, InvesteringsplanLocalService>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			InvesteringsplanLocalService.class);

		ServiceTracker
			<InvesteringsplanLocalService, InvesteringsplanLocalService>
				serviceTracker =
					new ServiceTracker
						<InvesteringsplanLocalService,
						 InvesteringsplanLocalService>(
							 bundle.getBundleContext(),
							 InvesteringsplanLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}