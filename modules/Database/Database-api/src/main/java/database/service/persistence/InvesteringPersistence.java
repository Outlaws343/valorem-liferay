/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import database.exception.NoSuchInvesteringException;

import database.model.Investering;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the investering service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringUtil
 * @generated
 */
@ProviderType
public interface InvesteringPersistence extends BasePersistence<Investering> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link InvesteringUtil} to access the investering persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Caches the investering in the entity cache if it is enabled.
	 *
	 * @param investering the investering
	 */
	public void cacheResult(Investering investering);

	/**
	 * Caches the investerings in the entity cache if it is enabled.
	 *
	 * @param investerings the investerings
	 */
	public void cacheResult(java.util.List<Investering> investerings);

	/**
	 * Creates a new investering with the primary key. Does not add the investering to the database.
	 *
	 * @param investeringsId the primary key for the new investering
	 * @return the new investering
	 */
	public Investering create(int investeringsId);

	/**
	 * Removes the investering with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering that was removed
	 * @throws NoSuchInvesteringException if a investering with the primary key could not be found
	 */
	public Investering remove(int investeringsId)
		throws NoSuchInvesteringException;

	public Investering updateImpl(Investering investering);

	/**
	 * Returns the investering with the primary key or throws a <code>NoSuchInvesteringException</code> if it could not be found.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering
	 * @throws NoSuchInvesteringException if a investering with the primary key could not be found
	 */
	public Investering findByPrimaryKey(int investeringsId)
		throws NoSuchInvesteringException;

	/**
	 * Returns the investering with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering, or <code>null</code> if a investering with the primary key could not be found
	 */
	public Investering fetchByPrimaryKey(int investeringsId);

	/**
	 * Returns all the investerings.
	 *
	 * @return the investerings
	 */
	public java.util.List<Investering> findAll();

	/**
	 * Returns a range of all the investerings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investerings
	 * @param end the upper bound of the range of investerings (not inclusive)
	 * @return the range of investerings
	 */
	public java.util.List<Investering> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the investerings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investerings
	 * @param end the upper bound of the range of investerings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of investerings
	 */
	public java.util.List<Investering> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Investering>
			orderByComparator);

	/**
	 * Returns an ordered range of all the investerings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investerings
	 * @param end the upper bound of the range of investerings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of investerings
	 */
	public java.util.List<Investering> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Investering>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the investerings from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of investerings.
	 *
	 * @return the number of investerings
	 */
	public int countAll();

}