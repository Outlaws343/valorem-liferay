/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import database.exception.NoSuchSTOException;

import database.model.STO;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the sto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see STOUtil
 * @generated
 */
@ProviderType
public interface STOPersistence extends BasePersistence<STO> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link STOUtil} to access the sto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Caches the sto in the entity cache if it is enabled.
	 *
	 * @param sto the sto
	 */
	public void cacheResult(STO sto);

	/**
	 * Caches the stos in the entity cache if it is enabled.
	 *
	 * @param stOs the stos
	 */
	public void cacheResult(java.util.List<STO> stOs);

	/**
	 * Creates a new sto with the primary key. Does not add the sto to the database.
	 *
	 * @param STOId the primary key for the new sto
	 * @return the new sto
	 */
	public STO create(int STOId);

	/**
	 * Removes the sto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto that was removed
	 * @throws NoSuchSTOException if a sto with the primary key could not be found
	 */
	public STO remove(int STOId) throws NoSuchSTOException;

	public STO updateImpl(STO sto);

	/**
	 * Returns the sto with the primary key or throws a <code>NoSuchSTOException</code> if it could not be found.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto
	 * @throws NoSuchSTOException if a sto with the primary key could not be found
	 */
	public STO findByPrimaryKey(int STOId) throws NoSuchSTOException;

	/**
	 * Returns the sto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto, or <code>null</code> if a sto with the primary key could not be found
	 */
	public STO fetchByPrimaryKey(int STOId);

	/**
	 * Returns all the stos.
	 *
	 * @return the stos
	 */
	public java.util.List<STO> findAll();

	/**
	 * Returns a range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @return the range of stos
	 */
	public java.util.List<STO> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of stos
	 */
	public java.util.List<STO> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<STO>
			orderByComparator);

	/**
	 * Returns an ordered range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of stos
	 */
	public java.util.List<STO> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<STO> orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the stos from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of stos.
	 *
	 * @return the number of stos
	 */
	public int countAll();

}