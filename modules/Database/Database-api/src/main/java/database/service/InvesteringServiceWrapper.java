/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InvesteringService}.
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringService
 * @generated
 */
public class InvesteringServiceWrapper
	implements InvesteringService, ServiceWrapper<InvesteringService> {

	public InvesteringServiceWrapper(InvesteringService investeringService) {
		_investeringService = investeringService;
	}

	@Override
	public database.model.Investering addInvestering(
		int investeringsplanId, int participantId, int waarde) {

		return _investeringService.addInvestering(
			investeringsplanId, participantId, waarde);
	}

	@Override
	public java.util.List<database.model.Investering> getInvesteringById(
		int participantId) {

		return _investeringService.getInvesteringById(participantId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _investeringService.getOSGiServiceIdentifier();
	}

	@Override
	public InvesteringService getWrappedService() {
		return _investeringService;
	}

	@Override
	public void setWrappedService(InvesteringService investeringService) {
		_investeringService = investeringService;
	}

	private InvesteringService _investeringService;

}