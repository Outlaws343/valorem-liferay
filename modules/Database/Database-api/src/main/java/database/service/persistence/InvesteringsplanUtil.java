/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import database.model.Investeringsplan;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the investeringsplan service. This utility wraps <code>database.service.persistence.impl.InvesteringsplanPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringsplanPersistence
 * @generated
 */
public class InvesteringsplanUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Investeringsplan investeringsplan) {
		getPersistence().clearCache(investeringsplan);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Investeringsplan> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Investeringsplan> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Investeringsplan> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Investeringsplan> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Investeringsplan> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Investeringsplan update(Investeringsplan investeringsplan) {
		return getPersistence().update(investeringsplan);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Investeringsplan update(
		Investeringsplan investeringsplan, ServiceContext serviceContext) {

		return getPersistence().update(investeringsplan, serviceContext);
	}

	/**
	 * Caches the investeringsplan in the entity cache if it is enabled.
	 *
	 * @param investeringsplan the investeringsplan
	 */
	public static void cacheResult(Investeringsplan investeringsplan) {
		getPersistence().cacheResult(investeringsplan);
	}

	/**
	 * Caches the investeringsplans in the entity cache if it is enabled.
	 *
	 * @param investeringsplans the investeringsplans
	 */
	public static void cacheResult(List<Investeringsplan> investeringsplans) {
		getPersistence().cacheResult(investeringsplans);
	}

	/**
	 * Creates a new investeringsplan with the primary key. Does not add the investeringsplan to the database.
	 *
	 * @param investeringsplanId the primary key for the new investeringsplan
	 * @return the new investeringsplan
	 */
	public static Investeringsplan create(int investeringsplanId) {
		return getPersistence().create(investeringsplanId);
	}

	/**
	 * Removes the investeringsplan with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan that was removed
	 * @throws NoSuchInvesteringsplanException if a investeringsplan with the primary key could not be found
	 */
	public static Investeringsplan remove(int investeringsplanId)
		throws database.exception.NoSuchInvesteringsplanException {

		return getPersistence().remove(investeringsplanId);
	}

	public static Investeringsplan updateImpl(
		Investeringsplan investeringsplan) {

		return getPersistence().updateImpl(investeringsplan);
	}

	/**
	 * Returns the investeringsplan with the primary key or throws a <code>NoSuchInvesteringsplanException</code> if it could not be found.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan
	 * @throws NoSuchInvesteringsplanException if a investeringsplan with the primary key could not be found
	 */
	public static Investeringsplan findByPrimaryKey(int investeringsplanId)
		throws database.exception.NoSuchInvesteringsplanException {

		return getPersistence().findByPrimaryKey(investeringsplanId);
	}

	/**
	 * Returns the investeringsplan with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan, or <code>null</code> if a investeringsplan with the primary key could not be found
	 */
	public static Investeringsplan fetchByPrimaryKey(int investeringsplanId) {
		return getPersistence().fetchByPrimaryKey(investeringsplanId);
	}

	/**
	 * Returns all the investeringsplans.
	 *
	 * @return the investeringsplans
	 */
	public static List<Investeringsplan> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the investeringsplans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investeringsplans
	 * @param end the upper bound of the range of investeringsplans (not inclusive)
	 * @return the range of investeringsplans
	 */
	public static List<Investeringsplan> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the investeringsplans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investeringsplans
	 * @param end the upper bound of the range of investeringsplans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of investeringsplans
	 */
	public static List<Investeringsplan> findAll(
		int start, int end,
		OrderByComparator<Investeringsplan> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the investeringsplans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investeringsplans
	 * @param end the upper bound of the range of investeringsplans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of investeringsplans
	 */
	public static List<Investeringsplan> findAll(
		int start, int end,
		OrderByComparator<Investeringsplan> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the investeringsplans from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of investeringsplans.
	 *
	 * @return the number of investeringsplans
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static InvesteringsplanPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<InvesteringsplanPersistence, InvesteringsplanPersistence>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			InvesteringsplanPersistence.class);

		ServiceTracker<InvesteringsplanPersistence, InvesteringsplanPersistence>
			serviceTracker =
				new ServiceTracker
					<InvesteringsplanPersistence, InvesteringsplanPersistence>(
						bundle.getBundleContext(),
						InvesteringsplanPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}