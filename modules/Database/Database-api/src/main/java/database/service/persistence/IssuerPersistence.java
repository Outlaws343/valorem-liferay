/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import database.exception.NoSuchIssuerException;

import database.model.Issuer;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the issuer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IssuerUtil
 * @generated
 */
@ProviderType
public interface IssuerPersistence extends BasePersistence<Issuer> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link IssuerUtil} to access the issuer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Caches the issuer in the entity cache if it is enabled.
	 *
	 * @param issuer the issuer
	 */
	public void cacheResult(Issuer issuer);

	/**
	 * Caches the issuers in the entity cache if it is enabled.
	 *
	 * @param issuers the issuers
	 */
	public void cacheResult(java.util.List<Issuer> issuers);

	/**
	 * Creates a new issuer with the primary key. Does not add the issuer to the database.
	 *
	 * @param issuerId the primary key for the new issuer
	 * @return the new issuer
	 */
	public Issuer create(int issuerId);

	/**
	 * Removes the issuer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer that was removed
	 * @throws NoSuchIssuerException if a issuer with the primary key could not be found
	 */
	public Issuer remove(int issuerId) throws NoSuchIssuerException;

	public Issuer updateImpl(Issuer issuer);

	/**
	 * Returns the issuer with the primary key or throws a <code>NoSuchIssuerException</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer
	 * @throws NoSuchIssuerException if a issuer with the primary key could not be found
	 */
	public Issuer findByPrimaryKey(int issuerId) throws NoSuchIssuerException;

	/**
	 * Returns the issuer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer, or <code>null</code> if a issuer with the primary key could not be found
	 */
	public Issuer fetchByPrimaryKey(int issuerId);

	/**
	 * Returns all the issuers.
	 *
	 * @return the issuers
	 */
	public java.util.List<Issuer> findAll();

	/**
	 * Returns a range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @return the range of issuers
	 */
	public java.util.List<Issuer> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of issuers
	 */
	public java.util.List<Issuer> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Issuer>
			orderByComparator);

	/**
	 * Returns an ordered range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of issuers
	 */
	public java.util.List<Issuer> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Issuer>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the issuers from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of issuers.
	 *
	 * @return the number of issuers
	 */
	public int countAll();

}