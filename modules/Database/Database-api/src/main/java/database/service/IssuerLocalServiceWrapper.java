/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link IssuerLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see IssuerLocalService
 * @generated
 */
public class IssuerLocalServiceWrapper
	implements IssuerLocalService, ServiceWrapper<IssuerLocalService> {

	public IssuerLocalServiceWrapper(IssuerLocalService issuerLocalService) {
		_issuerLocalService = issuerLocalService;
	}

	/**
	 * Adds the issuer to the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuer the issuer
	 * @return the issuer that was added
	 */
	@Override
	public database.model.Issuer addIssuer(database.model.Issuer issuer) {
		return _issuerLocalService.addIssuer(issuer);
	}

	/**
	 * Creates a new issuer with the primary key. Does not add the issuer to the database.
	 *
	 * @param issuerId the primary key for the new issuer
	 * @return the new issuer
	 */
	@Override
	public database.model.Issuer createIssuer(int issuerId) {
		return _issuerLocalService.createIssuer(issuerId);
	}

	/**
	 * Deletes the issuer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer that was removed
	 * @throws PortalException if a issuer with the primary key could not be found
	 */
	@Override
	public database.model.Issuer deleteIssuer(int issuerId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _issuerLocalService.deleteIssuer(issuerId);
	}

	/**
	 * Deletes the issuer from the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuer the issuer
	 * @return the issuer that was removed
	 */
	@Override
	public database.model.Issuer deleteIssuer(database.model.Issuer issuer) {
		return _issuerLocalService.deleteIssuer(issuer);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _issuerLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _issuerLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _issuerLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _issuerLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _issuerLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _issuerLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _issuerLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public database.model.Issuer fetchIssuer(int issuerId) {
		return _issuerLocalService.fetchIssuer(issuerId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _issuerLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _issuerLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the issuer with the primary key.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer
	 * @throws PortalException if a issuer with the primary key could not be found
	 */
	@Override
	public database.model.Issuer getIssuer(int issuerId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _issuerLocalService.getIssuer(issuerId);
	}

	/**
	 * Returns a range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @return the range of issuers
	 */
	@Override
	public java.util.List<database.model.Issuer> getIssuers(
		int start, int end) {

		return _issuerLocalService.getIssuers(start, end);
	}

	/**
	 * Returns the number of issuers.
	 *
	 * @return the number of issuers
	 */
	@Override
	public int getIssuersCount() {
		return _issuerLocalService.getIssuersCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _issuerLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _issuerLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the issuer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param issuer the issuer
	 * @return the issuer that was updated
	 */
	@Override
	public database.model.Issuer updateIssuer(database.model.Issuer issuer) {
		return _issuerLocalService.updateIssuer(issuer);
	}

	@Override
	public IssuerLocalService getWrappedService() {
		return _issuerLocalService;
	}

	@Override
	public void setWrappedService(IssuerLocalService issuerLocalService) {
		_issuerLocalService = issuerLocalService;
	}

	private IssuerLocalService _issuerLocalService;

}