/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import database.model.STO;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the sto service. This utility wraps <code>database.service.persistence.impl.STOPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see STOPersistence
 * @generated
 */
public class STOUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(STO sto) {
		getPersistence().clearCache(sto);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, STO> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<STO> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<STO> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<STO> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<STO> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static STO update(STO sto) {
		return getPersistence().update(sto);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static STO update(STO sto, ServiceContext serviceContext) {
		return getPersistence().update(sto, serviceContext);
	}

	/**
	 * Caches the sto in the entity cache if it is enabled.
	 *
	 * @param sto the sto
	 */
	public static void cacheResult(STO sto) {
		getPersistence().cacheResult(sto);
	}

	/**
	 * Caches the stos in the entity cache if it is enabled.
	 *
	 * @param stOs the stos
	 */
	public static void cacheResult(List<STO> stOs) {
		getPersistence().cacheResult(stOs);
	}

	/**
	 * Creates a new sto with the primary key. Does not add the sto to the database.
	 *
	 * @param STOId the primary key for the new sto
	 * @return the new sto
	 */
	public static STO create(int STOId) {
		return getPersistence().create(STOId);
	}

	/**
	 * Removes the sto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto that was removed
	 * @throws NoSuchSTOException if a sto with the primary key could not be found
	 */
	public static STO remove(int STOId)
		throws database.exception.NoSuchSTOException {

		return getPersistence().remove(STOId);
	}

	public static STO updateImpl(STO sto) {
		return getPersistence().updateImpl(sto);
	}

	/**
	 * Returns the sto with the primary key or throws a <code>NoSuchSTOException</code> if it could not be found.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto
	 * @throws NoSuchSTOException if a sto with the primary key could not be found
	 */
	public static STO findByPrimaryKey(int STOId)
		throws database.exception.NoSuchSTOException {

		return getPersistence().findByPrimaryKey(STOId);
	}

	/**
	 * Returns the sto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto, or <code>null</code> if a sto with the primary key could not be found
	 */
	public static STO fetchByPrimaryKey(int STOId) {
		return getPersistence().fetchByPrimaryKey(STOId);
	}

	/**
	 * Returns all the stos.
	 *
	 * @return the stos
	 */
	public static List<STO> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @return the range of stos
	 */
	public static List<STO> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of stos
	 */
	public static List<STO> findAll(
		int start, int end, OrderByComparator<STO> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of stos
	 */
	public static List<STO> findAll(
		int start, int end, OrderByComparator<STO> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the stos from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of stos.
	 *
	 * @return the number of stos
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static STOPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<STOPersistence, STOPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(STOPersistence.class);

		ServiceTracker<STOPersistence, STOPersistence> serviceTracker =
			new ServiceTracker<STOPersistence, STOPersistence>(
				bundle.getBundleContext(), STOPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}