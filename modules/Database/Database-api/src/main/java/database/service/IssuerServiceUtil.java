/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Issuer. This utility wraps
 * <code>database.service.impl.IssuerServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see IssuerService
 * @generated
 */
public class IssuerServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>database.service.impl.IssuerServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static database.model.Issuer addIssuer(
		String voornaam, String achternaam, String bedrijfsnaam,
		String woonplaats, String email, int kvk, int vog, int bkr,
		String tel_nummer) {

		return getService().addIssuer(
			voornaam, achternaam, bedrijfsnaam, woonplaats, email, kvk, vog,
			bkr, tel_nummer);
	}

	public static java.util.List<database.model.Issuer> getAcceptedIssuers() {
		return getService().getAcceptedIssuers();
	}

	public static java.util.List<database.model.Issuer> getDeniedIssuers() {
		return getService().getDeniedIssuers();
	}

	public static java.util.List<database.model.Issuer> getNotCheckedIssuers() {
		return getService().getNotCheckedIssuers();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static database.model.Issuer updateIssuer(
		int issuerId, boolean geaccepteerd, String reden) {

		return getService().updateIssuer(issuerId, geaccepteerd, reden);
	}

	public static IssuerService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<IssuerService, IssuerService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(IssuerService.class);

		ServiceTracker<IssuerService, IssuerService> serviceTracker =
			new ServiceTracker<IssuerService, IssuerService>(
				bundle.getBundleContext(), IssuerService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}