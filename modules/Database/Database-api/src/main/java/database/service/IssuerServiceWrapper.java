/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link IssuerService}.
 *
 * @author Brian Wing Shun Chan
 * @see IssuerService
 * @generated
 */
public class IssuerServiceWrapper
	implements IssuerService, ServiceWrapper<IssuerService> {

	public IssuerServiceWrapper(IssuerService issuerService) {
		_issuerService = issuerService;
	}

	@Override
	public database.model.Issuer addIssuer(
		String voornaam, String achternaam, String bedrijfsnaam,
		String woonplaats, String email, int kvk, int vog, int bkr,
		String tel_nummer) {

		return _issuerService.addIssuer(
			voornaam, achternaam, bedrijfsnaam, woonplaats, email, kvk, vog,
			bkr, tel_nummer);
	}

	@Override
	public java.util.List<database.model.Issuer> getAcceptedIssuers() {
		return _issuerService.getAcceptedIssuers();
	}

	@Override
	public java.util.List<database.model.Issuer> getDeniedIssuers() {
		return _issuerService.getDeniedIssuers();
	}

	@Override
	public java.util.List<database.model.Issuer> getNotCheckedIssuers() {
		return _issuerService.getNotCheckedIssuers();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _issuerService.getOSGiServiceIdentifier();
	}

	@Override
	public database.model.Issuer updateIssuer(
		int issuerId, boolean geaccepteerd, String reden) {

		return _issuerService.updateIssuer(issuerId, geaccepteerd, reden);
	}

	@Override
	public IssuerService getWrappedService() {
		return _issuerService;
	}

	@Override
	public void setWrappedService(IssuerService issuerService) {
		_issuerService = issuerService;
	}

	private IssuerService _issuerService;

}