/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Investering. This utility wraps
 * <code>database.service.impl.InvesteringServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringService
 * @generated
 */
public class InvesteringServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>database.service.impl.InvesteringServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static database.model.Investering addInvestering(
		int investeringsplanId, int participantId, int waarde) {

		return getService().addInvestering(
			investeringsplanId, participantId, waarde);
	}

	public static java.util.List<database.model.Investering> getInvesteringById(
		int participantId) {

		return getService().getInvesteringById(participantId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static InvesteringService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<InvesteringService, InvesteringService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(InvesteringService.class);

		ServiceTracker<InvesteringService, InvesteringService> serviceTracker =
			new ServiceTracker<InvesteringService, InvesteringService>(
				bundle.getBundleContext(), InvesteringService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}