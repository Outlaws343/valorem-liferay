/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import database.exception.NoSuchMilestoneException;

import database.model.Milestone;

import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the milestone service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MilestoneUtil
 * @generated
 */
@ProviderType
public interface MilestonePersistence extends BasePersistence<Milestone> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MilestoneUtil} to access the milestone persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Caches the milestone in the entity cache if it is enabled.
	 *
	 * @param milestone the milestone
	 */
	public void cacheResult(Milestone milestone);

	/**
	 * Caches the milestones in the entity cache if it is enabled.
	 *
	 * @param milestones the milestones
	 */
	public void cacheResult(java.util.List<Milestone> milestones);

	/**
	 * Creates a new milestone with the primary key. Does not add the milestone to the database.
	 *
	 * @param milestonePK the primary key for the new milestone
	 * @return the new milestone
	 */
	public Milestone create(
		database.service.persistence.MilestonePK milestonePK);

	/**
	 * Removes the milestone with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param milestonePK the primary key of the milestone
	 * @return the milestone that was removed
	 * @throws NoSuchMilestoneException if a milestone with the primary key could not be found
	 */
	public Milestone remove(
			database.service.persistence.MilestonePK milestonePK)
		throws NoSuchMilestoneException;

	public Milestone updateImpl(Milestone milestone);

	/**
	 * Returns the milestone with the primary key or throws a <code>NoSuchMilestoneException</code> if it could not be found.
	 *
	 * @param milestonePK the primary key of the milestone
	 * @return the milestone
	 * @throws NoSuchMilestoneException if a milestone with the primary key could not be found
	 */
	public Milestone findByPrimaryKey(
			database.service.persistence.MilestonePK milestonePK)
		throws NoSuchMilestoneException;

	/**
	 * Returns the milestone with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param milestonePK the primary key of the milestone
	 * @return the milestone, or <code>null</code> if a milestone with the primary key could not be found
	 */
	public Milestone fetchByPrimaryKey(
		database.service.persistence.MilestonePK milestonePK);

	/**
	 * Returns all the milestones.
	 *
	 * @return the milestones
	 */
	public java.util.List<Milestone> findAll();

	/**
	 * Returns a range of all the milestones.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MilestoneModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of milestones
	 * @param end the upper bound of the range of milestones (not inclusive)
	 * @return the range of milestones
	 */
	public java.util.List<Milestone> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the milestones.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MilestoneModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of milestones
	 * @param end the upper bound of the range of milestones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of milestones
	 */
	public java.util.List<Milestone> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Milestone>
			orderByComparator);

	/**
	 * Returns an ordered range of all the milestones.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MilestoneModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of milestones
	 * @param end the upper bound of the range of milestones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of milestones
	 */
	public java.util.List<Milestone> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Milestone>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the milestones from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of milestones.
	 *
	 * @return the number of milestones
	 */
	public int countAll();

	public Set<String> getCompoundPKColumnNames();

}