/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InvesteringsplanService}.
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringsplanService
 * @generated
 */
public class InvesteringsplanServiceWrapper
	implements InvesteringsplanService,
			   ServiceWrapper<InvesteringsplanService> {

	public InvesteringsplanServiceWrapper(
		InvesteringsplanService investeringsplanService) {

		_investeringsplanService = investeringsplanService;
	}

	@Override
	public database.model.Investeringsplan addInvesteringsplan(
		String naam, int doel, java.util.Date beginDatum, String beschrijving) {

		return _investeringsplanService.addInvesteringsplan(
			naam, doel, beginDatum, beschrijving);
	}

	@Override
	public java.util.List<database.model.Investeringsplan>
		getAcceptedInvesteringsplan() {

		return _investeringsplanService.getAcceptedInvesteringsplan();
	}

	@Override
	public java.util.List<database.model.Investeringsplan>
		getAllInvesteringsplannen() {

		return _investeringsplanService.getAllInvesteringsplannen();
	}

	@Override
	public java.util.List<database.model.Investeringsplan>
		getDeniedInvesteringsplan() {

		return _investeringsplanService.getDeniedInvesteringsplan();
	}

	@Override
	public database.model.Investeringsplan getInvesteringsplanById(
		int investeringsplanId) {

		return _investeringsplanService.getInvesteringsplanById(
			investeringsplanId);
	}

	@Override
	public java.util.List<database.model.Investeringsplan>
		getInvesteringsplansByIssuerId(int issuerId) {

		return _investeringsplanService.getInvesteringsplansByIssuerId(
			issuerId);
	}

	@Override
	public java.util.List<database.model.Investeringsplan>
		getNotCheckedInvesteringsplan() {

		return _investeringsplanService.getNotCheckedInvesteringsplan();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _investeringsplanService.getOSGiServiceIdentifier();
	}

	@Override
	public database.model.Investeringsplan updateInvesteringsplan(
		int investeringsplanId, boolean geaccepteerd, String reden) {

		return _investeringsplanService.updateInvesteringsplan(
			investeringsplanId, geaccepteerd, reden);
	}

	@Override
	public database.model.Investeringsplan updateInvesteringsplanProgress(
		int investeringsplanId, int voortgang) {

		return _investeringsplanService.updateInvesteringsplanProgress(
			investeringsplanId, voortgang);
	}

	@Override
	public InvesteringsplanService getWrappedService() {
		return _investeringsplanService;
	}

	@Override
	public void setWrappedService(
		InvesteringsplanService investeringsplanService) {

		_investeringsplanService = investeringsplanService;
	}

	private InvesteringsplanService _investeringsplanService;

}