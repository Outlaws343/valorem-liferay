/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Investeringsplan. This utility wraps
 * <code>database.service.impl.InvesteringsplanServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringsplanService
 * @generated
 */
public class InvesteringsplanServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>database.service.impl.InvesteringsplanServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static database.model.Investeringsplan addInvesteringsplan(
		String naam, int doel, java.util.Date beginDatum, String beschrijving) {

		return getService().addInvesteringsplan(
			naam, doel, beginDatum, beschrijving);
	}

	public static java.util.List<database.model.Investeringsplan>
		getAcceptedInvesteringsplan() {

		return getService().getAcceptedInvesteringsplan();
	}

	public static java.util.List<database.model.Investeringsplan>
		getAllInvesteringsplannen() {

		return getService().getAllInvesteringsplannen();
	}

	public static java.util.List<database.model.Investeringsplan>
		getDeniedInvesteringsplan() {

		return getService().getDeniedInvesteringsplan();
	}

	public static database.model.Investeringsplan getInvesteringsplanById(
		int investeringsplanId) {

		return getService().getInvesteringsplanById(investeringsplanId);
	}

	public static java.util.List<database.model.Investeringsplan>
		getInvesteringsplansByIssuerId(int issuerId) {

		return getService().getInvesteringsplansByIssuerId(issuerId);
	}

	public static java.util.List<database.model.Investeringsplan>
		getNotCheckedInvesteringsplan() {

		return getService().getNotCheckedInvesteringsplan();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static database.model.Investeringsplan updateInvesteringsplan(
		int investeringsplanId, boolean geaccepteerd, String reden) {

		return getService().updateInvesteringsplan(
			investeringsplanId, geaccepteerd, reden);
	}

	public static database.model.Investeringsplan
		updateInvesteringsplanProgress(int investeringsplanId, int voortgang) {

		return getService().updateInvesteringsplanProgress(
			investeringsplanId, voortgang);
	}

	public static InvesteringsplanService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<InvesteringsplanService, InvesteringsplanService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(InvesteringsplanService.class);

		ServiceTracker<InvesteringsplanService, InvesteringsplanService>
			serviceTracker =
				new ServiceTracker
					<InvesteringsplanService, InvesteringsplanService>(
						bundle.getBundleContext(),
						InvesteringsplanService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}