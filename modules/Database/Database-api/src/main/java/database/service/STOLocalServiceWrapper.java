/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link STOLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see STOLocalService
 * @generated
 */
public class STOLocalServiceWrapper
	implements ServiceWrapper<STOLocalService>, STOLocalService {

	public STOLocalServiceWrapper(STOLocalService stoLocalService) {
		_stoLocalService = stoLocalService;
	}

	/**
	 * Adds the sto to the database. Also notifies the appropriate model listeners.
	 *
	 * @param sto the sto
	 * @return the sto that was added
	 */
	@Override
	public database.model.STO addSTO(database.model.STO sto) {
		return _stoLocalService.addSTO(sto);
	}

	/**
	 * Creates a new sto with the primary key. Does not add the sto to the database.
	 *
	 * @param STOId the primary key for the new sto
	 * @return the new sto
	 */
	@Override
	public database.model.STO createSTO(int STOId) {
		return _stoLocalService.createSTO(STOId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _stoLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the sto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto that was removed
	 * @throws PortalException if a sto with the primary key could not be found
	 */
	@Override
	public database.model.STO deleteSTO(int STOId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _stoLocalService.deleteSTO(STOId);
	}

	/**
	 * Deletes the sto from the database. Also notifies the appropriate model listeners.
	 *
	 * @param sto the sto
	 * @return the sto that was removed
	 */
	@Override
	public database.model.STO deleteSTO(database.model.STO sto) {
		return _stoLocalService.deleteSTO(sto);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _stoLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _stoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.STOModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _stoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.STOModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _stoLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _stoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _stoLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public database.model.STO fetchSTO(int STOId) {
		return _stoLocalService.fetchSTO(STOId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _stoLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _stoLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _stoLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _stoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the sto with the primary key.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto
	 * @throws PortalException if a sto with the primary key could not be found
	 */
	@Override
	public database.model.STO getSTO(int STOId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _stoLocalService.getSTO(STOId);
	}

	/**
	 * Returns a range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @return the range of stos
	 */
	@Override
	public java.util.List<database.model.STO> getSTOs(int start, int end) {
		return _stoLocalService.getSTOs(start, end);
	}

	/**
	 * Returns the number of stos.
	 *
	 * @return the number of stos
	 */
	@Override
	public int getSTOsCount() {
		return _stoLocalService.getSTOsCount();
	}

	/**
	 * Updates the sto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param sto the sto
	 * @return the sto that was updated
	 */
	@Override
	public database.model.STO updateSTO(database.model.STO sto) {
		return _stoLocalService.updateSTO(sto);
	}

	@Override
	public STOLocalService getWrappedService() {
		return _stoLocalService;
	}

	@Override
	public void setWrappedService(STOLocalService stoLocalService) {
		_stoLocalService = stoLocalService;
	}

	private STOLocalService _stoLocalService;

}