/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import database.model.Issuer;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the issuer service. This utility wraps <code>database.service.persistence.impl.IssuerPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IssuerPersistence
 * @generated
 */
public class IssuerUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Issuer issuer) {
		getPersistence().clearCache(issuer);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Issuer> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Issuer> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Issuer> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Issuer> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Issuer> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Issuer update(Issuer issuer) {
		return getPersistence().update(issuer);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Issuer update(Issuer issuer, ServiceContext serviceContext) {
		return getPersistence().update(issuer, serviceContext);
	}

	/**
	 * Caches the issuer in the entity cache if it is enabled.
	 *
	 * @param issuer the issuer
	 */
	public static void cacheResult(Issuer issuer) {
		getPersistence().cacheResult(issuer);
	}

	/**
	 * Caches the issuers in the entity cache if it is enabled.
	 *
	 * @param issuers the issuers
	 */
	public static void cacheResult(List<Issuer> issuers) {
		getPersistence().cacheResult(issuers);
	}

	/**
	 * Creates a new issuer with the primary key. Does not add the issuer to the database.
	 *
	 * @param issuerId the primary key for the new issuer
	 * @return the new issuer
	 */
	public static Issuer create(int issuerId) {
		return getPersistence().create(issuerId);
	}

	/**
	 * Removes the issuer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer that was removed
	 * @throws NoSuchIssuerException if a issuer with the primary key could not be found
	 */
	public static Issuer remove(int issuerId)
		throws database.exception.NoSuchIssuerException {

		return getPersistence().remove(issuerId);
	}

	public static Issuer updateImpl(Issuer issuer) {
		return getPersistence().updateImpl(issuer);
	}

	/**
	 * Returns the issuer with the primary key or throws a <code>NoSuchIssuerException</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer
	 * @throws NoSuchIssuerException if a issuer with the primary key could not be found
	 */
	public static Issuer findByPrimaryKey(int issuerId)
		throws database.exception.NoSuchIssuerException {

		return getPersistence().findByPrimaryKey(issuerId);
	}

	/**
	 * Returns the issuer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer, or <code>null</code> if a issuer with the primary key could not be found
	 */
	public static Issuer fetchByPrimaryKey(int issuerId) {
		return getPersistence().fetchByPrimaryKey(issuerId);
	}

	/**
	 * Returns all the issuers.
	 *
	 * @return the issuers
	 */
	public static List<Issuer> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @return the range of issuers
	 */
	public static List<Issuer> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of issuers
	 */
	public static List<Issuer> findAll(
		int start, int end, OrderByComparator<Issuer> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of issuers
	 */
	public static List<Issuer> findAll(
		int start, int end, OrderByComparator<Issuer> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the issuers from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of issuers.
	 *
	 * @return the number of issuers
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static IssuerPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<IssuerPersistence, IssuerPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(IssuerPersistence.class);

		ServiceTracker<IssuerPersistence, IssuerPersistence> serviceTracker =
			new ServiceTracker<IssuerPersistence, IssuerPersistence>(
				bundle.getBundleContext(), IssuerPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}