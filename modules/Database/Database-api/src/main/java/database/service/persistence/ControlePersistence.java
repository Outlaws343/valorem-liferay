/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import database.exception.NoSuchControleException;

import database.model.Controle;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the controle service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ControleUtil
 * @generated
 */
@ProviderType
public interface ControlePersistence extends BasePersistence<Controle> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ControleUtil} to access the controle persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Caches the controle in the entity cache if it is enabled.
	 *
	 * @param controle the controle
	 */
	public void cacheResult(Controle controle);

	/**
	 * Caches the controles in the entity cache if it is enabled.
	 *
	 * @param controles the controles
	 */
	public void cacheResult(java.util.List<Controle> controles);

	/**
	 * Creates a new controle with the primary key. Does not add the controle to the database.
	 *
	 * @param issuerId the primary key for the new controle
	 * @return the new controle
	 */
	public Controle create(int issuerId);

	/**
	 * Removes the controle with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle that was removed
	 * @throws NoSuchControleException if a controle with the primary key could not be found
	 */
	public Controle remove(int issuerId) throws NoSuchControleException;

	public Controle updateImpl(Controle controle);

	/**
	 * Returns the controle with the primary key or throws a <code>NoSuchControleException</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle
	 * @throws NoSuchControleException if a controle with the primary key could not be found
	 */
	public Controle findByPrimaryKey(int issuerId)
		throws NoSuchControleException;

	/**
	 * Returns the controle with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle, or <code>null</code> if a controle with the primary key could not be found
	 */
	public Controle fetchByPrimaryKey(int issuerId);

	/**
	 * Returns all the controles.
	 *
	 * @return the controles
	 */
	public java.util.List<Controle> findAll();

	/**
	 * Returns a range of all the controles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ControleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of controles
	 * @param end the upper bound of the range of controles (not inclusive)
	 * @return the range of controles
	 */
	public java.util.List<Controle> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the controles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ControleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of controles
	 * @param end the upper bound of the range of controles (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of controles
	 */
	public java.util.List<Controle> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Controle>
			orderByComparator);

	/**
	 * Returns an ordered range of all the controles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ControleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of controles
	 * @param end the upper bound of the range of controles (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of controles
	 */
	public java.util.List<Controle> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Controle>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the controles from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of controles.
	 *
	 * @return the number of controles
	 */
	public int countAll();

}