/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ControleLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ControleLocalService
 * @generated
 */
public class ControleLocalServiceWrapper
	implements ControleLocalService, ServiceWrapper<ControleLocalService> {

	public ControleLocalServiceWrapper(
		ControleLocalService controleLocalService) {

		_controleLocalService = controleLocalService;
	}

	/**
	 * Adds the controle to the database. Also notifies the appropriate model listeners.
	 *
	 * @param controle the controle
	 * @return the controle that was added
	 */
	@Override
	public database.model.Controle addControle(
		database.model.Controle controle) {

		return _controleLocalService.addControle(controle);
	}

	/**
	 * Creates a new controle with the primary key. Does not add the controle to the database.
	 *
	 * @param issuerId the primary key for the new controle
	 * @return the new controle
	 */
	@Override
	public database.model.Controle createControle(int issuerId) {
		return _controleLocalService.createControle(issuerId);
	}

	/**
	 * Deletes the controle from the database. Also notifies the appropriate model listeners.
	 *
	 * @param controle the controle
	 * @return the controle that was removed
	 */
	@Override
	public database.model.Controle deleteControle(
		database.model.Controle controle) {

		return _controleLocalService.deleteControle(controle);
	}

	/**
	 * Deletes the controle with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle that was removed
	 * @throws PortalException if a controle with the primary key could not be found
	 */
	@Override
	public database.model.Controle deleteControle(int issuerId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _controleLocalService.deleteControle(issuerId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _controleLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _controleLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _controleLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.ControleModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _controleLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.ControleModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _controleLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _controleLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _controleLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public database.model.Controle fetchControle(int issuerId) {
		return _controleLocalService.fetchControle(issuerId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _controleLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the controle with the primary key.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle
	 * @throws PortalException if a controle with the primary key could not be found
	 */
	@Override
	public database.model.Controle getControle(int issuerId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _controleLocalService.getControle(issuerId);
	}

	/**
	 * Returns a range of all the controles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.ControleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of controles
	 * @param end the upper bound of the range of controles (not inclusive)
	 * @return the range of controles
	 */
	@Override
	public java.util.List<database.model.Controle> getControles(
		int start, int end) {

		return _controleLocalService.getControles(start, end);
	}

	/**
	 * Returns the number of controles.
	 *
	 * @return the number of controles
	 */
	@Override
	public int getControlesCount() {
		return _controleLocalService.getControlesCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _controleLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _controleLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _controleLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the controle in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param controle the controle
	 * @return the controle that was updated
	 */
	@Override
	public database.model.Controle updateControle(
		database.model.Controle controle) {

		return _controleLocalService.updateControle(controle);
	}

	@Override
	public ControleLocalService getWrappedService() {
		return _controleLocalService;
	}

	@Override
	public void setWrappedService(ControleLocalService controleLocalService) {
		_controleLocalService = controleLocalService;
	}

	private ControleLocalService _controleLocalService;

}