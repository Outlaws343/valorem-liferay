/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class InvesteringsplanPK
	implements Comparable<InvesteringsplanPK>, Serializable {

	public String issuerId;
	public String naam;

	public InvesteringsplanPK() {
	}

	public InvesteringsplanPK(String issuerId, String naam) {
		this.issuerId = issuerId;
		this.naam = naam;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	@Override
	public int compareTo(InvesteringsplanPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		value = issuerId.compareTo(pk.issuerId);

		if (value != 0) {
			return value;
		}

		value = naam.compareTo(pk.naam);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InvesteringsplanPK)) {
			return false;
		}

		InvesteringsplanPK pk = (InvesteringsplanPK)obj;

		if (issuerId.equals(pk.issuerId) && naam.equals(pk.naam)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, issuerId);
		hashCode = HashUtil.hash(hashCode, naam);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(6);

		sb.append("{");

		sb.append("issuerId=");

		sb.append(issuerId);
		sb.append(", naam=");

		sb.append(naam);

		sb.append("}");

		return sb.toString();
	}

}