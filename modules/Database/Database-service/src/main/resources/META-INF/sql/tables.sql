create table Investering (
	investeringsId INTEGER not null primary key,
	investeringsplanId INTEGER,
	participantId INTEGER,
	STOId INTEGER,
	waarde INTEGER
);

create table Investeringsplan (
	investeringsplanId INTEGER not null primary key,
	issuerId INTEGER,
	naam VARCHAR(75) null,
	status VARCHAR(75) null,
	voortgang INTEGER,
	doel INTEGER,
	beschrijving VARCHAR(200) null,
	begindatum DATE null,
	gecontroleerd BOOLEAN,
	geaccepteerd BOOLEAN,
	reden VARCHAR(75) null
);

create table Issuer (
	issuerId INTEGER not null primary key,
	voornaam VARCHAR(75) null,
	achternaam VARCHAR(75) null,
	bedrijfsnaam VARCHAR(75) null,
	woonplaats VARCHAR(75) null,
	email VARCHAR(75) null,
	kvk INTEGER,
	vog INTEGER,
	bkr INTEGER,
	tel_nummer VARCHAR(75) null,
	beschrijving VARCHAR(200) null,
	gecontroleerd BOOLEAN,
	geaccepteerd BOOLEAN,
	reden VARCHAR(75) null
);

create table Milestone (
	investeringsplanId INTEGER not null,
	naam VARCHAR(75) not null,
	milestone_percentage INTEGER,
	einddatum DATE null,
	beschrijving VARCHAR(200) null,
	gecontroleerd BOOLEAN,
	geaccepteerd BOOLEAN,
	inleverings_beschrijving VARCHAR(75) null,
	reden VARCHAR(200) null,
	primary key (investeringsplanId, naam)
);

create table Participant (
	participantId INTEGER not null primary key,
	voornaam VARCHAR(75) null,
	achternaam VARCHAR(75) null,
	woonplaats VARCHAR(75) null,
	adres VARCHAR(75) null,
	bsn INTEGER,
	email VARCHAR(75) null,
	tel_nummer VARCHAR(75) null,
	verificatie BOOLEAN
);

create table STO (
	STOId INTEGER not null primary key,
	waarde VARCHAR(75) null,
	participantId INTEGER
);