/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import database.model.Investering;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Investering in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class InvesteringCacheModel
	implements CacheModel<Investering>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InvesteringCacheModel)) {
			return false;
		}

		InvesteringCacheModel investeringCacheModel =
			(InvesteringCacheModel)obj;

		if (investeringsId == investeringCacheModel.investeringsId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, investeringsId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{investeringsId=");
		sb.append(investeringsId);
		sb.append(", investeringsplanId=");
		sb.append(investeringsplanId);
		sb.append(", participantId=");
		sb.append(participantId);
		sb.append(", STOId=");
		sb.append(STOId);
		sb.append(", waarde=");
		sb.append(waarde);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Investering toEntityModel() {
		InvesteringImpl investeringImpl = new InvesteringImpl();

		investeringImpl.setInvesteringsId(investeringsId);
		investeringImpl.setInvesteringsplanId(investeringsplanId);
		investeringImpl.setParticipantId(participantId);
		investeringImpl.setSTOId(STOId);
		investeringImpl.setWaarde(waarde);

		investeringImpl.resetOriginalValues();

		return investeringImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		investeringsId = objectInput.readInt();

		investeringsplanId = objectInput.readInt();

		participantId = objectInput.readInt();

		STOId = objectInput.readInt();

		waarde = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeInt(investeringsId);

		objectOutput.writeInt(investeringsplanId);

		objectOutput.writeInt(participantId);

		objectOutput.writeInt(STOId);

		objectOutput.writeInt(waarde);
	}

	public int investeringsId;
	public int investeringsplanId;
	public int participantId;
	public int STOId;
	public int waarde;

}