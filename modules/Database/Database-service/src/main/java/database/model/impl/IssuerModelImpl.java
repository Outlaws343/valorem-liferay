/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.util.ProxyUtil;

import database.model.Issuer;
import database.model.IssuerModel;
import database.model.IssuerSoap;

import java.io.Serializable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * The base model implementation for the Issuer service. Represents a row in the &quot;Issuer&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface <code>IssuerModel</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link IssuerImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IssuerImpl
 * @generated
 */
@JSON(strict = true)
public class IssuerModelImpl
	extends BaseModelImpl<Issuer> implements IssuerModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a issuer model instance should use the <code>Issuer</code> interface instead.
	 */
	public static final String TABLE_NAME = "Issuer";

	public static final Object[][] TABLE_COLUMNS = {
		{"issuerId", Types.INTEGER}, {"voornaam", Types.VARCHAR},
		{"achternaam", Types.VARCHAR}, {"bedrijfsnaam", Types.VARCHAR},
		{"woonplaats", Types.VARCHAR}, {"email", Types.VARCHAR},
		{"kvk", Types.INTEGER}, {"vog", Types.INTEGER}, {"bkr", Types.INTEGER},
		{"tel_nummer", Types.VARCHAR}, {"beschrijving", Types.VARCHAR},
		{"gecontroleerd", Types.BOOLEAN}, {"geaccepteerd", Types.BOOLEAN},
		{"reden", Types.VARCHAR}
	};

	public static final Map<String, Integer> TABLE_COLUMNS_MAP =
		new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("issuerId", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("voornaam", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("achternaam", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("bedrijfsnaam", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("woonplaats", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("email", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("kvk", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("vog", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("bkr", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("tel_nummer", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("beschrijving", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("gecontroleerd", Types.BOOLEAN);
		TABLE_COLUMNS_MAP.put("geaccepteerd", Types.BOOLEAN);
		TABLE_COLUMNS_MAP.put("reden", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE =
		"create table Issuer (issuerId INTEGER not null primary key,voornaam VARCHAR(75) null,achternaam VARCHAR(75) null,bedrijfsnaam VARCHAR(75) null,woonplaats VARCHAR(75) null,email VARCHAR(75) null,kvk INTEGER,vog INTEGER,bkr INTEGER,tel_nummer VARCHAR(75) null,beschrijving VARCHAR(200) null,gecontroleerd BOOLEAN,geaccepteerd BOOLEAN,reden VARCHAR(75) null)";

	public static final String TABLE_SQL_DROP = "drop table Issuer";

	public static final String ORDER_BY_JPQL = " ORDER BY issuer.issuerId ASC";

	public static final String ORDER_BY_SQL = " ORDER BY Issuer.issuerId ASC";

	public static final String DATA_SOURCE = "liferayDataSource";

	public static final String SESSION_FACTORY = "liferaySessionFactory";

	public static final String TX_MANAGER = "liferayTransactionManager";

	public static void setEntityCacheEnabled(boolean entityCacheEnabled) {
		_entityCacheEnabled = entityCacheEnabled;
	}

	public static void setFinderCacheEnabled(boolean finderCacheEnabled) {
		_finderCacheEnabled = finderCacheEnabled;
	}

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static Issuer toModel(IssuerSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		Issuer model = new IssuerImpl();

		model.setIssuerId(soapModel.getIssuerId());
		model.setVoornaam(soapModel.getVoornaam());
		model.setAchternaam(soapModel.getAchternaam());
		model.setBedrijfsnaam(soapModel.getBedrijfsnaam());
		model.setWoonplaats(soapModel.getWoonplaats());
		model.setEmail(soapModel.getEmail());
		model.setKvk(soapModel.getKvk());
		model.setVog(soapModel.getVog());
		model.setBkr(soapModel.getBkr());
		model.setTel_nummer(soapModel.getTel_nummer());
		model.setBeschrijving(soapModel.getBeschrijving());
		model.setGecontroleerd(soapModel.isGecontroleerd());
		model.setGeaccepteerd(soapModel.isGeaccepteerd());
		model.setReden(soapModel.getReden());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<Issuer> toModels(IssuerSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<Issuer> models = new ArrayList<Issuer>(soapModels.length);

		for (IssuerSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public IssuerModelImpl() {
	}

	@Override
	public int getPrimaryKey() {
		return _issuerId;
	}

	@Override
	public void setPrimaryKey(int primaryKey) {
		setIssuerId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _issuerId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Class<?> getModelClass() {
		return Issuer.class;
	}

	@Override
	public String getModelClassName() {
		return Issuer.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		Map<String, Function<Issuer, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		for (Map.Entry<String, Function<Issuer, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<Issuer, Object> attributeGetterFunction = entry.getValue();

			attributes.put(
				attributeName, attributeGetterFunction.apply((Issuer)this));
		}

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Map<String, BiConsumer<Issuer, Object>> attributeSetterBiConsumers =
			getAttributeSetterBiConsumers();

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			String attributeName = entry.getKey();

			BiConsumer<Issuer, Object> attributeSetterBiConsumer =
				attributeSetterBiConsumers.get(attributeName);

			if (attributeSetterBiConsumer != null) {
				attributeSetterBiConsumer.accept(
					(Issuer)this, entry.getValue());
			}
		}
	}

	public Map<String, Function<Issuer, Object>> getAttributeGetterFunctions() {
		return _attributeGetterFunctions;
	}

	public Map<String, BiConsumer<Issuer, Object>>
		getAttributeSetterBiConsumers() {

		return _attributeSetterBiConsumers;
	}

	private static Function<InvocationHandler, Issuer>
		_getProxyProviderFunction() {

		Class<?> proxyClass = ProxyUtil.getProxyClass(
			Issuer.class.getClassLoader(), Issuer.class, ModelWrapper.class);

		try {
			Constructor<Issuer> constructor =
				(Constructor<Issuer>)proxyClass.getConstructor(
					InvocationHandler.class);

			return invocationHandler -> {
				try {
					return constructor.newInstance(invocationHandler);
				}
				catch (ReflectiveOperationException
							reflectiveOperationException) {

					throw new InternalError(reflectiveOperationException);
				}
			};
		}
		catch (NoSuchMethodException noSuchMethodException) {
			throw new InternalError(noSuchMethodException);
		}
	}

	private static final Map<String, Function<Issuer, Object>>
		_attributeGetterFunctions;
	private static final Map<String, BiConsumer<Issuer, Object>>
		_attributeSetterBiConsumers;

	static {
		Map<String, Function<Issuer, Object>> attributeGetterFunctions =
			new LinkedHashMap<String, Function<Issuer, Object>>();
		Map<String, BiConsumer<Issuer, ?>> attributeSetterBiConsumers =
			new LinkedHashMap<String, BiConsumer<Issuer, ?>>();

		attributeGetterFunctions.put("issuerId", Issuer::getIssuerId);
		attributeSetterBiConsumers.put(
			"issuerId", (BiConsumer<Issuer, Integer>)Issuer::setIssuerId);
		attributeGetterFunctions.put("voornaam", Issuer::getVoornaam);
		attributeSetterBiConsumers.put(
			"voornaam", (BiConsumer<Issuer, String>)Issuer::setVoornaam);
		attributeGetterFunctions.put("achternaam", Issuer::getAchternaam);
		attributeSetterBiConsumers.put(
			"achternaam", (BiConsumer<Issuer, String>)Issuer::setAchternaam);
		attributeGetterFunctions.put("bedrijfsnaam", Issuer::getBedrijfsnaam);
		attributeSetterBiConsumers.put(
			"bedrijfsnaam",
			(BiConsumer<Issuer, String>)Issuer::setBedrijfsnaam);
		attributeGetterFunctions.put("woonplaats", Issuer::getWoonplaats);
		attributeSetterBiConsumers.put(
			"woonplaats", (BiConsumer<Issuer, String>)Issuer::setWoonplaats);
		attributeGetterFunctions.put("email", Issuer::getEmail);
		attributeSetterBiConsumers.put(
			"email", (BiConsumer<Issuer, String>)Issuer::setEmail);
		attributeGetterFunctions.put("kvk", Issuer::getKvk);
		attributeSetterBiConsumers.put(
			"kvk", (BiConsumer<Issuer, Integer>)Issuer::setKvk);
		attributeGetterFunctions.put("vog", Issuer::getVog);
		attributeSetterBiConsumers.put(
			"vog", (BiConsumer<Issuer, Integer>)Issuer::setVog);
		attributeGetterFunctions.put("bkr", Issuer::getBkr);
		attributeSetterBiConsumers.put(
			"bkr", (BiConsumer<Issuer, Integer>)Issuer::setBkr);
		attributeGetterFunctions.put("tel_nummer", Issuer::getTel_nummer);
		attributeSetterBiConsumers.put(
			"tel_nummer", (BiConsumer<Issuer, String>)Issuer::setTel_nummer);
		attributeGetterFunctions.put("beschrijving", Issuer::getBeschrijving);
		attributeSetterBiConsumers.put(
			"beschrijving",
			(BiConsumer<Issuer, String>)Issuer::setBeschrijving);
		attributeGetterFunctions.put("gecontroleerd", Issuer::getGecontroleerd);
		attributeSetterBiConsumers.put(
			"gecontroleerd",
			(BiConsumer<Issuer, Boolean>)Issuer::setGecontroleerd);
		attributeGetterFunctions.put("geaccepteerd", Issuer::getGeaccepteerd);
		attributeSetterBiConsumers.put(
			"geaccepteerd",
			(BiConsumer<Issuer, Boolean>)Issuer::setGeaccepteerd);
		attributeGetterFunctions.put("reden", Issuer::getReden);
		attributeSetterBiConsumers.put(
			"reden", (BiConsumer<Issuer, String>)Issuer::setReden);

		_attributeGetterFunctions = Collections.unmodifiableMap(
			attributeGetterFunctions);
		_attributeSetterBiConsumers = Collections.unmodifiableMap(
			(Map)attributeSetterBiConsumers);
	}

	@JSON
	@Override
	public int getIssuerId() {
		return _issuerId;
	}

	@Override
	public void setIssuerId(int issuerId) {
		_issuerId = issuerId;
	}

	@JSON
	@Override
	public String getVoornaam() {
		if (_voornaam == null) {
			return "";
		}
		else {
			return _voornaam;
		}
	}

	@Override
	public void setVoornaam(String voornaam) {
		_voornaam = voornaam;
	}

	@JSON
	@Override
	public String getAchternaam() {
		if (_achternaam == null) {
			return "";
		}
		else {
			return _achternaam;
		}
	}

	@Override
	public void setAchternaam(String achternaam) {
		_achternaam = achternaam;
	}

	@JSON
	@Override
	public String getBedrijfsnaam() {
		if (_bedrijfsnaam == null) {
			return "";
		}
		else {
			return _bedrijfsnaam;
		}
	}

	@Override
	public void setBedrijfsnaam(String bedrijfsnaam) {
		_bedrijfsnaam = bedrijfsnaam;
	}

	@JSON
	@Override
	public String getWoonplaats() {
		if (_woonplaats == null) {
			return "";
		}
		else {
			return _woonplaats;
		}
	}

	@Override
	public void setWoonplaats(String woonplaats) {
		_woonplaats = woonplaats;
	}

	@JSON
	@Override
	public String getEmail() {
		if (_email == null) {
			return "";
		}
		else {
			return _email;
		}
	}

	@Override
	public void setEmail(String email) {
		_email = email;
	}

	@JSON
	@Override
	public int getKvk() {
		return _kvk;
	}

	@Override
	public void setKvk(int kvk) {
		_kvk = kvk;
	}

	@JSON
	@Override
	public int getVog() {
		return _vog;
	}

	@Override
	public void setVog(int vog) {
		_vog = vog;
	}

	@JSON
	@Override
	public int getBkr() {
		return _bkr;
	}

	@Override
	public void setBkr(int bkr) {
		_bkr = bkr;
	}

	@JSON
	@Override
	public String getTel_nummer() {
		if (_tel_nummer == null) {
			return "";
		}
		else {
			return _tel_nummer;
		}
	}

	@Override
	public void setTel_nummer(String tel_nummer) {
		_tel_nummer = tel_nummer;
	}

	@JSON
	@Override
	public String getBeschrijving() {
		if (_beschrijving == null) {
			return "";
		}
		else {
			return _beschrijving;
		}
	}

	@Override
	public void setBeschrijving(String beschrijving) {
		_beschrijving = beschrijving;
	}

	@JSON
	@Override
	public boolean getGecontroleerd() {
		return _gecontroleerd;
	}

	@JSON
	@Override
	public boolean isGecontroleerd() {
		return _gecontroleerd;
	}

	@Override
	public void setGecontroleerd(boolean gecontroleerd) {
		_gecontroleerd = gecontroleerd;
	}

	@JSON
	@Override
	public boolean getGeaccepteerd() {
		return _geaccepteerd;
	}

	@JSON
	@Override
	public boolean isGeaccepteerd() {
		return _geaccepteerd;
	}

	@Override
	public void setGeaccepteerd(boolean geaccepteerd) {
		_geaccepteerd = geaccepteerd;
	}

	@JSON
	@Override
	public String getReden() {
		if (_reden == null) {
			return "";
		}
		else {
			return _reden;
		}
	}

	@Override
	public void setReden(String reden) {
		_reden = reden;
	}

	@Override
	public Issuer toEscapedModel() {
		if (_escapedModel == null) {
			Function<InvocationHandler, Issuer>
				escapedModelProxyProviderFunction =
					EscapedModelProxyProviderFunctionHolder.
						_escapedModelProxyProviderFunction;

			_escapedModel = escapedModelProxyProviderFunction.apply(
				new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		IssuerImpl issuerImpl = new IssuerImpl();

		issuerImpl.setIssuerId(getIssuerId());
		issuerImpl.setVoornaam(getVoornaam());
		issuerImpl.setAchternaam(getAchternaam());
		issuerImpl.setBedrijfsnaam(getBedrijfsnaam());
		issuerImpl.setWoonplaats(getWoonplaats());
		issuerImpl.setEmail(getEmail());
		issuerImpl.setKvk(getKvk());
		issuerImpl.setVog(getVog());
		issuerImpl.setBkr(getBkr());
		issuerImpl.setTel_nummer(getTel_nummer());
		issuerImpl.setBeschrijving(getBeschrijving());
		issuerImpl.setGecontroleerd(isGecontroleerd());
		issuerImpl.setGeaccepteerd(isGeaccepteerd());
		issuerImpl.setReden(getReden());

		issuerImpl.resetOriginalValues();

		return issuerImpl;
	}

	@Override
	public int compareTo(Issuer issuer) {
		int primaryKey = issuer.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Issuer)) {
			return false;
		}

		Issuer issuer = (Issuer)obj;

		int primaryKey = issuer.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _entityCacheEnabled;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _finderCacheEnabled;
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<Issuer> toCacheModel() {
		IssuerCacheModel issuerCacheModel = new IssuerCacheModel();

		issuerCacheModel.issuerId = getIssuerId();

		issuerCacheModel.voornaam = getVoornaam();

		String voornaam = issuerCacheModel.voornaam;

		if ((voornaam != null) && (voornaam.length() == 0)) {
			issuerCacheModel.voornaam = null;
		}

		issuerCacheModel.achternaam = getAchternaam();

		String achternaam = issuerCacheModel.achternaam;

		if ((achternaam != null) && (achternaam.length() == 0)) {
			issuerCacheModel.achternaam = null;
		}

		issuerCacheModel.bedrijfsnaam = getBedrijfsnaam();

		String bedrijfsnaam = issuerCacheModel.bedrijfsnaam;

		if ((bedrijfsnaam != null) && (bedrijfsnaam.length() == 0)) {
			issuerCacheModel.bedrijfsnaam = null;
		}

		issuerCacheModel.woonplaats = getWoonplaats();

		String woonplaats = issuerCacheModel.woonplaats;

		if ((woonplaats != null) && (woonplaats.length() == 0)) {
			issuerCacheModel.woonplaats = null;
		}

		issuerCacheModel.email = getEmail();

		String email = issuerCacheModel.email;

		if ((email != null) && (email.length() == 0)) {
			issuerCacheModel.email = null;
		}

		issuerCacheModel.kvk = getKvk();

		issuerCacheModel.vog = getVog();

		issuerCacheModel.bkr = getBkr();

		issuerCacheModel.tel_nummer = getTel_nummer();

		String tel_nummer = issuerCacheModel.tel_nummer;

		if ((tel_nummer != null) && (tel_nummer.length() == 0)) {
			issuerCacheModel.tel_nummer = null;
		}

		issuerCacheModel.beschrijving = getBeschrijving();

		String beschrijving = issuerCacheModel.beschrijving;

		if ((beschrijving != null) && (beschrijving.length() == 0)) {
			issuerCacheModel.beschrijving = null;
		}

		issuerCacheModel.gecontroleerd = isGecontroleerd();

		issuerCacheModel.geaccepteerd = isGeaccepteerd();

		issuerCacheModel.reden = getReden();

		String reden = issuerCacheModel.reden;

		if ((reden != null) && (reden.length() == 0)) {
			issuerCacheModel.reden = null;
		}

		return issuerCacheModel;
	}

	@Override
	public String toString() {
		Map<String, Function<Issuer, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			4 * attributeGetterFunctions.size() + 2);

		sb.append("{");

		for (Map.Entry<String, Function<Issuer, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<Issuer, Object> attributeGetterFunction = entry.getValue();

			sb.append(attributeName);
			sb.append("=");
			sb.append(attributeGetterFunction.apply((Issuer)this));
			sb.append(", ");
		}

		if (sb.index() > 1) {
			sb.setIndex(sb.index() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		Map<String, Function<Issuer, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			5 * attributeGetterFunctions.size() + 4);

		sb.append("<model><model-name>");
		sb.append(getModelClassName());
		sb.append("</model-name>");

		for (Map.Entry<String, Function<Issuer, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<Issuer, Object> attributeGetterFunction = entry.getValue();

			sb.append("<column><column-name>");
			sb.append(attributeName);
			sb.append("</column-name><column-value><![CDATA[");
			sb.append(attributeGetterFunction.apply((Issuer)this));
			sb.append("]]></column-value></column>");
		}

		sb.append("</model>");

		return sb.toString();
	}

	private static class EscapedModelProxyProviderFunctionHolder {

		private static final Function<InvocationHandler, Issuer>
			_escapedModelProxyProviderFunction = _getProxyProviderFunction();

	}

	private static boolean _entityCacheEnabled;
	private static boolean _finderCacheEnabled;

	private int _issuerId;
	private String _voornaam;
	private String _achternaam;
	private String _bedrijfsnaam;
	private String _woonplaats;
	private String _email;
	private int _kvk;
	private int _vog;
	private int _bkr;
	private String _tel_nummer;
	private String _beschrijving;
	private boolean _gecontroleerd;
	private boolean _geaccepteerd;
	private String _reden;
	private Issuer _escapedModel;

}