/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import database.model.Investeringsplan;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Investeringsplan in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class InvesteringsplanCacheModel
	implements CacheModel<Investeringsplan>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InvesteringsplanCacheModel)) {
			return false;
		}

		InvesteringsplanCacheModel investeringsplanCacheModel =
			(InvesteringsplanCacheModel)obj;

		if (investeringsplanId ==
				investeringsplanCacheModel.investeringsplanId) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, investeringsplanId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{investeringsplanId=");
		sb.append(investeringsplanId);
		sb.append(", issuerId=");
		sb.append(issuerId);
		sb.append(", naam=");
		sb.append(naam);
		sb.append(", status=");
		sb.append(status);
		sb.append(", voortgang=");
		sb.append(voortgang);
		sb.append(", doel=");
		sb.append(doel);
		sb.append(", beschrijving=");
		sb.append(beschrijving);
		sb.append(", begindatum=");
		sb.append(begindatum);
		sb.append(", gecontroleerd=");
		sb.append(gecontroleerd);
		sb.append(", geaccepteerd=");
		sb.append(geaccepteerd);
		sb.append(", reden=");
		sb.append(reden);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Investeringsplan toEntityModel() {
		InvesteringsplanImpl investeringsplanImpl = new InvesteringsplanImpl();

		investeringsplanImpl.setInvesteringsplanId(investeringsplanId);
		investeringsplanImpl.setIssuerId(issuerId);

		if (naam == null) {
			investeringsplanImpl.setNaam("");
		}
		else {
			investeringsplanImpl.setNaam(naam);
		}

		if (status == null) {
			investeringsplanImpl.setStatus("");
		}
		else {
			investeringsplanImpl.setStatus(status);
		}

		investeringsplanImpl.setVoortgang(voortgang);
		investeringsplanImpl.setDoel(doel);

		if (beschrijving == null) {
			investeringsplanImpl.setBeschrijving("");
		}
		else {
			investeringsplanImpl.setBeschrijving(beschrijving);
		}

		if (begindatum == Long.MIN_VALUE) {
			investeringsplanImpl.setBegindatum(null);
		}
		else {
			investeringsplanImpl.setBegindatum(new Date(begindatum));
		}

		investeringsplanImpl.setGecontroleerd(gecontroleerd);
		investeringsplanImpl.setGeaccepteerd(geaccepteerd);

		if (reden == null) {
			investeringsplanImpl.setReden("");
		}
		else {
			investeringsplanImpl.setReden(reden);
		}

		investeringsplanImpl.resetOriginalValues();

		return investeringsplanImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		investeringsplanId = objectInput.readInt();

		issuerId = objectInput.readInt();
		naam = objectInput.readUTF();
		status = objectInput.readUTF();

		voortgang = objectInput.readInt();

		doel = objectInput.readInt();
		beschrijving = objectInput.readUTF();
		begindatum = objectInput.readLong();

		gecontroleerd = objectInput.readBoolean();

		geaccepteerd = objectInput.readBoolean();
		reden = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeInt(investeringsplanId);

		objectOutput.writeInt(issuerId);

		if (naam == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(naam);
		}

		if (status == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(status);
		}

		objectOutput.writeInt(voortgang);

		objectOutput.writeInt(doel);

		if (beschrijving == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(beschrijving);
		}

		objectOutput.writeLong(begindatum);

		objectOutput.writeBoolean(gecontroleerd);

		objectOutput.writeBoolean(geaccepteerd);

		if (reden == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(reden);
		}
	}

	public int investeringsplanId;
	public int issuerId;
	public String naam;
	public String status;
	public int voortgang;
	public int doel;
	public String beschrijving;
	public long begindatum;
	public boolean gecontroleerd;
	public boolean geaccepteerd;
	public String reden;

}