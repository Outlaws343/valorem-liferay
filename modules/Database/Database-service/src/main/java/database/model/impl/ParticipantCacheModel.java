/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import database.model.Participant;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Participant in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ParticipantCacheModel
	implements CacheModel<Participant>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParticipantCacheModel)) {
			return false;
		}

		ParticipantCacheModel participantCacheModel =
			(ParticipantCacheModel)obj;

		if (participantId == participantCacheModel.participantId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, participantId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{participantId=");
		sb.append(participantId);
		sb.append(", voornaam=");
		sb.append(voornaam);
		sb.append(", achternaam=");
		sb.append(achternaam);
		sb.append(", woonplaats=");
		sb.append(woonplaats);
		sb.append(", adres=");
		sb.append(adres);
		sb.append(", bsn=");
		sb.append(bsn);
		sb.append(", email=");
		sb.append(email);
		sb.append(", tel_nummer=");
		sb.append(tel_nummer);
		sb.append(", verificatie=");
		sb.append(verificatie);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Participant toEntityModel() {
		ParticipantImpl participantImpl = new ParticipantImpl();

		participantImpl.setParticipantId(participantId);

		if (voornaam == null) {
			participantImpl.setVoornaam("");
		}
		else {
			participantImpl.setVoornaam(voornaam);
		}

		if (achternaam == null) {
			participantImpl.setAchternaam("");
		}
		else {
			participantImpl.setAchternaam(achternaam);
		}

		if (woonplaats == null) {
			participantImpl.setWoonplaats("");
		}
		else {
			participantImpl.setWoonplaats(woonplaats);
		}

		if (adres == null) {
			participantImpl.setAdres("");
		}
		else {
			participantImpl.setAdres(adres);
		}

		participantImpl.setBsn(bsn);

		if (email == null) {
			participantImpl.setEmail("");
		}
		else {
			participantImpl.setEmail(email);
		}

		if (tel_nummer == null) {
			participantImpl.setTel_nummer("");
		}
		else {
			participantImpl.setTel_nummer(tel_nummer);
		}

		participantImpl.setVerificatie(verificatie);

		participantImpl.resetOriginalValues();

		return participantImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		participantId = objectInput.readInt();
		voornaam = objectInput.readUTF();
		achternaam = objectInput.readUTF();
		woonplaats = objectInput.readUTF();
		adres = objectInput.readUTF();

		bsn = objectInput.readInt();
		email = objectInput.readUTF();
		tel_nummer = objectInput.readUTF();

		verificatie = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeInt(participantId);

		if (voornaam == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(voornaam);
		}

		if (achternaam == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(achternaam);
		}

		if (woonplaats == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(woonplaats);
		}

		if (adres == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(adres);
		}

		objectOutput.writeInt(bsn);

		if (email == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (tel_nummer == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(tel_nummer);
		}

		objectOutput.writeBoolean(verificatie);
	}

	public int participantId;
	public String voornaam;
	public String achternaam;
	public String woonplaats;
	public String adres;
	public int bsn;
	public String email;
	public String tel_nummer;
	public boolean verificatie;

}