/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import database.model.Milestone;

import database.service.persistence.MilestonePK;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Milestone in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MilestoneCacheModel
	implements CacheModel<Milestone>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MilestoneCacheModel)) {
			return false;
		}

		MilestoneCacheModel milestoneCacheModel = (MilestoneCacheModel)obj;

		if (milestonePK.equals(milestoneCacheModel.milestonePK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, milestonePK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{investeringsplanId=");
		sb.append(investeringsplanId);
		sb.append(", naam=");
		sb.append(naam);
		sb.append(", milestone_percentage=");
		sb.append(milestone_percentage);
		sb.append(", einddatum=");
		sb.append(einddatum);
		sb.append(", beschrijving=");
		sb.append(beschrijving);
		sb.append(", gecontroleerd=");
		sb.append(gecontroleerd);
		sb.append(", geaccepteerd=");
		sb.append(geaccepteerd);
		sb.append(", inleverings_beschrijving=");
		sb.append(inleverings_beschrijving);
		sb.append(", reden=");
		sb.append(reden);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Milestone toEntityModel() {
		MilestoneImpl milestoneImpl = new MilestoneImpl();

		milestoneImpl.setInvesteringsplanId(investeringsplanId);

		if (naam == null) {
			milestoneImpl.setNaam("");
		}
		else {
			milestoneImpl.setNaam(naam);
		}

		milestoneImpl.setMilestone_percentage(milestone_percentage);

		if (einddatum == Long.MIN_VALUE) {
			milestoneImpl.setEinddatum(null);
		}
		else {
			milestoneImpl.setEinddatum(new Date(einddatum));
		}

		if (beschrijving == null) {
			milestoneImpl.setBeschrijving("");
		}
		else {
			milestoneImpl.setBeschrijving(beschrijving);
		}

		milestoneImpl.setGecontroleerd(gecontroleerd);
		milestoneImpl.setGeaccepteerd(geaccepteerd);

		if (inleverings_beschrijving == null) {
			milestoneImpl.setInleverings_beschrijving("");
		}
		else {
			milestoneImpl.setInleverings_beschrijving(inleverings_beschrijving);
		}

		if (reden == null) {
			milestoneImpl.setReden("");
		}
		else {
			milestoneImpl.setReden(reden);
		}

		milestoneImpl.resetOriginalValues();

		return milestoneImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		investeringsplanId = objectInput.readInt();
		naam = objectInput.readUTF();

		milestone_percentage = objectInput.readInt();
		einddatum = objectInput.readLong();
		beschrijving = objectInput.readUTF();

		gecontroleerd = objectInput.readBoolean();

		geaccepteerd = objectInput.readBoolean();
		inleverings_beschrijving = objectInput.readUTF();
		reden = objectInput.readUTF();

		milestonePK = new MilestonePK(investeringsplanId, naam);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeInt(investeringsplanId);

		if (naam == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(naam);
		}

		objectOutput.writeInt(milestone_percentage);
		objectOutput.writeLong(einddatum);

		if (beschrijving == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(beschrijving);
		}

		objectOutput.writeBoolean(gecontroleerd);

		objectOutput.writeBoolean(geaccepteerd);

		if (inleverings_beschrijving == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(inleverings_beschrijving);
		}

		if (reden == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(reden);
		}
	}

	public int investeringsplanId;
	public String naam;
	public int milestone_percentage;
	public long einddatum;
	public String beschrijving;
	public boolean gecontroleerd;
	public boolean geaccepteerd;
	public String inleverings_beschrijving;
	public String reden;
	public transient MilestonePK milestonePK;

}