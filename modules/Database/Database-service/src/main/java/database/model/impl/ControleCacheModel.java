/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import database.model.Controle;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Controle in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ControleCacheModel
	implements CacheModel<Controle>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ControleCacheModel)) {
			return false;
		}

		ControleCacheModel controleCacheModel = (ControleCacheModel)obj;

		if (issuerId == controleCacheModel.issuerId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, issuerId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{issuerId=");
		sb.append(issuerId);
		sb.append(", akkoord=");
		sb.append(akkoord);
		sb.append(", reden=");
		sb.append(reden);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Controle toEntityModel() {
		ControleImpl controleImpl = new ControleImpl();

		controleImpl.setIssuerId(issuerId);
		controleImpl.setAkkoord(akkoord);

		if (reden == null) {
			controleImpl.setReden("");
		}
		else {
			controleImpl.setReden(reden);
		}

		controleImpl.resetOriginalValues();

		return controleImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		issuerId = objectInput.readInt();

		akkoord = objectInput.readBoolean();
		reden = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeInt(issuerId);

		objectOutput.writeBoolean(akkoord);

		if (reden == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(reden);
		}
	}

	public int issuerId;
	public boolean akkoord;
	public String reden;

}