/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import database.model.Issuer;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Issuer in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class IssuerCacheModel implements CacheModel<Issuer>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IssuerCacheModel)) {
			return false;
		}

		IssuerCacheModel issuerCacheModel = (IssuerCacheModel)obj;

		if (issuerId == issuerCacheModel.issuerId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, issuerId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{issuerId=");
		sb.append(issuerId);
		sb.append(", voornaam=");
		sb.append(voornaam);
		sb.append(", achternaam=");
		sb.append(achternaam);
		sb.append(", bedrijfsnaam=");
		sb.append(bedrijfsnaam);
		sb.append(", woonplaats=");
		sb.append(woonplaats);
		sb.append(", email=");
		sb.append(email);
		sb.append(", kvk=");
		sb.append(kvk);
		sb.append(", vog=");
		sb.append(vog);
		sb.append(", bkr=");
		sb.append(bkr);
		sb.append(", tel_nummer=");
		sb.append(tel_nummer);
		sb.append(", beschrijving=");
		sb.append(beschrijving);
		sb.append(", gecontroleerd=");
		sb.append(gecontroleerd);
		sb.append(", geaccepteerd=");
		sb.append(geaccepteerd);
		sb.append(", reden=");
		sb.append(reden);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Issuer toEntityModel() {
		IssuerImpl issuerImpl = new IssuerImpl();

		issuerImpl.setIssuerId(issuerId);

		if (voornaam == null) {
			issuerImpl.setVoornaam("");
		}
		else {
			issuerImpl.setVoornaam(voornaam);
		}

		if (achternaam == null) {
			issuerImpl.setAchternaam("");
		}
		else {
			issuerImpl.setAchternaam(achternaam);
		}

		if (bedrijfsnaam == null) {
			issuerImpl.setBedrijfsnaam("");
		}
		else {
			issuerImpl.setBedrijfsnaam(bedrijfsnaam);
		}

		if (woonplaats == null) {
			issuerImpl.setWoonplaats("");
		}
		else {
			issuerImpl.setWoonplaats(woonplaats);
		}

		if (email == null) {
			issuerImpl.setEmail("");
		}
		else {
			issuerImpl.setEmail(email);
		}

		issuerImpl.setKvk(kvk);
		issuerImpl.setVog(vog);
		issuerImpl.setBkr(bkr);

		if (tel_nummer == null) {
			issuerImpl.setTel_nummer("");
		}
		else {
			issuerImpl.setTel_nummer(tel_nummer);
		}

		if (beschrijving == null) {
			issuerImpl.setBeschrijving("");
		}
		else {
			issuerImpl.setBeschrijving(beschrijving);
		}

		issuerImpl.setGecontroleerd(gecontroleerd);
		issuerImpl.setGeaccepteerd(geaccepteerd);

		if (reden == null) {
			issuerImpl.setReden("");
		}
		else {
			issuerImpl.setReden(reden);
		}

		issuerImpl.resetOriginalValues();

		return issuerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		issuerId = objectInput.readInt();
		voornaam = objectInput.readUTF();
		achternaam = objectInput.readUTF();
		bedrijfsnaam = objectInput.readUTF();
		woonplaats = objectInput.readUTF();
		email = objectInput.readUTF();

		kvk = objectInput.readInt();

		vog = objectInput.readInt();

		bkr = objectInput.readInt();
		tel_nummer = objectInput.readUTF();
		beschrijving = objectInput.readUTF();

		gecontroleerd = objectInput.readBoolean();

		geaccepteerd = objectInput.readBoolean();
		reden = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeInt(issuerId);

		if (voornaam == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(voornaam);
		}

		if (achternaam == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(achternaam);
		}

		if (bedrijfsnaam == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bedrijfsnaam);
		}

		if (woonplaats == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(woonplaats);
		}

		if (email == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(email);
		}

		objectOutput.writeInt(kvk);

		objectOutput.writeInt(vog);

		objectOutput.writeInt(bkr);

		if (tel_nummer == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(tel_nummer);
		}

		if (beschrijving == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(beschrijving);
		}

		objectOutput.writeBoolean(gecontroleerd);

		objectOutput.writeBoolean(geaccepteerd);

		if (reden == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(reden);
		}
	}

	public int issuerId;
	public String voornaam;
	public String achternaam;
	public String bedrijfsnaam;
	public String woonplaats;
	public String email;
	public int kvk;
	public int vog;
	public int bkr;
	public String tel_nummer;
	public String beschrijving;
	public boolean gecontroleerd;
	public boolean geaccepteerd;
	public String reden;

}