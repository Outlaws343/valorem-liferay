/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.model.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.util.ProxyUtil;

import database.model.Milestone;
import database.model.MilestoneModel;
import database.model.MilestoneSoap;

import database.service.persistence.MilestonePK;

import java.io.Serializable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * The base model implementation for the Milestone service. Represents a row in the &quot;Milestone&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface <code>MilestoneModel</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link MilestoneImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MilestoneImpl
 * @generated
 */
@JSON(strict = true)
public class MilestoneModelImpl
	extends BaseModelImpl<Milestone> implements MilestoneModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a milestone model instance should use the <code>Milestone</code> interface instead.
	 */
	public static final String TABLE_NAME = "Milestone";

	public static final Object[][] TABLE_COLUMNS = {
		{"investeringsplanId", Types.INTEGER}, {"naam", Types.VARCHAR},
		{"milestone_percentage", Types.INTEGER}, {"einddatum", Types.TIMESTAMP},
		{"beschrijving", Types.VARCHAR}, {"gecontroleerd", Types.BOOLEAN},
		{"geaccepteerd", Types.BOOLEAN},
		{"inleverings_beschrijving", Types.VARCHAR}, {"reden", Types.VARCHAR}
	};

	public static final Map<String, Integer> TABLE_COLUMNS_MAP =
		new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("investeringsplanId", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("naam", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("milestone_percentage", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("einddatum", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("beschrijving", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("gecontroleerd", Types.BOOLEAN);
		TABLE_COLUMNS_MAP.put("geaccepteerd", Types.BOOLEAN);
		TABLE_COLUMNS_MAP.put("inleverings_beschrijving", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("reden", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE =
		"create table Milestone (investeringsplanId INTEGER not null,naam VARCHAR(75) not null,milestone_percentage INTEGER,einddatum DATE null,beschrijving VARCHAR(200) null,gecontroleerd BOOLEAN,geaccepteerd BOOLEAN,inleverings_beschrijving VARCHAR(75) null,reden VARCHAR(200) null,primary key (investeringsplanId, naam))";

	public static final String TABLE_SQL_DROP = "drop table Milestone";

	public static final String ORDER_BY_JPQL =
		" ORDER BY milestone.id.investeringsplanId ASC, milestone.id.naam ASC";

	public static final String ORDER_BY_SQL =
		" ORDER BY Milestone.investeringsplanId ASC, Milestone.naam ASC";

	public static final String DATA_SOURCE = "liferayDataSource";

	public static final String SESSION_FACTORY = "liferaySessionFactory";

	public static final String TX_MANAGER = "liferayTransactionManager";

	public static void setEntityCacheEnabled(boolean entityCacheEnabled) {
		_entityCacheEnabled = entityCacheEnabled;
	}

	public static void setFinderCacheEnabled(boolean finderCacheEnabled) {
		_finderCacheEnabled = finderCacheEnabled;
	}

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static Milestone toModel(MilestoneSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		Milestone model = new MilestoneImpl();

		model.setInvesteringsplanId(soapModel.getInvesteringsplanId());
		model.setNaam(soapModel.getNaam());
		model.setMilestone_percentage(soapModel.getMilestone_percentage());
		model.setEinddatum(soapModel.getEinddatum());
		model.setBeschrijving(soapModel.getBeschrijving());
		model.setGecontroleerd(soapModel.isGecontroleerd());
		model.setGeaccepteerd(soapModel.isGeaccepteerd());
		model.setInleverings_beschrijving(
			soapModel.getInleverings_beschrijving());
		model.setReden(soapModel.getReden());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<Milestone> toModels(MilestoneSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<Milestone> models = new ArrayList<Milestone>(soapModels.length);

		for (MilestoneSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public MilestoneModelImpl() {
	}

	@Override
	public MilestonePK getPrimaryKey() {
		return new MilestonePK(_investeringsplanId, _naam);
	}

	@Override
	public void setPrimaryKey(MilestonePK primaryKey) {
		setInvesteringsplanId(primaryKey.investeringsplanId);
		setNaam(primaryKey.naam);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new MilestonePK(_investeringsplanId, _naam);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((MilestonePK)primaryKeyObj);
	}

	@Override
	public Class<?> getModelClass() {
		return Milestone.class;
	}

	@Override
	public String getModelClassName() {
		return Milestone.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		Map<String, Function<Milestone, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		for (Map.Entry<String, Function<Milestone, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<Milestone, Object> attributeGetterFunction =
				entry.getValue();

			attributes.put(
				attributeName, attributeGetterFunction.apply((Milestone)this));
		}

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Map<String, BiConsumer<Milestone, Object>> attributeSetterBiConsumers =
			getAttributeSetterBiConsumers();

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			String attributeName = entry.getKey();

			BiConsumer<Milestone, Object> attributeSetterBiConsumer =
				attributeSetterBiConsumers.get(attributeName);

			if (attributeSetterBiConsumer != null) {
				attributeSetterBiConsumer.accept(
					(Milestone)this, entry.getValue());
			}
		}
	}

	public Map<String, Function<Milestone, Object>>
		getAttributeGetterFunctions() {

		return _attributeGetterFunctions;
	}

	public Map<String, BiConsumer<Milestone, Object>>
		getAttributeSetterBiConsumers() {

		return _attributeSetterBiConsumers;
	}

	private static Function<InvocationHandler, Milestone>
		_getProxyProviderFunction() {

		Class<?> proxyClass = ProxyUtil.getProxyClass(
			Milestone.class.getClassLoader(), Milestone.class,
			ModelWrapper.class);

		try {
			Constructor<Milestone> constructor =
				(Constructor<Milestone>)proxyClass.getConstructor(
					InvocationHandler.class);

			return invocationHandler -> {
				try {
					return constructor.newInstance(invocationHandler);
				}
				catch (ReflectiveOperationException
							reflectiveOperationException) {

					throw new InternalError(reflectiveOperationException);
				}
			};
		}
		catch (NoSuchMethodException noSuchMethodException) {
			throw new InternalError(noSuchMethodException);
		}
	}

	private static final Map<String, Function<Milestone, Object>>
		_attributeGetterFunctions;
	private static final Map<String, BiConsumer<Milestone, Object>>
		_attributeSetterBiConsumers;

	static {
		Map<String, Function<Milestone, Object>> attributeGetterFunctions =
			new LinkedHashMap<String, Function<Milestone, Object>>();
		Map<String, BiConsumer<Milestone, ?>> attributeSetterBiConsumers =
			new LinkedHashMap<String, BiConsumer<Milestone, ?>>();

		attributeGetterFunctions.put(
			"investeringsplanId", Milestone::getInvesteringsplanId);
		attributeSetterBiConsumers.put(
			"investeringsplanId",
			(BiConsumer<Milestone, Integer>)Milestone::setInvesteringsplanId);
		attributeGetterFunctions.put("naam", Milestone::getNaam);
		attributeSetterBiConsumers.put(
			"naam", (BiConsumer<Milestone, String>)Milestone::setNaam);
		attributeGetterFunctions.put(
			"milestone_percentage", Milestone::getMilestone_percentage);
		attributeSetterBiConsumers.put(
			"milestone_percentage",
			(BiConsumer<Milestone, Integer>)Milestone::setMilestone_percentage);
		attributeGetterFunctions.put("einddatum", Milestone::getEinddatum);
		attributeSetterBiConsumers.put(
			"einddatum", (BiConsumer<Milestone, Date>)Milestone::setEinddatum);
		attributeGetterFunctions.put(
			"beschrijving", Milestone::getBeschrijving);
		attributeSetterBiConsumers.put(
			"beschrijving",
			(BiConsumer<Milestone, String>)Milestone::setBeschrijving);
		attributeGetterFunctions.put(
			"gecontroleerd", Milestone::getGecontroleerd);
		attributeSetterBiConsumers.put(
			"gecontroleerd",
			(BiConsumer<Milestone, Boolean>)Milestone::setGecontroleerd);
		attributeGetterFunctions.put(
			"geaccepteerd", Milestone::getGeaccepteerd);
		attributeSetterBiConsumers.put(
			"geaccepteerd",
			(BiConsumer<Milestone, Boolean>)Milestone::setGeaccepteerd);
		attributeGetterFunctions.put(
			"inleverings_beschrijving", Milestone::getInleverings_beschrijving);
		attributeSetterBiConsumers.put(
			"inleverings_beschrijving",
			(BiConsumer<Milestone, String>)
				Milestone::setInleverings_beschrijving);
		attributeGetterFunctions.put("reden", Milestone::getReden);
		attributeSetterBiConsumers.put(
			"reden", (BiConsumer<Milestone, String>)Milestone::setReden);

		_attributeGetterFunctions = Collections.unmodifiableMap(
			attributeGetterFunctions);
		_attributeSetterBiConsumers = Collections.unmodifiableMap(
			(Map)attributeSetterBiConsumers);
	}

	@JSON
	@Override
	public int getInvesteringsplanId() {
		return _investeringsplanId;
	}

	@Override
	public void setInvesteringsplanId(int investeringsplanId) {
		_investeringsplanId = investeringsplanId;
	}

	@JSON
	@Override
	public String getNaam() {
		if (_naam == null) {
			return "";
		}
		else {
			return _naam;
		}
	}

	@Override
	public void setNaam(String naam) {
		_naam = naam;
	}

	@JSON
	@Override
	public int getMilestone_percentage() {
		return _milestone_percentage;
	}

	@Override
	public void setMilestone_percentage(int milestone_percentage) {
		_milestone_percentage = milestone_percentage;
	}

	@JSON
	@Override
	public Date getEinddatum() {
		return _einddatum;
	}

	@Override
	public void setEinddatum(Date einddatum) {
		_einddatum = einddatum;
	}

	@JSON
	@Override
	public String getBeschrijving() {
		if (_beschrijving == null) {
			return "";
		}
		else {
			return _beschrijving;
		}
	}

	@Override
	public void setBeschrijving(String beschrijving) {
		_beschrijving = beschrijving;
	}

	@JSON
	@Override
	public boolean getGecontroleerd() {
		return _gecontroleerd;
	}

	@JSON
	@Override
	public boolean isGecontroleerd() {
		return _gecontroleerd;
	}

	@Override
	public void setGecontroleerd(boolean gecontroleerd) {
		_gecontroleerd = gecontroleerd;
	}

	@JSON
	@Override
	public boolean getGeaccepteerd() {
		return _geaccepteerd;
	}

	@JSON
	@Override
	public boolean isGeaccepteerd() {
		return _geaccepteerd;
	}

	@Override
	public void setGeaccepteerd(boolean geaccepteerd) {
		_geaccepteerd = geaccepteerd;
	}

	@JSON
	@Override
	public String getInleverings_beschrijving() {
		if (_inleverings_beschrijving == null) {
			return "";
		}
		else {
			return _inleverings_beschrijving;
		}
	}

	@Override
	public void setInleverings_beschrijving(String inleverings_beschrijving) {
		_inleverings_beschrijving = inleverings_beschrijving;
	}

	@JSON
	@Override
	public String getReden() {
		if (_reden == null) {
			return "";
		}
		else {
			return _reden;
		}
	}

	@Override
	public void setReden(String reden) {
		_reden = reden;
	}

	@Override
	public Milestone toEscapedModel() {
		if (_escapedModel == null) {
			Function<InvocationHandler, Milestone>
				escapedModelProxyProviderFunction =
					EscapedModelProxyProviderFunctionHolder.
						_escapedModelProxyProviderFunction;

			_escapedModel = escapedModelProxyProviderFunction.apply(
				new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		MilestoneImpl milestoneImpl = new MilestoneImpl();

		milestoneImpl.setInvesteringsplanId(getInvesteringsplanId());
		milestoneImpl.setNaam(getNaam());
		milestoneImpl.setMilestone_percentage(getMilestone_percentage());
		milestoneImpl.setEinddatum(getEinddatum());
		milestoneImpl.setBeschrijving(getBeschrijving());
		milestoneImpl.setGecontroleerd(isGecontroleerd());
		milestoneImpl.setGeaccepteerd(isGeaccepteerd());
		milestoneImpl.setInleverings_beschrijving(
			getInleverings_beschrijving());
		milestoneImpl.setReden(getReden());

		milestoneImpl.resetOriginalValues();

		return milestoneImpl;
	}

	@Override
	public int compareTo(Milestone milestone) {
		MilestonePK primaryKey = milestone.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Milestone)) {
			return false;
		}

		Milestone milestone = (Milestone)obj;

		MilestonePK primaryKey = milestone.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _entityCacheEnabled;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _finderCacheEnabled;
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<Milestone> toCacheModel() {
		MilestoneCacheModel milestoneCacheModel = new MilestoneCacheModel();

		milestoneCacheModel.milestonePK = getPrimaryKey();

		milestoneCacheModel.investeringsplanId = getInvesteringsplanId();

		milestoneCacheModel.naam = getNaam();

		String naam = milestoneCacheModel.naam;

		if ((naam != null) && (naam.length() == 0)) {
			milestoneCacheModel.naam = null;
		}

		milestoneCacheModel.milestone_percentage = getMilestone_percentage();

		Date einddatum = getEinddatum();

		if (einddatum != null) {
			milestoneCacheModel.einddatum = einddatum.getTime();
		}
		else {
			milestoneCacheModel.einddatum = Long.MIN_VALUE;
		}

		milestoneCacheModel.beschrijving = getBeschrijving();

		String beschrijving = milestoneCacheModel.beschrijving;

		if ((beschrijving != null) && (beschrijving.length() == 0)) {
			milestoneCacheModel.beschrijving = null;
		}

		milestoneCacheModel.gecontroleerd = isGecontroleerd();

		milestoneCacheModel.geaccepteerd = isGeaccepteerd();

		milestoneCacheModel.inleverings_beschrijving =
			getInleverings_beschrijving();

		String inleverings_beschrijving =
			milestoneCacheModel.inleverings_beschrijving;

		if ((inleverings_beschrijving != null) &&
			(inleverings_beschrijving.length() == 0)) {

			milestoneCacheModel.inleverings_beschrijving = null;
		}

		milestoneCacheModel.reden = getReden();

		String reden = milestoneCacheModel.reden;

		if ((reden != null) && (reden.length() == 0)) {
			milestoneCacheModel.reden = null;
		}

		return milestoneCacheModel;
	}

	@Override
	public String toString() {
		Map<String, Function<Milestone, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			4 * attributeGetterFunctions.size() + 2);

		sb.append("{");

		for (Map.Entry<String, Function<Milestone, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<Milestone, Object> attributeGetterFunction =
				entry.getValue();

			sb.append(attributeName);
			sb.append("=");
			sb.append(attributeGetterFunction.apply((Milestone)this));
			sb.append(", ");
		}

		if (sb.index() > 1) {
			sb.setIndex(sb.index() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		Map<String, Function<Milestone, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			5 * attributeGetterFunctions.size() + 4);

		sb.append("<model><model-name>");
		sb.append(getModelClassName());
		sb.append("</model-name>");

		for (Map.Entry<String, Function<Milestone, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<Milestone, Object> attributeGetterFunction =
				entry.getValue();

			sb.append("<column><column-name>");
			sb.append(attributeName);
			sb.append("</column-name><column-value><![CDATA[");
			sb.append(attributeGetterFunction.apply((Milestone)this));
			sb.append("]]></column-value></column>");
		}

		sb.append("</model>");

		return sb.toString();
	}

	private static class EscapedModelProxyProviderFunctionHolder {

		private static final Function<InvocationHandler, Milestone>
			_escapedModelProxyProviderFunction = _getProxyProviderFunction();

	}

	private static boolean _entityCacheEnabled;
	private static boolean _finderCacheEnabled;

	private int _investeringsplanId;
	private String _naam;
	private int _milestone_percentage;
	private Date _einddatum;
	private String _beschrijving;
	private boolean _gecontroleerd;
	private boolean _geaccepteerd;
	private String _inleverings_beschrijving;
	private String _reden;
	private Milestone _escapedModel;

}