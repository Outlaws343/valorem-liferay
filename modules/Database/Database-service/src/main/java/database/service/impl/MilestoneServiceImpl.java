/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.service.persistence.UserUtil;
import database.model.Milestone;
import database.service.base.MilestoneServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the milestone remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>database.service.MilestoneService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MilestoneServiceBaseImpl
 */
@Component(
		property = {
				"json.web.service.context.name=milestone",
				"json.web.service.context.path=Milestone"
		},
		service = AopService.class
)
public class MilestoneServiceImpl extends MilestoneServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>database.service.MilestoneServiceUtil</code> to access the milestone remote service.
	 */
	@JSONWebService(method = "GET")
	public List<Milestone> getMilestonesByInvestmentPlanId(int investmentPlanId){
		return milestoneFinder.getMilestonesByInvestmentPlanId(investmentPlanId);
	}

	@JSONWebService(method = "POST")
	public Milestone addMilestone(String naam, int investeringsplanId, Date eindDatum, int percentage, String beschrijving){
		return milestoneFinder.addMilestone(naam, investeringsplanId, eindDatum, percentage, beschrijving);
	}

	@JSONWebService(method = "GET")
	public List<Milestone> getNotCheckedMilestones(){
		return milestoneFinder.getNotCheckedMilestones();
	}

	@JSONWebService(method = "GET")
	public List<Milestone> getAcceptedMilestones(){
		return milestoneFinder.getAcceptedMilestones();
	}

	@JSONWebService(method = "GET")
	public List<Milestone> getDeniedMilestones(){
		return milestoneFinder.getDeniedMilestones();
	}

	@JSONWebService(method = "UPDATE")
	public Milestone updateMilestone(int investeringsplanId, String naam, boolean geaccepteerd, String reden){
		return milestoneFinder.updateMilestone(investeringsplanId, naam, geaccepteerd, reden);
	}

	@JSONWebService(method = "UPDATE")
	public Milestone completeNextMilestone(int investeringsplanId, String beschrijving){
		return milestoneFinder.completeNextMilestone(investeringsplanId, beschrijving);
	}
}