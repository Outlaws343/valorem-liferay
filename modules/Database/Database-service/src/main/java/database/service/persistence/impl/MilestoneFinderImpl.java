package database.service.persistence.impl;

import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.PortalException;
import database.model.Milestone;
import database.model.impl.MilestoneImpl;
import database.service.MilestoneLocalServiceUtil;
import database.service.persistence.MilestoneFinder;
import database.service.persistence.MilestonePK;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Date;
import java.util.List;

@Component(service = MilestoneFinder.class)
public class MilestoneFinderImpl extends MilestoneFinderBaseImpl implements MilestoneFinder {
    @Reference
    private CustomSQL customSQL;

    public List<Milestone> getMilestonesByInvestmentPlanId(int investmentPlanId){
        Session session = openSession();

        String sql = customSQL.get(getClass(), "getMilestonesByInvesteringsplanId");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Milestone", MilestoneImpl.class);

        QueryPos qPos = QueryPos.getInstance(sqlQuery);
        qPos.add(investmentPlanId);

        return (List<Milestone>) sqlQuery.list();
    }

    public Milestone addMilestone(String naam, int investeringsplanId, Date eindDatum, int percentage, String beschrijving) {
        Milestone milestone = MilestoneLocalServiceUtil.createMilestone(new MilestonePK(investeringsplanId, naam));
        milestone.setNaam(naam);
        milestone.setInvesteringsplanId(investeringsplanId);
        milestone.setEinddatum(eindDatum);
        milestone.setBeschrijving(beschrijving);
        milestone.setMilestone_percentage(percentage);
        milestone.setGeaccepteerd(false);
        milestone.setGecontroleerd(false);
        MilestoneLocalServiceUtil.addMilestone(milestone);

        return milestone;
    }

    public List<Milestone> getNotCheckedMilestones() {
        Session session = openSession();
        String sql = customSQL.get(getClass(), "getNotCheckedMilestones");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Milestone", MilestoneImpl.class);

        return (List<Milestone>) sqlQuery.list();
    }

    public List<Milestone> getAcceptedMilestones() {
        Session session = openSession();
        String sql = customSQL.get(getClass(), "getAcceptedMilestones");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Milestone", MilestoneImpl.class);

        return (List<Milestone>) sqlQuery.list();
    }

    public List<Milestone> getDeniedMilestones() {
        Session session = openSession();
        String sql = customSQL.get(getClass(), "getDeniedMilestones");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Milestone", MilestoneImpl.class);

        return (List<Milestone>) sqlQuery.list();
    }

    public Milestone updateMilestone(int investeringsplanId, String naam, boolean geaccepteerd, String reden){
        try {
            Milestone milestone = MilestoneLocalServiceUtil.getMilestone(new MilestonePK(investeringsplanId, naam));
            milestone.setGecontroleerd(true);
            milestone.setGeaccepteerd(geaccepteerd);
            milestone.setReden(reden);
            MilestoneLocalServiceUtil.updateMilestone(milestone);

            return milestone;
        } catch (PortalException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Milestone completeNextMilestone(int investeringsplanId, String beschrijving) {
        Session session = openSession();

        String sql = customSQL.get(getClass(), "getNextMilestoneByInvesteringsplanId");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Milestone", MilestoneImpl.class);

        QueryPos qPos = QueryPos.getInstance(sqlQuery);
        qPos.add(investeringsplanId);

        List<Milestone> milestones = (List<Milestone>) sqlQuery.list();

        if(milestones.isEmpty())
            return null;

        Milestone milestone = milestones.get(0);

        milestone.setInleverings_beschrijving(beschrijving);
        MilestoneLocalServiceUtil.updateMilestone(milestone);

        return milestone;
    }

}
