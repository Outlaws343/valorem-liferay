/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import database.service.MilestoneServiceUtil;

/**
 * Provides the HTTP utility for the
 * <code>MilestoneServiceUtil</code> service
 * utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * <code>HttpPrincipal</code> parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MilestoneServiceSoap
 * @generated
 */
public class MilestoneServiceHttp {

	public static java.util.List<database.model.Milestone>
		getMilestonesByInvestmentPlanId(
			HttpPrincipal httpPrincipal, int investmentPlanId) {

		try {
			MethodKey methodKey = new MethodKey(
				MilestoneServiceUtil.class, "getMilestonesByInvestmentPlanId",
				_getMilestonesByInvestmentPlanIdParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, investmentPlanId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Milestone>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static database.model.Milestone addMilestone(
		HttpPrincipal httpPrincipal, String naam, int investeringsplanId,
		java.util.Date eindDatum, int percentage, String beschrijving) {

		try {
			MethodKey methodKey = new MethodKey(
				MilestoneServiceUtil.class, "addMilestone",
				_addMilestoneParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, naam, investeringsplanId, eindDatum, percentage,
				beschrijving);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (database.model.Milestone)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<database.model.Milestone>
		getNotCheckedMilestones(HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				MilestoneServiceUtil.class, "getNotCheckedMilestones",
				_getNotCheckedMilestonesParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Milestone>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<database.model.Milestone>
		getAcceptedMilestones(HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				MilestoneServiceUtil.class, "getAcceptedMilestones",
				_getAcceptedMilestonesParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Milestone>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<database.model.Milestone> getDeniedMilestones(
		HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				MilestoneServiceUtil.class, "getDeniedMilestones",
				_getDeniedMilestonesParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Milestone>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static database.model.Milestone updateMilestone(
		HttpPrincipal httpPrincipal, int investeringsplanId, String naam,
		boolean geaccepteerd, String reden) {

		try {
			MethodKey methodKey = new MethodKey(
				MilestoneServiceUtil.class, "updateMilestone",
				_updateMilestoneParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, investeringsplanId, naam, geaccepteerd, reden);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (database.model.Milestone)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static database.model.Milestone completeNextMilestone(
		HttpPrincipal httpPrincipal, int investeringsplanId,
		String beschrijving) {

		try {
			MethodKey methodKey = new MethodKey(
				MilestoneServiceUtil.class, "completeNextMilestone",
				_completeNextMilestoneParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, investeringsplanId, beschrijving);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (database.model.Milestone)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(MilestoneServiceHttp.class);

	private static final Class<?>[]
		_getMilestonesByInvestmentPlanIdParameterTypes0 = new Class[] {
			int.class
		};
	private static final Class<?>[] _addMilestoneParameterTypes1 = new Class[] {
		String.class, int.class, java.util.Date.class, int.class, String.class
	};
	private static final Class<?>[] _getNotCheckedMilestonesParameterTypes2 =
		new Class[] {};
	private static final Class<?>[] _getAcceptedMilestonesParameterTypes3 =
		new Class[] {};
	private static final Class<?>[] _getDeniedMilestonesParameterTypes4 =
		new Class[] {};
	private static final Class<?>[] _updateMilestoneParameterTypes5 =
		new Class[] {int.class, String.class, boolean.class, String.class};
	private static final Class<?>[] _completeNextMilestoneParameterTypes6 =
		new Class[] {int.class, String.class};

}