/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import database.model.Investeringsplan;
import database.model.Issuer;
import database.service.base.InvesteringsplanServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the investeringsplan remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>database.service.InvesteringsplanService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringsplanServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=investeringsplan",
		"json.web.service.context.path=Investeringsplan"
	},
	service = AopService.class
)
public class InvesteringsplanServiceImpl extends InvesteringsplanServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>database.service.InvesteringsplanServiceUtil</code> to access the investeringsplan remote service.
	 */

	@JSONWebService(method = "GET")
	public List<Investeringsplan> getAllInvesteringsplannen(){
		return investeringsplanFinder.getAllInvesteringsplannen();
	}

	@JSONWebService(method = "GET")
	public Investeringsplan getInvesteringsplanById(int investeringsplanId){
		return investeringsplanFinder.getInvesteringsplanById(investeringsplanId);
	}

	@JSONWebService(method = "GET")
	public List<Investeringsplan> getNotCheckedInvesteringsplan(){
		return investeringsplanFinder.getNotCheckedInvesteringsplan();
	}

	@JSONWebService(method = "GET")
	public List<Investeringsplan> getAcceptedInvesteringsplan(){
		return investeringsplanFinder.getAcceptedInvesteringsplan();
	}

	@JSONWebService(method = "GET")
	public List<Investeringsplan> getDeniedInvesteringsplan(){
		return investeringsplanFinder.getDeniedInvesteringsplan();
	}

	@JSONWebService(method = "GET")
	public List<Investeringsplan>  getInvesteringsplansByIssuerId(int issuerId){
		return investeringsplanFinder.getInvesteringsplansByIssuerId(issuerId);
	}

	@JSONWebService(method = "POST")
	public Investeringsplan addInvesteringsplan(String naam, int doel, Date beginDatum, String beschrijving) {
		return investeringsplanFinder.addInvesteringsplan(naam, doel, beginDatum, beschrijving);
	}

	@JSONWebService(method = "UPDATE")
	public Investeringsplan updateInvesteringsplan(int investeringsplanId, boolean geaccepteerd, String reden){
		return investeringsplanFinder.updateInvesteringsplan(investeringsplanId, geaccepteerd, reden);
	}

	@JSONWebService(method = "UPDATE")
	public Investeringsplan updateInvesteringsplanProgress(int investeringsplanId, int voortgang){
		return investeringsplanFinder.updateInvesteringsplanProgress(investeringsplanId, voortgang);
	}
}