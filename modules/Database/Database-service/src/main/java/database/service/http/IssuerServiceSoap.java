/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import database.service.IssuerServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * <code>IssuerServiceUtil</code> service
 * utility. The static methods of this class call the same methods of the
 * service utility. However, the signatures are different because it is
 * difficult for SOAP to support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a <code>java.util.List</code>,
 * that is translated to an array of
 * <code>database.model.IssuerSoap</code>. If the method in the
 * service utility returns a
 * <code>database.model.Issuer</code>, that is translated to a
 * <code>database.model.IssuerSoap</code>. Methods that SOAP
 * cannot safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IssuerServiceHttp
 * @generated
 */
public class IssuerServiceSoap {

	public static database.model.IssuerSoap[] getNotCheckedIssuers()
		throws RemoteException {

		try {
			java.util.List<database.model.Issuer> returnValue =
				IssuerServiceUtil.getNotCheckedIssuers();

			return database.model.IssuerSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.IssuerSoap[] getAcceptedIssuers()
		throws RemoteException {

		try {
			java.util.List<database.model.Issuer> returnValue =
				IssuerServiceUtil.getAcceptedIssuers();

			return database.model.IssuerSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.IssuerSoap[] getDeniedIssuers()
		throws RemoteException {

		try {
			java.util.List<database.model.Issuer> returnValue =
				IssuerServiceUtil.getDeniedIssuers();

			return database.model.IssuerSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.IssuerSoap addIssuer(
			String voornaam, String achternaam, String bedrijfsnaam,
			String woonplaats, String email, int kvk, int vog, int bkr,
			String tel_nummer)
		throws RemoteException {

		try {
			database.model.Issuer returnValue = IssuerServiceUtil.addIssuer(
				voornaam, achternaam, bedrijfsnaam, woonplaats, email, kvk, vog,
				bkr, tel_nummer);

			return database.model.IssuerSoap.toSoapModel(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.IssuerSoap updateIssuer(
			int issuerId, boolean geaccepteerd, String reden)
		throws RemoteException {

		try {
			database.model.Issuer returnValue = IssuerServiceUtil.updateIssuer(
				issuerId, geaccepteerd, reden);

			return database.model.IssuerSoap.toSoapModel(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(IssuerServiceSoap.class);

}