/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;

import database.exception.NoSuchInvesteringsplanException;

import database.model.Investeringsplan;
import database.model.impl.InvesteringsplanImpl;
import database.model.impl.InvesteringsplanModelImpl;

import database.service.persistence.InvesteringsplanPersistence;
import database.service.persistence.impl.constants.ValoremPersistenceConstants;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the investeringsplan service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = InvesteringsplanPersistence.class)
public class InvesteringsplanPersistenceImpl
	extends BasePersistenceImpl<Investeringsplan>
	implements InvesteringsplanPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>InvesteringsplanUtil</code> to access the investeringsplan persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		InvesteringsplanImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;

	public InvesteringsplanPersistenceImpl() {
		setModelClass(Investeringsplan.class);

		setModelImplClass(InvesteringsplanImpl.class);
		setModelPKClass(int.class);
	}

	/**
	 * Caches the investeringsplan in the entity cache if it is enabled.
	 *
	 * @param investeringsplan the investeringsplan
	 */
	@Override
	public void cacheResult(Investeringsplan investeringsplan) {
		entityCache.putResult(
			entityCacheEnabled, InvesteringsplanImpl.class,
			investeringsplan.getPrimaryKey(), investeringsplan);

		investeringsplan.resetOriginalValues();
	}

	/**
	 * Caches the investeringsplans in the entity cache if it is enabled.
	 *
	 * @param investeringsplans the investeringsplans
	 */
	@Override
	public void cacheResult(List<Investeringsplan> investeringsplans) {
		for (Investeringsplan investeringsplan : investeringsplans) {
			if (entityCache.getResult(
					entityCacheEnabled, InvesteringsplanImpl.class,
					investeringsplan.getPrimaryKey()) == null) {

				cacheResult(investeringsplan);
			}
			else {
				investeringsplan.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all investeringsplans.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(InvesteringsplanImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the investeringsplan.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Investeringsplan investeringsplan) {
		entityCache.removeResult(
			entityCacheEnabled, InvesteringsplanImpl.class,
			investeringsplan.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Investeringsplan> investeringsplans) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Investeringsplan investeringsplan : investeringsplans) {
			entityCache.removeResult(
				entityCacheEnabled, InvesteringsplanImpl.class,
				investeringsplan.getPrimaryKey());
		}
	}

	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				entityCacheEnabled, InvesteringsplanImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new investeringsplan with the primary key. Does not add the investeringsplan to the database.
	 *
	 * @param investeringsplanId the primary key for the new investeringsplan
	 * @return the new investeringsplan
	 */
	@Override
	public Investeringsplan create(int investeringsplanId) {
		Investeringsplan investeringsplan = new InvesteringsplanImpl();

		investeringsplan.setNew(true);
		investeringsplan.setPrimaryKey(investeringsplanId);

		return investeringsplan;
	}

	/**
	 * Removes the investeringsplan with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan that was removed
	 * @throws NoSuchInvesteringsplanException if a investeringsplan with the primary key could not be found
	 */
	@Override
	public Investeringsplan remove(int investeringsplanId)
		throws NoSuchInvesteringsplanException {

		return remove((Serializable)investeringsplanId);
	}

	/**
	 * Removes the investeringsplan with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the investeringsplan
	 * @return the investeringsplan that was removed
	 * @throws NoSuchInvesteringsplanException if a investeringsplan with the primary key could not be found
	 */
	@Override
	public Investeringsplan remove(Serializable primaryKey)
		throws NoSuchInvesteringsplanException {

		Session session = null;

		try {
			session = openSession();

			Investeringsplan investeringsplan = (Investeringsplan)session.get(
				InvesteringsplanImpl.class, primaryKey);

			if (investeringsplan == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchInvesteringsplanException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(investeringsplan);
		}
		catch (NoSuchInvesteringsplanException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Investeringsplan removeImpl(Investeringsplan investeringsplan) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(investeringsplan)) {
				investeringsplan = (Investeringsplan)session.get(
					InvesteringsplanImpl.class,
					investeringsplan.getPrimaryKeyObj());
			}

			if (investeringsplan != null) {
				session.delete(investeringsplan);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (investeringsplan != null) {
			clearCache(investeringsplan);
		}

		return investeringsplan;
	}

	@Override
	public Investeringsplan updateImpl(Investeringsplan investeringsplan) {
		boolean isNew = investeringsplan.isNew();

		Session session = null;

		try {
			session = openSession();

			if (investeringsplan.isNew()) {
				session.save(investeringsplan);

				investeringsplan.setNew(false);
			}
			else {
				investeringsplan = (Investeringsplan)session.merge(
					investeringsplan);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			entityCacheEnabled, InvesteringsplanImpl.class,
			investeringsplan.getPrimaryKey(), investeringsplan, false);

		investeringsplan.resetOriginalValues();

		return investeringsplan;
	}

	/**
	 * Returns the investeringsplan with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the investeringsplan
	 * @return the investeringsplan
	 * @throws NoSuchInvesteringsplanException if a investeringsplan with the primary key could not be found
	 */
	@Override
	public Investeringsplan findByPrimaryKey(Serializable primaryKey)
		throws NoSuchInvesteringsplanException {

		Investeringsplan investeringsplan = fetchByPrimaryKey(primaryKey);

		if (investeringsplan == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchInvesteringsplanException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return investeringsplan;
	}

	/**
	 * Returns the investeringsplan with the primary key or throws a <code>NoSuchInvesteringsplanException</code> if it could not be found.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan
	 * @throws NoSuchInvesteringsplanException if a investeringsplan with the primary key could not be found
	 */
	@Override
	public Investeringsplan findByPrimaryKey(int investeringsplanId)
		throws NoSuchInvesteringsplanException {

		return findByPrimaryKey((Serializable)investeringsplanId);
	}

	/**
	 * Returns the investeringsplan with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param investeringsplanId the primary key of the investeringsplan
	 * @return the investeringsplan, or <code>null</code> if a investeringsplan with the primary key could not be found
	 */
	@Override
	public Investeringsplan fetchByPrimaryKey(int investeringsplanId) {
		return fetchByPrimaryKey((Serializable)investeringsplanId);
	}

	/**
	 * Returns all the investeringsplans.
	 *
	 * @return the investeringsplans
	 */
	@Override
	public List<Investeringsplan> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the investeringsplans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investeringsplans
	 * @param end the upper bound of the range of investeringsplans (not inclusive)
	 * @return the range of investeringsplans
	 */
	@Override
	public List<Investeringsplan> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the investeringsplans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investeringsplans
	 * @param end the upper bound of the range of investeringsplans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of investeringsplans
	 */
	@Override
	public List<Investeringsplan> findAll(
		int start, int end,
		OrderByComparator<Investeringsplan> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the investeringsplans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringsplanModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investeringsplans
	 * @param end the upper bound of the range of investeringsplans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of investeringsplans
	 */
	@Override
	public List<Investeringsplan> findAll(
		int start, int end,
		OrderByComparator<Investeringsplan> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Investeringsplan> list = null;

		if (useFinderCache) {
			list = (List<Investeringsplan>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_INVESTERINGSPLAN);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_INVESTERINGSPLAN;

				sql = sql.concat(InvesteringsplanModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Investeringsplan>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the investeringsplans from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Investeringsplan investeringsplan : findAll()) {
			remove(investeringsplan);
		}
	}

	/**
	 * Returns the number of investeringsplans.
	 *
	 * @return the number of investeringsplans
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_INVESTERINGSPLAN);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "investeringsplanId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_INVESTERINGSPLAN;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return InvesteringsplanModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the investeringsplan persistence.
	 */
	@Activate
	public void activate() {
		InvesteringsplanModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		InvesteringsplanModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, InvesteringsplanImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, InvesteringsplanImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(InvesteringsplanImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.database.model.Investeringsplan"),
			true);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_INVESTERINGSPLAN =
		"SELECT investeringsplan FROM Investeringsplan investeringsplan";

	private static final String _SQL_COUNT_INVESTERINGSPLAN =
		"SELECT COUNT(investeringsplan) FROM Investeringsplan investeringsplan";

	private static final String _ORDER_BY_ENTITY_ALIAS = "investeringsplan.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Investeringsplan exists with the primary key ";

	private static final Log _log = LogFactoryUtil.getLog(
		InvesteringsplanPersistenceImpl.class);

	static {
		try {
			Class.forName(ValoremPersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException classNotFoundException) {
			throw new ExceptionInInitializerError(classNotFoundException);
		}
	}

}