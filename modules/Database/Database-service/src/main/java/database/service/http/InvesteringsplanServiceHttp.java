/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import database.service.InvesteringsplanServiceUtil;

/**
 * Provides the HTTP utility for the
 * <code>InvesteringsplanServiceUtil</code> service
 * utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * <code>HttpPrincipal</code> parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringsplanServiceSoap
 * @generated
 */
public class InvesteringsplanServiceHttp {

	public static java.util.List<database.model.Investeringsplan>
		getAllInvesteringsplannen(HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				InvesteringsplanServiceUtil.class, "getAllInvesteringsplannen",
				_getAllInvesteringsplannenParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Investeringsplan>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static database.model.Investeringsplan getInvesteringsplanById(
		HttpPrincipal httpPrincipal, int investeringsplanId) {

		try {
			MethodKey methodKey = new MethodKey(
				InvesteringsplanServiceUtil.class, "getInvesteringsplanById",
				_getInvesteringsplanByIdParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, investeringsplanId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (database.model.Investeringsplan)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<database.model.Investeringsplan>
		getNotCheckedInvesteringsplan(HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				InvesteringsplanServiceUtil.class,
				"getNotCheckedInvesteringsplan",
				_getNotCheckedInvesteringsplanParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Investeringsplan>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<database.model.Investeringsplan>
		getAcceptedInvesteringsplan(HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				InvesteringsplanServiceUtil.class,
				"getAcceptedInvesteringsplan",
				_getAcceptedInvesteringsplanParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Investeringsplan>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<database.model.Investeringsplan>
		getDeniedInvesteringsplan(HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				InvesteringsplanServiceUtil.class, "getDeniedInvesteringsplan",
				_getDeniedInvesteringsplanParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Investeringsplan>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<database.model.Investeringsplan>
		getInvesteringsplansByIssuerId(
			HttpPrincipal httpPrincipal, int issuerId) {

		try {
			MethodKey methodKey = new MethodKey(
				InvesteringsplanServiceUtil.class,
				"getInvesteringsplansByIssuerId",
				_getInvesteringsplansByIssuerIdParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, issuerId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Investeringsplan>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static database.model.Investeringsplan addInvesteringsplan(
		HttpPrincipal httpPrincipal, String naam, int doel,
		java.util.Date beginDatum, String beschrijving) {

		try {
			MethodKey methodKey = new MethodKey(
				InvesteringsplanServiceUtil.class, "addInvesteringsplan",
				_addInvesteringsplanParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, naam, doel, beginDatum, beschrijving);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (database.model.Investeringsplan)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static database.model.Investeringsplan updateInvesteringsplan(
		HttpPrincipal httpPrincipal, int investeringsplanId,
		boolean geaccepteerd, String reden) {

		try {
			MethodKey methodKey = new MethodKey(
				InvesteringsplanServiceUtil.class, "updateInvesteringsplan",
				_updateInvesteringsplanParameterTypes7);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, investeringsplanId, geaccepteerd, reden);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (database.model.Investeringsplan)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static database.model.Investeringsplan
		updateInvesteringsplanProgress(
			HttpPrincipal httpPrincipal, int investeringsplanId,
			int voortgang) {

		try {
			MethodKey methodKey = new MethodKey(
				InvesteringsplanServiceUtil.class,
				"updateInvesteringsplanProgress",
				_updateInvesteringsplanProgressParameterTypes8);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, investeringsplanId, voortgang);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (database.model.Investeringsplan)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(
		InvesteringsplanServiceHttp.class);

	private static final Class<?>[] _getAllInvesteringsplannenParameterTypes0 =
		new Class[] {};
	private static final Class<?>[] _getInvesteringsplanByIdParameterTypes1 =
		new Class[] {int.class};
	private static final Class<?>[]
		_getNotCheckedInvesteringsplanParameterTypes2 = new Class[] {};
	private static final Class<?>[]
		_getAcceptedInvesteringsplanParameterTypes3 = new Class[] {};
	private static final Class<?>[] _getDeniedInvesteringsplanParameterTypes4 =
		new Class[] {};
	private static final Class<?>[]
		_getInvesteringsplansByIssuerIdParameterTypes5 = new Class[] {
			int.class
		};
	private static final Class<?>[] _addInvesteringsplanParameterTypes6 =
		new Class[] {
			String.class, int.class, java.util.Date.class, String.class
		};
	private static final Class<?>[] _updateInvesteringsplanParameterTypes7 =
		new Class[] {int.class, boolean.class, String.class};
	private static final Class<?>[]
		_updateInvesteringsplanProgressParameterTypes8 = new Class[] {
			int.class, int.class
		};

}