/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;

import database.exception.NoSuchSTOException;

import database.model.STO;
import database.model.impl.STOImpl;
import database.model.impl.STOModelImpl;

import database.service.persistence.STOPersistence;
import database.service.persistence.impl.constants.ValoremPersistenceConstants;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the sto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = STOPersistence.class)
public class STOPersistenceImpl
	extends BasePersistenceImpl<STO> implements STOPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>STOUtil</code> to access the sto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		STOImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;

	public STOPersistenceImpl() {
		setModelClass(STO.class);

		setModelImplClass(STOImpl.class);
		setModelPKClass(int.class);
	}

	/**
	 * Caches the sto in the entity cache if it is enabled.
	 *
	 * @param sto the sto
	 */
	@Override
	public void cacheResult(STO sto) {
		entityCache.putResult(
			entityCacheEnabled, STOImpl.class, sto.getPrimaryKey(), sto);

		sto.resetOriginalValues();
	}

	/**
	 * Caches the stos in the entity cache if it is enabled.
	 *
	 * @param stOs the stos
	 */
	@Override
	public void cacheResult(List<STO> stOs) {
		for (STO sto : stOs) {
			if (entityCache.getResult(
					entityCacheEnabled, STOImpl.class, sto.getPrimaryKey()) ==
						null) {

				cacheResult(sto);
			}
			else {
				sto.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all stos.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(STOImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the sto.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(STO sto) {
		entityCache.removeResult(
			entityCacheEnabled, STOImpl.class, sto.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<STO> stOs) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (STO sto : stOs) {
			entityCache.removeResult(
				entityCacheEnabled, STOImpl.class, sto.getPrimaryKey());
		}
	}

	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				entityCacheEnabled, STOImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new sto with the primary key. Does not add the sto to the database.
	 *
	 * @param STOId the primary key for the new sto
	 * @return the new sto
	 */
	@Override
	public STO create(int STOId) {
		STO sto = new STOImpl();

		sto.setNew(true);
		sto.setPrimaryKey(STOId);

		return sto;
	}

	/**
	 * Removes the sto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto that was removed
	 * @throws NoSuchSTOException if a sto with the primary key could not be found
	 */
	@Override
	public STO remove(int STOId) throws NoSuchSTOException {
		return remove((Serializable)STOId);
	}

	/**
	 * Removes the sto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the sto
	 * @return the sto that was removed
	 * @throws NoSuchSTOException if a sto with the primary key could not be found
	 */
	@Override
	public STO remove(Serializable primaryKey) throws NoSuchSTOException {
		Session session = null;

		try {
			session = openSession();

			STO sto = (STO)session.get(STOImpl.class, primaryKey);

			if (sto == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSTOException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(sto);
		}
		catch (NoSuchSTOException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected STO removeImpl(STO sto) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(sto)) {
				sto = (STO)session.get(STOImpl.class, sto.getPrimaryKeyObj());
			}

			if (sto != null) {
				session.delete(sto);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (sto != null) {
			clearCache(sto);
		}

		return sto;
	}

	@Override
	public STO updateImpl(STO sto) {
		boolean isNew = sto.isNew();

		Session session = null;

		try {
			session = openSession();

			if (sto.isNew()) {
				session.save(sto);

				sto.setNew(false);
			}
			else {
				sto = (STO)session.merge(sto);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			entityCacheEnabled, STOImpl.class, sto.getPrimaryKey(), sto, false);

		sto.resetOriginalValues();

		return sto;
	}

	/**
	 * Returns the sto with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the sto
	 * @return the sto
	 * @throws NoSuchSTOException if a sto with the primary key could not be found
	 */
	@Override
	public STO findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSTOException {

		STO sto = fetchByPrimaryKey(primaryKey);

		if (sto == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSTOException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return sto;
	}

	/**
	 * Returns the sto with the primary key or throws a <code>NoSuchSTOException</code> if it could not be found.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto
	 * @throws NoSuchSTOException if a sto with the primary key could not be found
	 */
	@Override
	public STO findByPrimaryKey(int STOId) throws NoSuchSTOException {
		return findByPrimaryKey((Serializable)STOId);
	}

	/**
	 * Returns the sto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param STOId the primary key of the sto
	 * @return the sto, or <code>null</code> if a sto with the primary key could not be found
	 */
	@Override
	public STO fetchByPrimaryKey(int STOId) {
		return fetchByPrimaryKey((Serializable)STOId);
	}

	/**
	 * Returns all the stos.
	 *
	 * @return the stos
	 */
	@Override
	public List<STO> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @return the range of stos
	 */
	@Override
	public List<STO> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of stos
	 */
	@Override
	public List<STO> findAll(
		int start, int end, OrderByComparator<STO> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the stos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>STOModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stos
	 * @param end the upper bound of the range of stos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of stos
	 */
	@Override
	public List<STO> findAll(
		int start, int end, OrderByComparator<STO> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<STO> list = null;

		if (useFinderCache) {
			list = (List<STO>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_STO);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_STO;

				sql = sql.concat(STOModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<STO>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the stos from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (STO sto : findAll()) {
			remove(sto);
		}
	}

	/**
	 * Returns the number of stos.
	 *
	 * @return the number of stos
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_STO);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "STOId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_STO;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return STOModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the sto persistence.
	 */
	@Activate
	public void activate() {
		STOModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		STOModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, STOImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, STOImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(STOImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.database.model.STO"),
			true);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_STO = "SELECT sto FROM STO sto";

	private static final String _SQL_COUNT_STO =
		"SELECT COUNT(sto) FROM STO sto";

	private static final String _ORDER_BY_ENTITY_ALIAS = "sto.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No STO exists with the primary key ";

	private static final Log _log = LogFactoryUtil.getLog(
		STOPersistenceImpl.class);

	static {
		try {
			Class.forName(ValoremPersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException classNotFoundException) {
			throw new ExceptionInInitializerError(classNotFoundException);
		}
	}

}