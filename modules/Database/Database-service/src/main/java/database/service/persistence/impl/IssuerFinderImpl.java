package database.service.persistence.impl;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.PortalException;
import database.model.Issuer;
import database.model.impl.IssuerImpl;
import database.service.IssuerLocalService;
import database.service.IssuerLocalServiceUtil;
import database.service.persistence.IssuerFinder;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Collections;
import java.util.List;

@Component(service = IssuerFinder.class)
public class IssuerFinderImpl extends IssuerFinderBaseImpl implements IssuerFinder {

    @Reference
    private CustomSQL customSQL;

    public List<Issuer> getNotCheckedIssuers() {
        Session session = openSession();
        String sql = customSQL.get(getClass(), "getNotCheckedIssuers");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Issuers", IssuerImpl.class);

        return (List<Issuer>) sqlQuery.list();
    }

    public List<Issuer> getAcceptedIssuers() {
        Session session = openSession();
        String sql = customSQL.get(getClass(), "getAcceptedIssuers");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Issuers", IssuerImpl.class);

        return (List<Issuer>) sqlQuery.list();
    }

    public List<Issuer> getDeniedIssuers() {
        Session session = openSession();
        String sql = customSQL.get(getClass(), "getDeniedIssuers");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Issuers", IssuerImpl.class);

        return (List<Issuer>) sqlQuery.list();
    }

    public Issuer addIssuer(String voornaam, String achternaam, String bedrijfsnaam, String woonplaats, String email,
                            int kvk, int vog, int bkr, String tel_nummer) {
        Issuer issuer = IssuerLocalServiceUtil.createIssuer((int) CounterLocalServiceUtil.increment());
        issuer.setVoornaam(voornaam);
        issuer.setAchternaam(achternaam);
        issuer.setBedrijfsnaam(bedrijfsnaam);
        issuer.setWoonplaats(woonplaats);
        issuer.setEmail(email);
        issuer.setKvk(kvk);
        issuer.setVog(vog);
        issuer.setBkr(bkr);
        issuer.setTel_nummer(tel_nummer);
        IssuerLocalServiceUtil.addIssuer(issuer);

        return issuer;
    }

    public Issuer updateIssuer(int issuerId, boolean geaccepteerd, String reden){
        try {
            Issuer issuer = IssuerLocalServiceUtil.getIssuer(issuerId);
            issuer.setGecontroleerd(true);
            issuer.setGeaccepteerd(geaccepteerd);
            issuer.setReden(reden);
            IssuerLocalServiceUtil.updateIssuer(issuer);

            return issuer;
        } catch (PortalException e) {
            e.printStackTrace();
        }
        return null;
    }
}
