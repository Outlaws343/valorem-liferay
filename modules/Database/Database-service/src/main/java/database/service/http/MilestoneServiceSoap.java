/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import database.service.MilestoneServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * <code>MilestoneServiceUtil</code> service
 * utility. The static methods of this class call the same methods of the
 * service utility. However, the signatures are different because it is
 * difficult for SOAP to support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a <code>java.util.List</code>,
 * that is translated to an array of
 * <code>database.model.MilestoneSoap</code>. If the method in the
 * service utility returns a
 * <code>database.model.Milestone</code>, that is translated to a
 * <code>database.model.MilestoneSoap</code>. Methods that SOAP
 * cannot safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MilestoneServiceHttp
 * @generated
 */
public class MilestoneServiceSoap {

	public static database.model.MilestoneSoap[]
			getMilestonesByInvestmentPlanId(int investmentPlanId)
		throws RemoteException {

		try {
			java.util.List<database.model.Milestone> returnValue =
				MilestoneServiceUtil.getMilestonesByInvestmentPlanId(
					investmentPlanId);

			return database.model.MilestoneSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.MilestoneSoap addMilestone(
			String naam, int investeringsplanId, java.util.Date eindDatum,
			int percentage, String beschrijving)
		throws RemoteException {

		try {
			database.model.Milestone returnValue =
				MilestoneServiceUtil.addMilestone(
					naam, investeringsplanId, eindDatum, percentage,
					beschrijving);

			return database.model.MilestoneSoap.toSoapModel(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.MilestoneSoap[] getNotCheckedMilestones()
		throws RemoteException {

		try {
			java.util.List<database.model.Milestone> returnValue =
				MilestoneServiceUtil.getNotCheckedMilestones();

			return database.model.MilestoneSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.MilestoneSoap[] getAcceptedMilestones()
		throws RemoteException {

		try {
			java.util.List<database.model.Milestone> returnValue =
				MilestoneServiceUtil.getAcceptedMilestones();

			return database.model.MilestoneSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.MilestoneSoap[] getDeniedMilestones()
		throws RemoteException {

		try {
			java.util.List<database.model.Milestone> returnValue =
				MilestoneServiceUtil.getDeniedMilestones();

			return database.model.MilestoneSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.MilestoneSoap updateMilestone(
			int investeringsplanId, String naam, boolean geaccepteerd,
			String reden)
		throws RemoteException {

		try {
			database.model.Milestone returnValue =
				MilestoneServiceUtil.updateMilestone(
					investeringsplanId, naam, geaccepteerd, reden);

			return database.model.MilestoneSoap.toSoapModel(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static database.model.MilestoneSoap completeNextMilestone(
			int investeringsplanId, String beschrijving)
		throws RemoteException {

		try {
			database.model.Milestone returnValue =
				MilestoneServiceUtil.completeNextMilestone(
					investeringsplanId, beschrijving);

			return database.model.MilestoneSoap.toSoapModel(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(MilestoneServiceSoap.class);

}