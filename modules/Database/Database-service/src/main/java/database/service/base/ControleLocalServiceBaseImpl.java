/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.base;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalServiceImpl;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PortalUtil;

import database.model.Controle;

import database.service.ControleLocalService;
import database.service.persistence.ControlePersistence;
import database.service.persistence.InvesteringPersistence;
import database.service.persistence.InvesteringsplanPersistence;
import database.service.persistence.IssuerFinder;
import database.service.persistence.IssuerPersistence;
import database.service.persistence.MilestonePersistence;
import database.service.persistence.ParticipantPersistence;
import database.service.persistence.STOPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Reference;

/**
 * Provides the base implementation for the controle local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link database.service.impl.ControleLocalServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see database.service.impl.ControleLocalServiceImpl
 * @generated
 */
public abstract class ControleLocalServiceBaseImpl
	extends BaseLocalServiceImpl
	implements AopService, ControleLocalService, IdentifiableOSGiService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Use <code>ControleLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>database.service.ControleLocalServiceUtil</code>.
	 */

	/**
	 * Adds the controle to the database. Also notifies the appropriate model listeners.
	 *
	 * @param controle the controle
	 * @return the controle that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Controle addControle(Controle controle) {
		controle.setNew(true);

		return controlePersistence.update(controle);
	}

	/**
	 * Creates a new controle with the primary key. Does not add the controle to the database.
	 *
	 * @param issuerId the primary key for the new controle
	 * @return the new controle
	 */
	@Override
	@Transactional(enabled = false)
	public Controle createControle(int issuerId) {
		return controlePersistence.create(issuerId);
	}

	/**
	 * Deletes the controle with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle that was removed
	 * @throws PortalException if a controle with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Controle deleteControle(int issuerId) throws PortalException {
		return controlePersistence.remove(issuerId);
	}

	/**
	 * Deletes the controle from the database. Also notifies the appropriate model listeners.
	 *
	 * @param controle the controle
	 * @return the controle that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Controle deleteControle(Controle controle) {
		return controlePersistence.remove(controle);
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(
			Controle.class, clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return controlePersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.ControleModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return controlePersistence.findWithDynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.ControleModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return controlePersistence.findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return controlePersistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection) {

		return controlePersistence.countWithDynamicQuery(
			dynamicQuery, projection);
	}

	@Override
	public Controle fetchControle(int issuerId) {
		return controlePersistence.fetchByPrimaryKey(issuerId);
	}

	/**
	 * Returns the controle with the primary key.
	 *
	 * @param issuerId the primary key of the controle
	 * @return the controle
	 * @throws PortalException if a controle with the primary key could not be found
	 */
	@Override
	public Controle getControle(int issuerId) throws PortalException {
		return controlePersistence.findByPrimaryKey(issuerId);
	}

	@Override
	public ActionableDynamicQuery getActionableDynamicQuery() {
		ActionableDynamicQuery actionableDynamicQuery =
			new DefaultActionableDynamicQuery();

		actionableDynamicQuery.setBaseLocalService(controleLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(Controle.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("issuerId");

		return actionableDynamicQuery;
	}

	@Override
	public IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		IndexableActionableDynamicQuery indexableActionableDynamicQuery =
			new IndexableActionableDynamicQuery();

		indexableActionableDynamicQuery.setBaseLocalService(
			controleLocalService);
		indexableActionableDynamicQuery.setClassLoader(getClassLoader());
		indexableActionableDynamicQuery.setModelClass(Controle.class);

		indexableActionableDynamicQuery.setPrimaryKeyPropertyName("issuerId");

		return indexableActionableDynamicQuery;
	}

	protected void initActionableDynamicQuery(
		ActionableDynamicQuery actionableDynamicQuery) {

		actionableDynamicQuery.setBaseLocalService(controleLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(Controle.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("issuerId");
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException {

		return controleLocalService.deleteControle((Controle)persistedModel);
	}

	public BasePersistence<Controle> getBasePersistence() {
		return controlePersistence;
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return controlePersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the controles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.ControleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of controles
	 * @param end the upper bound of the range of controles (not inclusive)
	 * @return the range of controles
	 */
	@Override
	public List<Controle> getControles(int start, int end) {
		return controlePersistence.findAll(start, end);
	}

	/**
	 * Returns the number of controles.
	 *
	 * @return the number of controles
	 */
	@Override
	public int getControlesCount() {
		return controlePersistence.countAll();
	}

	/**
	 * Updates the controle in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param controle the controle
	 * @return the controle that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Controle updateControle(Controle controle) {
		return controlePersistence.update(controle);
	}

	@Override
	public Class<?>[] getAopInterfaces() {
		return new Class<?>[] {
			ControleLocalService.class, IdentifiableOSGiService.class,
			PersistedModelLocalService.class
		};
	}

	@Override
	public void setAopProxy(Object aopProxy) {
		controleLocalService = (ControleLocalService)aopProxy;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return ControleLocalService.class.getName();
	}

	protected Class<?> getModelClass() {
		return Controle.class;
	}

	protected String getModelClassName() {
		return Controle.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = controlePersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(
				dataSource, sql);

			sqlUpdate.update();
		}
		catch (Exception exception) {
			throw new SystemException(exception);
		}
	}

	protected ControleLocalService controleLocalService;

	@Reference
	protected ControlePersistence controlePersistence;

	@Reference
	protected InvesteringPersistence investeringPersistence;

	@Reference
	protected InvesteringsplanPersistence investeringsplanPersistence;

	@Reference
	protected IssuerPersistence issuerPersistence;

	@Reference
	protected IssuerFinder issuerFinder;

	@Reference
	protected MilestonePersistence milestonePersistence;

	@Reference
	protected ParticipantPersistence participantPersistence;

	@Reference
	protected STOPersistence stoPersistence;

	@Reference
	protected com.liferay.counter.kernel.service.CounterLocalService
		counterLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.ClassNameLocalService
		classNameLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.ResourceLocalService
		resourceLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.UserLocalService
		userLocalService;

}