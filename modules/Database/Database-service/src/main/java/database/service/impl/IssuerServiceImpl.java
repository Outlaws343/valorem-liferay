/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import database.model.Issuer;
import database.service.base.IssuerServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the issuer remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>database.service.IssuerService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IssuerServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=account",
		"json.web.service.context.path=Issuer"
	},
	service = AopService.class
)
public class IssuerServiceImpl extends IssuerServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>database.service.IssuerServiceUtil</code> to access the issuer remote service.
	 */

	@JSONWebService(method = "GET")
	public List<Issuer> getNotCheckedIssuers(){
		return issuerFinder.getNotCheckedIssuers();
	}

	@JSONWebService(method = "GET")
	public List<Issuer> getAcceptedIssuers(){
		return issuerFinder.getAcceptedIssuers();
	}

	@JSONWebService(method = "GET")
	public List<Issuer> getDeniedIssuers(){
		return issuerFinder.getDeniedIssuers();
	}

	@JSONWebService(method = "POST")
	public Issuer addIssuer(String voornaam, String achternaam, String bedrijfsnaam, String woonplaats, String email,
							int kvk, int vog, int bkr, String tel_nummer){
		return issuerFinder.addIssuer(voornaam, achternaam, bedrijfsnaam, woonplaats, email, kvk, vog, bkr, tel_nummer);
	}

	@JSONWebService(method = "UPDATE")
	public Issuer updateIssuer(int issuerId, boolean geaccepteerd, String reden){
		return issuerFinder.updateIssuer(issuerId, geaccepteerd, reden);
	}
}