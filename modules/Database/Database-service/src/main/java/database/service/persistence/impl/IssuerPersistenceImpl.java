/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;

import database.exception.NoSuchIssuerException;

import database.model.Issuer;
import database.model.impl.IssuerImpl;
import database.model.impl.IssuerModelImpl;

import database.service.persistence.IssuerPersistence;
import database.service.persistence.impl.constants.ValoremPersistenceConstants;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the issuer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = IssuerPersistence.class)
public class IssuerPersistenceImpl
	extends BasePersistenceImpl<Issuer> implements IssuerPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>IssuerUtil</code> to access the issuer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		IssuerImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;

	public IssuerPersistenceImpl() {
		setModelClass(Issuer.class);

		setModelImplClass(IssuerImpl.class);
		setModelPKClass(int.class);
	}

	/**
	 * Caches the issuer in the entity cache if it is enabled.
	 *
	 * @param issuer the issuer
	 */
	@Override
	public void cacheResult(Issuer issuer) {
		entityCache.putResult(
			entityCacheEnabled, IssuerImpl.class, issuer.getPrimaryKey(),
			issuer);

		issuer.resetOriginalValues();
	}

	/**
	 * Caches the issuers in the entity cache if it is enabled.
	 *
	 * @param issuers the issuers
	 */
	@Override
	public void cacheResult(List<Issuer> issuers) {
		for (Issuer issuer : issuers) {
			if (entityCache.getResult(
					entityCacheEnabled, IssuerImpl.class,
					issuer.getPrimaryKey()) == null) {

				cacheResult(issuer);
			}
			else {
				issuer.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all issuers.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(IssuerImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the issuer.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Issuer issuer) {
		entityCache.removeResult(
			entityCacheEnabled, IssuerImpl.class, issuer.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Issuer> issuers) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Issuer issuer : issuers) {
			entityCache.removeResult(
				entityCacheEnabled, IssuerImpl.class, issuer.getPrimaryKey());
		}
	}

	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				entityCacheEnabled, IssuerImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new issuer with the primary key. Does not add the issuer to the database.
	 *
	 * @param issuerId the primary key for the new issuer
	 * @return the new issuer
	 */
	@Override
	public Issuer create(int issuerId) {
		Issuer issuer = new IssuerImpl();

		issuer.setNew(true);
		issuer.setPrimaryKey(issuerId);

		return issuer;
	}

	/**
	 * Removes the issuer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer that was removed
	 * @throws NoSuchIssuerException if a issuer with the primary key could not be found
	 */
	@Override
	public Issuer remove(int issuerId) throws NoSuchIssuerException {
		return remove((Serializable)issuerId);
	}

	/**
	 * Removes the issuer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the issuer
	 * @return the issuer that was removed
	 * @throws NoSuchIssuerException if a issuer with the primary key could not be found
	 */
	@Override
	public Issuer remove(Serializable primaryKey) throws NoSuchIssuerException {
		Session session = null;

		try {
			session = openSession();

			Issuer issuer = (Issuer)session.get(IssuerImpl.class, primaryKey);

			if (issuer == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchIssuerException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(issuer);
		}
		catch (NoSuchIssuerException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Issuer removeImpl(Issuer issuer) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(issuer)) {
				issuer = (Issuer)session.get(
					IssuerImpl.class, issuer.getPrimaryKeyObj());
			}

			if (issuer != null) {
				session.delete(issuer);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (issuer != null) {
			clearCache(issuer);
		}

		return issuer;
	}

	@Override
	public Issuer updateImpl(Issuer issuer) {
		boolean isNew = issuer.isNew();

		Session session = null;

		try {
			session = openSession();

			if (issuer.isNew()) {
				session.save(issuer);

				issuer.setNew(false);
			}
			else {
				issuer = (Issuer)session.merge(issuer);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			entityCacheEnabled, IssuerImpl.class, issuer.getPrimaryKey(),
			issuer, false);

		issuer.resetOriginalValues();

		return issuer;
	}

	/**
	 * Returns the issuer with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the issuer
	 * @return the issuer
	 * @throws NoSuchIssuerException if a issuer with the primary key could not be found
	 */
	@Override
	public Issuer findByPrimaryKey(Serializable primaryKey)
		throws NoSuchIssuerException {

		Issuer issuer = fetchByPrimaryKey(primaryKey);

		if (issuer == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchIssuerException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return issuer;
	}

	/**
	 * Returns the issuer with the primary key or throws a <code>NoSuchIssuerException</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer
	 * @throws NoSuchIssuerException if a issuer with the primary key could not be found
	 */
	@Override
	public Issuer findByPrimaryKey(int issuerId) throws NoSuchIssuerException {
		return findByPrimaryKey((Serializable)issuerId);
	}

	/**
	 * Returns the issuer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param issuerId the primary key of the issuer
	 * @return the issuer, or <code>null</code> if a issuer with the primary key could not be found
	 */
	@Override
	public Issuer fetchByPrimaryKey(int issuerId) {
		return fetchByPrimaryKey((Serializable)issuerId);
	}

	/**
	 * Returns all the issuers.
	 *
	 * @return the issuers
	 */
	@Override
	public List<Issuer> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @return the range of issuers
	 */
	@Override
	public List<Issuer> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of issuers
	 */
	@Override
	public List<Issuer> findAll(
		int start, int end, OrderByComparator<Issuer> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the issuers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>IssuerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of issuers
	 * @param end the upper bound of the range of issuers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of issuers
	 */
	@Override
	public List<Issuer> findAll(
		int start, int end, OrderByComparator<Issuer> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Issuer> list = null;

		if (useFinderCache) {
			list = (List<Issuer>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_ISSUER);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_ISSUER;

				sql = sql.concat(IssuerModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Issuer>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the issuers from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Issuer issuer : findAll()) {
			remove(issuer);
		}
	}

	/**
	 * Returns the number of issuers.
	 *
	 * @return the number of issuers
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_ISSUER);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "issuerId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_ISSUER;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return IssuerModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the issuer persistence.
	 */
	@Activate
	public void activate() {
		IssuerModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		IssuerModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, IssuerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, IssuerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(IssuerImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.database.model.Issuer"),
			true);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_ISSUER =
		"SELECT issuer FROM Issuer issuer";

	private static final String _SQL_COUNT_ISSUER =
		"SELECT COUNT(issuer) FROM Issuer issuer";

	private static final String _ORDER_BY_ENTITY_ALIAS = "issuer.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Issuer exists with the primary key ";

	private static final Log _log = LogFactoryUtil.getLog(
		IssuerPersistenceImpl.class);

	static {
		try {
			Class.forName(ValoremPersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException classNotFoundException) {
			throw new ExceptionInInitializerError(classNotFoundException);
		}
	}

}