package database.service.persistence.impl;

import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import database.model.Investering;
import database.model.impl.InvesteringImpl;
import database.service.persistence.InvesteringFinder;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.List;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import database.service.InvesteringLocalServiceUtil;

@Component(service = InvesteringFinder.class)
public class InvesteringFinderImpl extends InvesteringFinderBaseImpl implements InvesteringFinder {

    @Reference
    private CustomSQL customSQL;

    public Investering addInvestering(int investeringsplanId, int participantId, int waarde) {
        Investering investering = InvesteringLocalServiceUtil.createInvestering((int) CounterLocalServiceUtil.increment());
        investering.setInvesteringsplanId(investeringsplanId);
        investering.setParticipantId(participantId);
        investering.setWaarde(waarde);
        investering.setSTOId(0);
        InvesteringLocalServiceUtil.addInvestering(investering);

        return investering;
    }


    public List<Investering> getInvesteringById(int participantId) {
        Session session = openSession();

        String sql = customSQL.get(getClass(), "getInvesteringByParticipantId");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Investering", InvesteringImpl.class);

        QueryPos qPos = QueryPos.getInstance(sqlQuery);
        qPos.add(participantId);

        return (List<Investering>) sqlQuery.list();
    }
}
