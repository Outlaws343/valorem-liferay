package database.service.persistence.impl;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.PortalException;
import database.model.Investering;
import database.model.Investeringsplan;
import database.model.Issuer;
import database.model.impl.InvesteringsplanImpl;
import database.model.impl.IssuerImpl;
import database.service.InvesteringsplanLocalServiceUtil;
import database.service.IssuerLocalServiceUtil;
import database.service.persistence.InvesteringsplanFinder;
import database.service.persistence.IssuerFinder;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component(service = InvesteringsplanFinder.class)
public class InvesteringsplanFinderImpl extends InvesteringsplanFinderBaseImpl implements InvesteringsplanFinder {

    @Reference
    private CustomSQL customSQL;

    public List<Investeringsplan> getAllInvesteringsplannen() {
        Session session = openSession();

        String sql = customSQL.get(getClass(), "getAllInvesteringsplannen");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Investeringsplan", InvesteringsplanImpl.class);

        return (List<Investeringsplan>) sqlQuery.list();
    }

    public Investeringsplan getInvesteringsplanById(int investeringsplanId) {
        Session session = openSession();

        String sql = customSQL.get(getClass(), "getInvesteringsplanById");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Investeringsplan", InvesteringsplanImpl.class);

        QueryPos qPos = QueryPos.getInstance(sqlQuery);
        qPos.add(investeringsplanId);

        return (Investeringsplan) sqlQuery.list().get(0);
    }

    public List<Investeringsplan> getNotCheckedInvesteringsplan() {
        Session session = openSession();

        String sql = customSQL.get(getClass(), "getNotCheckedInvesteringsplan");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Investeringsplan", InvesteringsplanImpl.class);

        return (List<Investeringsplan>) sqlQuery.list();
    }

    public List<Investeringsplan> getAcceptedInvesteringsplan() {
        Session session = openSession();

        String sql = customSQL.get(getClass(), "getAcceptedInvesteringsplan");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Investeringsplan", InvesteringsplanImpl.class);

        return (List<Investeringsplan>) sqlQuery.list();
    }

    public List<Investeringsplan> getDeniedInvesteringsplan() {
        Session session = openSession();

        String sql = customSQL.get(getClass(), "getDeniedInvesteringsplan");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Investeringsplan", InvesteringsplanImpl.class);

        return (List<Investeringsplan>) sqlQuery.list();
    }

    public List<Investeringsplan> getInvesteringsplansByIssuerId(int issuerId) {
        Session session = openSession();

        String sql = customSQL.get(getClass(), "getInvesteringsplansByIssuerId");
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        sqlQuery.addEntity("Investeringsplan", InvesteringsplanImpl.class);

        QueryPos qPos = QueryPos.getInstance(sqlQuery);
        qPos.add(issuerId);

        return (List<Investeringsplan>) sqlQuery.list();
    }

    public Investeringsplan addInvesteringsplan(String naam, int doel, Date beginDatum, String beschrijving) {
        Investeringsplan investeringsplan = InvesteringsplanLocalServiceUtil.createInvesteringsplan((int) CounterLocalServiceUtil.increment());
        investeringsplan.setNaam(naam);
        investeringsplan.setStatus("Pending review...");
        investeringsplan.setVoortgang(0);
        investeringsplan.setDoel(doel);
        investeringsplan.setBegindatum(beginDatum);
        investeringsplan.setBeschrijving(beschrijving);
        investeringsplan.setIssuerId(1);
        investeringsplan.setGecontroleerd(false);
        investeringsplan.setGeaccepteerd(false);
        InvesteringsplanLocalServiceUtil.addInvesteringsplan(investeringsplan);

        return investeringsplan;
    }

    public Investeringsplan updateInvesteringsplan(int investeringsplanId, boolean geaccepteerd, String reden){
        try {
            Investeringsplan investeringsplan = InvesteringsplanLocalServiceUtil.getInvesteringsplan(investeringsplanId);
            investeringsplan.setGecontroleerd(true);
            investeringsplan.setGeaccepteerd(geaccepteerd);
            investeringsplan.setReden(reden);
            InvesteringsplanLocalServiceUtil.updateInvesteringsplan(investeringsplan);

            return investeringsplan;
        } catch (PortalException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Investeringsplan updateInvesteringsplanProgress(int investeringsplanId, int voortgang){
        try {
            Investeringsplan investeringsplan = InvesteringsplanLocalServiceUtil.getInvesteringsplan(investeringsplanId);
            investeringsplan.setVoortgang(voortgang);
            InvesteringsplanLocalServiceUtil.updateInvesteringsplan(investeringsplan);

            return investeringsplan;
        } catch (PortalException e) {
            e.printStackTrace();
        }
        return null;
    }
}
