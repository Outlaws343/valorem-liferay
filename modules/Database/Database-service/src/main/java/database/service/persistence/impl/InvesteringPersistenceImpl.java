/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;

import database.exception.NoSuchInvesteringException;

import database.model.Investering;
import database.model.impl.InvesteringImpl;
import database.model.impl.InvesteringModelImpl;

import database.service.persistence.InvesteringPersistence;
import database.service.persistence.impl.constants.ValoremPersistenceConstants;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the investering service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = InvesteringPersistence.class)
public class InvesteringPersistenceImpl
	extends BasePersistenceImpl<Investering> implements InvesteringPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>InvesteringUtil</code> to access the investering persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		InvesteringImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;

	public InvesteringPersistenceImpl() {
		setModelClass(Investering.class);

		setModelImplClass(InvesteringImpl.class);
		setModelPKClass(int.class);
	}

	/**
	 * Caches the investering in the entity cache if it is enabled.
	 *
	 * @param investering the investering
	 */
	@Override
	public void cacheResult(Investering investering) {
		entityCache.putResult(
			entityCacheEnabled, InvesteringImpl.class,
			investering.getPrimaryKey(), investering);

		investering.resetOriginalValues();
	}

	/**
	 * Caches the investerings in the entity cache if it is enabled.
	 *
	 * @param investerings the investerings
	 */
	@Override
	public void cacheResult(List<Investering> investerings) {
		for (Investering investering : investerings) {
			if (entityCache.getResult(
					entityCacheEnabled, InvesteringImpl.class,
					investering.getPrimaryKey()) == null) {

				cacheResult(investering);
			}
			else {
				investering.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all investerings.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(InvesteringImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the investering.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Investering investering) {
		entityCache.removeResult(
			entityCacheEnabled, InvesteringImpl.class,
			investering.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Investering> investerings) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Investering investering : investerings) {
			entityCache.removeResult(
				entityCacheEnabled, InvesteringImpl.class,
				investering.getPrimaryKey());
		}
	}

	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				entityCacheEnabled, InvesteringImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new investering with the primary key. Does not add the investering to the database.
	 *
	 * @param investeringsId the primary key for the new investering
	 * @return the new investering
	 */
	@Override
	public Investering create(int investeringsId) {
		Investering investering = new InvesteringImpl();

		investering.setNew(true);
		investering.setPrimaryKey(investeringsId);

		return investering;
	}

	/**
	 * Removes the investering with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering that was removed
	 * @throws NoSuchInvesteringException if a investering with the primary key could not be found
	 */
	@Override
	public Investering remove(int investeringsId)
		throws NoSuchInvesteringException {

		return remove((Serializable)investeringsId);
	}

	/**
	 * Removes the investering with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the investering
	 * @return the investering that was removed
	 * @throws NoSuchInvesteringException if a investering with the primary key could not be found
	 */
	@Override
	public Investering remove(Serializable primaryKey)
		throws NoSuchInvesteringException {

		Session session = null;

		try {
			session = openSession();

			Investering investering = (Investering)session.get(
				InvesteringImpl.class, primaryKey);

			if (investering == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchInvesteringException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(investering);
		}
		catch (NoSuchInvesteringException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Investering removeImpl(Investering investering) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(investering)) {
				investering = (Investering)session.get(
					InvesteringImpl.class, investering.getPrimaryKeyObj());
			}

			if (investering != null) {
				session.delete(investering);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (investering != null) {
			clearCache(investering);
		}

		return investering;
	}

	@Override
	public Investering updateImpl(Investering investering) {
		boolean isNew = investering.isNew();

		Session session = null;

		try {
			session = openSession();

			if (investering.isNew()) {
				session.save(investering);

				investering.setNew(false);
			}
			else {
				investering = (Investering)session.merge(investering);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			entityCacheEnabled, InvesteringImpl.class,
			investering.getPrimaryKey(), investering, false);

		investering.resetOriginalValues();

		return investering;
	}

	/**
	 * Returns the investering with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the investering
	 * @return the investering
	 * @throws NoSuchInvesteringException if a investering with the primary key could not be found
	 */
	@Override
	public Investering findByPrimaryKey(Serializable primaryKey)
		throws NoSuchInvesteringException {

		Investering investering = fetchByPrimaryKey(primaryKey);

		if (investering == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchInvesteringException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return investering;
	}

	/**
	 * Returns the investering with the primary key or throws a <code>NoSuchInvesteringException</code> if it could not be found.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering
	 * @throws NoSuchInvesteringException if a investering with the primary key could not be found
	 */
	@Override
	public Investering findByPrimaryKey(int investeringsId)
		throws NoSuchInvesteringException {

		return findByPrimaryKey((Serializable)investeringsId);
	}

	/**
	 * Returns the investering with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering, or <code>null</code> if a investering with the primary key could not be found
	 */
	@Override
	public Investering fetchByPrimaryKey(int investeringsId) {
		return fetchByPrimaryKey((Serializable)investeringsId);
	}

	/**
	 * Returns all the investerings.
	 *
	 * @return the investerings
	 */
	@Override
	public List<Investering> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the investerings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investerings
	 * @param end the upper bound of the range of investerings (not inclusive)
	 * @return the range of investerings
	 */
	@Override
	public List<Investering> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the investerings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investerings
	 * @param end the upper bound of the range of investerings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of investerings
	 */
	@Override
	public List<Investering> findAll(
		int start, int end, OrderByComparator<Investering> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the investerings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investerings
	 * @param end the upper bound of the range of investerings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of investerings
	 */
	@Override
	public List<Investering> findAll(
		int start, int end, OrderByComparator<Investering> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Investering> list = null;

		if (useFinderCache) {
			list = (List<Investering>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_INVESTERING);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_INVESTERING;

				sql = sql.concat(InvesteringModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Investering>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the investerings from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Investering investering : findAll()) {
			remove(investering);
		}
	}

	/**
	 * Returns the number of investerings.
	 *
	 * @return the number of investerings
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_INVESTERING);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "investeringsId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_INVESTERING;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return InvesteringModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the investering persistence.
	 */
	@Activate
	public void activate() {
		InvesteringModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		InvesteringModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, InvesteringImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, InvesteringImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(InvesteringImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.database.model.Investering"),
			true);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_INVESTERING =
		"SELECT investering FROM Investering investering";

	private static final String _SQL_COUNT_INVESTERING =
		"SELECT COUNT(investering) FROM Investering investering";

	private static final String _ORDER_BY_ENTITY_ALIAS = "investering.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Investering exists with the primary key ";

	private static final Log _log = LogFactoryUtil.getLog(
		InvesteringPersistenceImpl.class);

	static {
		try {
			Class.forName(ValoremPersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException classNotFoundException) {
			throw new ExceptionInInitializerError(classNotFoundException);
		}
	}

}