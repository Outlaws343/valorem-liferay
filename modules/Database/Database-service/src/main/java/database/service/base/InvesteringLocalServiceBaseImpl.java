/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.base;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalServiceImpl;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PortalUtil;

import database.model.Investering;

import database.service.InvesteringLocalService;
import database.service.persistence.InvesteringFinder;
import database.service.persistence.InvesteringPersistence;
import database.service.persistence.InvesteringsplanFinder;
import database.service.persistence.InvesteringsplanPersistence;
import database.service.persistence.IssuerFinder;
import database.service.persistence.IssuerPersistence;
import database.service.persistence.MilestoneFinder;
import database.service.persistence.MilestonePersistence;
import database.service.persistence.ParticipantPersistence;
import database.service.persistence.STOPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Reference;

/**
 * Provides the base implementation for the investering local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link database.service.impl.InvesteringLocalServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see database.service.impl.InvesteringLocalServiceImpl
 * @generated
 */
public abstract class InvesteringLocalServiceBaseImpl
	extends BaseLocalServiceImpl
	implements AopService, IdentifiableOSGiService, InvesteringLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Use <code>InvesteringLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>database.service.InvesteringLocalServiceUtil</code>.
	 */

	/**
	 * Adds the investering to the database. Also notifies the appropriate model listeners.
	 *
	 * @param investering the investering
	 * @return the investering that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Investering addInvestering(Investering investering) {
		investering.setNew(true);

		return investeringPersistence.update(investering);
	}

	/**
	 * Creates a new investering with the primary key. Does not add the investering to the database.
	 *
	 * @param investeringsId the primary key for the new investering
	 * @return the new investering
	 */
	@Override
	@Transactional(enabled = false)
	public Investering createInvestering(int investeringsId) {
		return investeringPersistence.create(investeringsId);
	}

	/**
	 * Deletes the investering with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering that was removed
	 * @throws PortalException if a investering with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Investering deleteInvestering(int investeringsId)
		throws PortalException {

		return investeringPersistence.remove(investeringsId);
	}

	/**
	 * Deletes the investering from the database. Also notifies the appropriate model listeners.
	 *
	 * @param investering the investering
	 * @return the investering that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Investering deleteInvestering(Investering investering) {
		return investeringPersistence.remove(investering);
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(
			Investering.class, clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return investeringPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return investeringPersistence.findWithDynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return investeringPersistence.findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return investeringPersistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection) {

		return investeringPersistence.countWithDynamicQuery(
			dynamicQuery, projection);
	}

	@Override
	public Investering fetchInvestering(int investeringsId) {
		return investeringPersistence.fetchByPrimaryKey(investeringsId);
	}

	/**
	 * Returns the investering with the primary key.
	 *
	 * @param investeringsId the primary key of the investering
	 * @return the investering
	 * @throws PortalException if a investering with the primary key could not be found
	 */
	@Override
	public Investering getInvestering(int investeringsId)
		throws PortalException {

		return investeringPersistence.findByPrimaryKey(investeringsId);
	}

	@Override
	public ActionableDynamicQuery getActionableDynamicQuery() {
		ActionableDynamicQuery actionableDynamicQuery =
			new DefaultActionableDynamicQuery();

		actionableDynamicQuery.setBaseLocalService(investeringLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(Investering.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("investeringsId");

		return actionableDynamicQuery;
	}

	@Override
	public IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		IndexableActionableDynamicQuery indexableActionableDynamicQuery =
			new IndexableActionableDynamicQuery();

		indexableActionableDynamicQuery.setBaseLocalService(
			investeringLocalService);
		indexableActionableDynamicQuery.setClassLoader(getClassLoader());
		indexableActionableDynamicQuery.setModelClass(Investering.class);

		indexableActionableDynamicQuery.setPrimaryKeyPropertyName(
			"investeringsId");

		return indexableActionableDynamicQuery;
	}

	protected void initActionableDynamicQuery(
		ActionableDynamicQuery actionableDynamicQuery) {

		actionableDynamicQuery.setBaseLocalService(investeringLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(Investering.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("investeringsId");
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException {

		return investeringLocalService.deleteInvestering(
			(Investering)persistedModel);
	}

	public BasePersistence<Investering> getBasePersistence() {
		return investeringPersistence;
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return investeringPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the investerings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>database.model.impl.InvesteringModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of investerings
	 * @param end the upper bound of the range of investerings (not inclusive)
	 * @return the range of investerings
	 */
	@Override
	public List<Investering> getInvesterings(int start, int end) {
		return investeringPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of investerings.
	 *
	 * @return the number of investerings
	 */
	@Override
	public int getInvesteringsCount() {
		return investeringPersistence.countAll();
	}

	/**
	 * Updates the investering in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param investering the investering
	 * @return the investering that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Investering updateInvestering(Investering investering) {
		return investeringPersistence.update(investering);
	}

	@Override
	public Class<?>[] getAopInterfaces() {
		return new Class<?>[] {
			InvesteringLocalService.class, IdentifiableOSGiService.class,
			PersistedModelLocalService.class
		};
	}

	@Override
	public void setAopProxy(Object aopProxy) {
		investeringLocalService = (InvesteringLocalService)aopProxy;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return InvesteringLocalService.class.getName();
	}

	protected Class<?> getModelClass() {
		return Investering.class;
	}

	protected String getModelClassName() {
		return Investering.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = investeringPersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(
				dataSource, sql);

			sqlUpdate.update();
		}
		catch (Exception exception) {
			throw new SystemException(exception);
		}
	}

	protected InvesteringLocalService investeringLocalService;

	@Reference
	protected InvesteringPersistence investeringPersistence;

	@Reference
	protected InvesteringFinder investeringFinder;

	@Reference
	protected InvesteringsplanPersistence investeringsplanPersistence;

	@Reference
	protected InvesteringsplanFinder investeringsplanFinder;

	@Reference
	protected IssuerPersistence issuerPersistence;

	@Reference
	protected IssuerFinder issuerFinder;

	@Reference
	protected MilestonePersistence milestonePersistence;

	@Reference
	protected MilestoneFinder milestoneFinder;

	@Reference
	protected ParticipantPersistence participantPersistence;

	@Reference
	protected STOPersistence stoPersistence;

	@Reference
	protected com.liferay.counter.kernel.service.CounterLocalService
		counterLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.ClassNameLocalService
		classNameLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.ResourceLocalService
		resourceLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.UserLocalService
		userLocalService;

}