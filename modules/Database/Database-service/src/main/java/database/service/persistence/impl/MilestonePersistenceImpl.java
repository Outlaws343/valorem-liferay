/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;

import database.exception.NoSuchMilestoneException;

import database.model.Milestone;
import database.model.impl.MilestoneImpl;
import database.model.impl.MilestoneModelImpl;

import database.service.persistence.MilestonePK;
import database.service.persistence.MilestonePersistence;
import database.service.persistence.impl.constants.ValoremPersistenceConstants;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the milestone service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = MilestonePersistence.class)
public class MilestonePersistenceImpl
	extends BasePersistenceImpl<Milestone> implements MilestonePersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>MilestoneUtil</code> to access the milestone persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		MilestoneImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;

	public MilestonePersistenceImpl() {
		setModelClass(Milestone.class);

		setModelImplClass(MilestoneImpl.class);
		setModelPKClass(MilestonePK.class);
	}

	/**
	 * Caches the milestone in the entity cache if it is enabled.
	 *
	 * @param milestone the milestone
	 */
	@Override
	public void cacheResult(Milestone milestone) {
		entityCache.putResult(
			entityCacheEnabled, MilestoneImpl.class, milestone.getPrimaryKey(),
			milestone);

		milestone.resetOriginalValues();
	}

	/**
	 * Caches the milestones in the entity cache if it is enabled.
	 *
	 * @param milestones the milestones
	 */
	@Override
	public void cacheResult(List<Milestone> milestones) {
		for (Milestone milestone : milestones) {
			if (entityCache.getResult(
					entityCacheEnabled, MilestoneImpl.class,
					milestone.getPrimaryKey()) == null) {

				cacheResult(milestone);
			}
			else {
				milestone.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all milestones.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(MilestoneImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the milestone.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Milestone milestone) {
		entityCache.removeResult(
			entityCacheEnabled, MilestoneImpl.class, milestone.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Milestone> milestones) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Milestone milestone : milestones) {
			entityCache.removeResult(
				entityCacheEnabled, MilestoneImpl.class,
				milestone.getPrimaryKey());
		}
	}

	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				entityCacheEnabled, MilestoneImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new milestone with the primary key. Does not add the milestone to the database.
	 *
	 * @param milestonePK the primary key for the new milestone
	 * @return the new milestone
	 */
	@Override
	public Milestone create(MilestonePK milestonePK) {
		Milestone milestone = new MilestoneImpl();

		milestone.setNew(true);
		milestone.setPrimaryKey(milestonePK);

		return milestone;
	}

	/**
	 * Removes the milestone with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param milestonePK the primary key of the milestone
	 * @return the milestone that was removed
	 * @throws NoSuchMilestoneException if a milestone with the primary key could not be found
	 */
	@Override
	public Milestone remove(MilestonePK milestonePK)
		throws NoSuchMilestoneException {

		return remove((Serializable)milestonePK);
	}

	/**
	 * Removes the milestone with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the milestone
	 * @return the milestone that was removed
	 * @throws NoSuchMilestoneException if a milestone with the primary key could not be found
	 */
	@Override
	public Milestone remove(Serializable primaryKey)
		throws NoSuchMilestoneException {

		Session session = null;

		try {
			session = openSession();

			Milestone milestone = (Milestone)session.get(
				MilestoneImpl.class, primaryKey);

			if (milestone == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMilestoneException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(milestone);
		}
		catch (NoSuchMilestoneException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Milestone removeImpl(Milestone milestone) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(milestone)) {
				milestone = (Milestone)session.get(
					MilestoneImpl.class, milestone.getPrimaryKeyObj());
			}

			if (milestone != null) {
				session.delete(milestone);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (milestone != null) {
			clearCache(milestone);
		}

		return milestone;
	}

	@Override
	public Milestone updateImpl(Milestone milestone) {
		boolean isNew = milestone.isNew();

		Session session = null;

		try {
			session = openSession();

			if (milestone.isNew()) {
				session.save(milestone);

				milestone.setNew(false);
			}
			else {
				milestone = (Milestone)session.merge(milestone);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			entityCacheEnabled, MilestoneImpl.class, milestone.getPrimaryKey(),
			milestone, false);

		milestone.resetOriginalValues();

		return milestone;
	}

	/**
	 * Returns the milestone with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the milestone
	 * @return the milestone
	 * @throws NoSuchMilestoneException if a milestone with the primary key could not be found
	 */
	@Override
	public Milestone findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMilestoneException {

		Milestone milestone = fetchByPrimaryKey(primaryKey);

		if (milestone == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMilestoneException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return milestone;
	}

	/**
	 * Returns the milestone with the primary key or throws a <code>NoSuchMilestoneException</code> if it could not be found.
	 *
	 * @param milestonePK the primary key of the milestone
	 * @return the milestone
	 * @throws NoSuchMilestoneException if a milestone with the primary key could not be found
	 */
	@Override
	public Milestone findByPrimaryKey(MilestonePK milestonePK)
		throws NoSuchMilestoneException {

		return findByPrimaryKey((Serializable)milestonePK);
	}

	/**
	 * Returns the milestone with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param milestonePK the primary key of the milestone
	 * @return the milestone, or <code>null</code> if a milestone with the primary key could not be found
	 */
	@Override
	public Milestone fetchByPrimaryKey(MilestonePK milestonePK) {
		return fetchByPrimaryKey((Serializable)milestonePK);
	}

	/**
	 * Returns all the milestones.
	 *
	 * @return the milestones
	 */
	@Override
	public List<Milestone> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the milestones.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MilestoneModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of milestones
	 * @param end the upper bound of the range of milestones (not inclusive)
	 * @return the range of milestones
	 */
	@Override
	public List<Milestone> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the milestones.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MilestoneModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of milestones
	 * @param end the upper bound of the range of milestones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of milestones
	 */
	@Override
	public List<Milestone> findAll(
		int start, int end, OrderByComparator<Milestone> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the milestones.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MilestoneModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of milestones
	 * @param end the upper bound of the range of milestones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of milestones
	 */
	@Override
	public List<Milestone> findAll(
		int start, int end, OrderByComparator<Milestone> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Milestone> list = null;

		if (useFinderCache) {
			list = (List<Milestone>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_MILESTONE);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_MILESTONE;

				sql = sql.concat(MilestoneModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Milestone>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the milestones from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Milestone milestone : findAll()) {
			remove(milestone);
		}
	}

	/**
	 * Returns the number of milestones.
	 *
	 * @return the number of milestones
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_MILESTONE);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getCompoundPKColumnNames() {
		return _compoundPKColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "milestonePK";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_MILESTONE;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return MilestoneModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the milestone persistence.
	 */
	@Activate
	public void activate() {
		MilestoneModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		MilestoneModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, MilestoneImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, MilestoneImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(MilestoneImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.database.model.Milestone"),
			true);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = ValoremPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_MILESTONE =
		"SELECT milestone FROM Milestone milestone";

	private static final String _SQL_COUNT_MILESTONE =
		"SELECT COUNT(milestone) FROM Milestone milestone";

	private static final String _ORDER_BY_ENTITY_ALIAS = "milestone.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Milestone exists with the primary key ";

	private static final Log _log = LogFactoryUtil.getLog(
		MilestonePersistenceImpl.class);

	private static final Set<String> _compoundPKColumnNames = SetUtil.fromArray(
		new String[] {"investeringsplanId", "naam"});

	static {
		try {
			Class.forName(ValoremPersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException classNotFoundException) {
			throw new ExceptionInInitializerError(classNotFoundException);
		}
	}

}