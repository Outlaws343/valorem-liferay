/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import database.model.Investering;
import database.model.Issuer;
import database.service.base.InvesteringServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the investering remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>database.service.InvesteringService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InvesteringServiceBaseImpl
 */
@Component(
        property = {
                "json.web.service.context.name=investering",
                "json.web.service.context.path=Investering"
        },
        service = AopService.class
)
public class InvesteringServiceImpl extends InvesteringServiceBaseImpl {

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Always use <code>database.service.InvesteringServiceUtil</code> to access the investering remote service.
     */

    @JSONWebService(method = "GET")
    public List<Investering> getInvesteringById(int participantId) {
        return investeringFinder.getInvesteringById(participantId);
    }

    @JSONWebService(method = "GET")
    public Investering addInvestering(int investeringsplanId, int participantId, int waarde) {
        return investeringFinder.addInvestering(investeringsplanId, participantId, waarde);
    }
}