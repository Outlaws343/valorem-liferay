/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package database.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import database.service.IssuerServiceUtil;

/**
 * Provides the HTTP utility for the
 * <code>IssuerServiceUtil</code> service
 * utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * <code>HttpPrincipal</code> parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IssuerServiceSoap
 * @generated
 */
public class IssuerServiceHttp {

	public static java.util.List<database.model.Issuer> getNotCheckedIssuers(
		HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				IssuerServiceUtil.class, "getNotCheckedIssuers",
				_getNotCheckedIssuersParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Issuer>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<database.model.Issuer> getAcceptedIssuers(
		HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				IssuerServiceUtil.class, "getAcceptedIssuers",
				_getAcceptedIssuersParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Issuer>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<database.model.Issuer> getDeniedIssuers(
		HttpPrincipal httpPrincipal) {

		try {
			MethodKey methodKey = new MethodKey(
				IssuerServiceUtil.class, "getDeniedIssuers",
				_getDeniedIssuersParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<database.model.Issuer>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static database.model.Issuer addIssuer(
		HttpPrincipal httpPrincipal, String voornaam, String achternaam,
		String bedrijfsnaam, String woonplaats, String email, int kvk, int vog,
		int bkr, String tel_nummer) {

		try {
			MethodKey methodKey = new MethodKey(
				IssuerServiceUtil.class, "addIssuer",
				_addIssuerParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, voornaam, achternaam, bedrijfsnaam, woonplaats,
				email, kvk, vog, bkr, tel_nummer);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (database.model.Issuer)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static database.model.Issuer updateIssuer(
		HttpPrincipal httpPrincipal, int issuerId, boolean geaccepteerd,
		String reden) {

		try {
			MethodKey methodKey = new MethodKey(
				IssuerServiceUtil.class, "updateIssuer",
				_updateIssuerParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, issuerId, geaccepteerd, reden);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (database.model.Issuer)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(IssuerServiceHttp.class);

	private static final Class<?>[] _getNotCheckedIssuersParameterTypes0 =
		new Class[] {};
	private static final Class<?>[] _getAcceptedIssuersParameterTypes1 =
		new Class[] {};
	private static final Class<?>[] _getDeniedIssuersParameterTypes2 =
		new Class[] {};
	private static final Class<?>[] _addIssuerParameterTypes3 = new Class[] {
		String.class, String.class, String.class, String.class, String.class,
		int.class, int.class, int.class, String.class
	};
	private static final Class<?>[] _updateIssuerParameterTypes4 = new Class[] {
		int.class, boolean.class, String.class
	};

}