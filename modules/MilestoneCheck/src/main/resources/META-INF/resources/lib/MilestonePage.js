import React from "react";

export default class MilestonePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            milestone: props.milestone,
            investeringsplan: null,
            fetchingInvesteringsplan: true,
            modal: false,
            isDenied: props.checked.denied,
            isCheck: props.checked.checked
        }

        this.hideModal = this.hideModal.bind(this);
        this.saveAction = this.saveAction.bind(this);

        this.addInvesteringsplan = this.addInvesteringsplan.bind(this);
    }

    componentDidMount() {
        Liferay.Service(
            '/investeringsplan.investeringsplan/get-investeringsplan-by-id',
            {
                investeringsplanId: this.state.milestone.investeringsplanId
            },
            (investeringsplan) => this.addInvesteringsplan(investeringsplan)
        );
    }

    addInvesteringsplan(investeringsplan) {
        this.setState({investeringsplan: investeringsplan, fetchingInvesteringsplan: false});
    }

    showModal(isAccept) {
        this.setState({modal: true, isAccept: isAccept});
    }

    hideModal() {
        this.setState({modal: false});
    }

    saveAction(description) {
        Liferay.Service(
            '/milestone.milestone/update-milestone',
            {
                investeringsplanId: this.state.milestone.investeringsplanId,
                naam: this.state.milestone.naam,
                geaccepteerd: this.state.isAccept,
                reden: description
            },
            function (obj) {
                console.log(obj);
            }
        );

        this.props.changeMilestonesStatus(this.state.isAccept, description);

        this.setState({isCheck: false})
        this.hideModal();
    }

    render() {
        return (
            <div>
                <div>
                    <svg className="bi bi-arrow-left-short" width="48" height="48" viewBox="0 0 16 16"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg" onClick={this.props.goToList}>
                        <path fillRule="evenodd"
                              d="M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z"/>
                        <path fillRule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </div>
                {this.state.fetchingInvesteringsplan === false ?
                    <div className="portlet-div">
                        <div className="flexboxhorizontal">
                            <div className="childcenter"><img
                                src="https://www.liferay.com/o/osb-www-theme/images/custom/open_graph_image.png"
                                alt="Logo"
                                width="300px" height="300px"/></div>
                            <div className="roundborder">
                                <div className="contactinfo">
                                    <h2>Investment plan</h2>
                                    <p>{this.state.investeringsplan.naam}</p>
                                    <h2>Milestone</h2>
                                    <p>{this.state.milestone.naam}</p>
                                    <h2>Description</h2>
                                    <p>{this.state.milestone.beschrijving}</p>
                                </div>
                            </div>
                        </div>

                        <div className="flexboxhorizontal">
                            <div className="childborder">
                                <h2>Submitted work</h2>
                                <p>{this.state.milestone.inleverings_beschrijving}</p>
                            </div>
                        </div>

                        {this.state.isDenied === true ?
                            <div className="flexboxhorizontal">
                                <div className="childborder">
                                    <h2 style={{color: "red"}}>Reason for denied milestone</h2>
                                    <p>{this.state.milestone.reden}</p>
                                </div>
                            </div>
                            : <div/>}


                        {this.state.isCheck === true ?
                            <div className="flexbuttons">
                                <button type="button" className="btn btn-success" onClick={() => this.showModal(true)}
                                        style={{margin: "0px 12px 0px 0px"}}>Accept
                                </button>
                                <button type="button" className="btn btn-danger"
                                        onClick={() => this.showModal(false)}>Deny
                                </button>
                            </div>
                            : <div/>}
                    </div>
                    : <h2>Loading milestone and investmentplan</h2>}

                {this.state.modal === true ?
                    <Modal isAccept={this.state.isAccept} hideModal={this.hideModal} milestone={this.state.milestone}
                           saveAction={this.saveAction}/>
                    : <div/>}
            </div>
        )
    }
}

class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isAccept: props.isAccept,
            milestone: props.milestone
        }
    }

    render() {
        return <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        {this.state.isAccept === true ?
                            <h5 className="modal-title" id="exampleModalLongTitle">Milestone accepteren</h5>
                            : <h5 className="modal-title" id="exampleModalLongTitle">Milestone weigeren</h5>}

                        <button type="button" className="close" aria-label="Close" onClick={this.props.hideModal}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        {this.state.isAccept === true ?
                            <p>Name: {this.state.milestone.naam}</p>
                            : <textarea className="form-control" id="description" rows="5"/>}
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={this.props.hideModal}>Cancel
                        </button>
                        {this.state.isAccept === true ?
                            <button type="button" className="btn btn-primary"
                                    onClick={() => this.props.saveAction("")}>Save</button>
                            : <button type="button" className="btn btn-primary"
                                      onClick={() => this.props.saveAction(document.getElementById('description').value)}>Save</button>}
                    </div>
                </div>
            </div>
        </div>
    }
}

