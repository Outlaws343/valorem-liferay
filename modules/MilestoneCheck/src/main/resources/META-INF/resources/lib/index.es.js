import React from "react";
import ReactDOM from "react-dom";
import MilestoneList from "./MilestoneList";

export default function(elementId) {
	ReactDOM.render(<MilestoneList />, document.getElementById(elementId));
}