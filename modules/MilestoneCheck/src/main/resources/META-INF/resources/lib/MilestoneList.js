import React from "react";
import MilestonePage from "./MilestonePage";

export default class MilestoneList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            milestones: [],
            milestone: null,
            milestonescheck: [],
            milestonesaccepted: [],
            milestonesdenied: [],
            dropdowntext: "Check",
            dropdownselected: {
                checked: true,
                accepted: false,
                denied: false
            }
        }

        this.goToList = this.goToList.bind(this);

        this.getMilestones = this.getMilestones.bind(this);

        this.changeMilestonesStatus = this.changeMilestonesStatus.bind(this);
    }

    getMilestones(milestones, key) {
        switch (key) {
            case "notchecked":
                this.setState({milestones: milestones, milestonescheck: milestones});
                break;
            case "accepted":
                this.setState({milestonesaccepted: milestones});
                break;
            case "denied":
                this.setState({milestonesdenied: milestones});
                break;
        }
    }

    componentDidMount() {
        Liferay.Service(
            '/milestone.milestone/get-not-checked-milestones',
            (obj) => this.getMilestones(obj, "notchecked"));

        Liferay.Service(
            '/milestone.milestone/get-accepted-milestones',
            (obj) => this.getMilestones(obj, "accepted"));

        Liferay.Service(
            '/milestone.milestone/get-denied-milestones',
            (obj) => this.getMilestones(obj, "denied"));
    }

    goToMilestonePage(milestone) {
        this.setState({milestone: milestone});
    }

    goToList() {
        this.setState({milestone: null});
    }

    changeDropdown(selected) {
        switch (selected) {
            case "Check":
                this.setState({
                    milestones: this.state.milestonescheck, dropdowntext: selected,
                    dropdownselected: {checked: true, accepted: false, denied: false}
                });
                break;
            case "Accepted":
                this.setState({
                    milestones: this.state.milestonesaccepted, dropdowntext: selected,
                    dropdownselected: {checked: false, accepted: true, denied: false}
                });
                break;
            case "Denied":
                this.setState({
                    milestones: this.state.milestonesdenied, dropdowntext: selected,
                    dropdownselected: {checked: false, accepted: false, denied: true}
                });
                break;
        }
    }

    changeMilestonesStatus(isAccept, reason){
        let newMilestonesCheck = this.state.milestonescheck.filter(milestone => milestone !== this.state.milestone);

        this.state.milestone.reden = reason;

        if (isAccept){
            this.setState({milestonesaccepted: this.state.milestonesaccepted.concat(this.state.milestone), milestonescheck: newMilestonesCheck, milestones: newMilestonesCheck});
        } else {
            this.setState({milestonesdenied: this.state.milestonesdenied.concat(this.state.milestone), milestonescheck: newMilestonesCheck, milestones: newMilestonesCheck});
        }
    }

    render() {
        if (this.state.milestone !== null) {
            return <MilestonePage milestone={this.state.milestone} checked={this.state.dropdownselected}
                                  goToList={this.goToList} changeMilestonesStatus={this.changeMilestonesStatus}/>
        }

        return (<div>
                <div>
                    {this.state.milestones.length === 1 ?
                        <h2>Milestone controle</h2>
                        : <h2>Milestones controle</h2>}

                    <div className="dropdown">
                        <button className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            {this.state.dropdowntext}
                        </button>

                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            {this.state.dropdownselected.checked === true ?
                                <a className="dropdown-item active"
                                   onClick={() => this.changeDropdown("Check")}>Check</a> :
                                <a className="dropdown-item" onClick={() => this.changeDropdown("Check")}>Check</a>}

                            {this.state.dropdownselected.accepted === true ?
                                <a className="dropdown-item active"
                                   onClick={() => this.changeDropdown("Accepted")}>Accepted</a> :
                                <a className="dropdown-item"
                                   onClick={() => this.changeDropdown("Accepted")}>Accepted</a>}

                            {this.state.dropdownselected.denied === true ?
                                <a className="dropdown-item active"
                                   onClick={() => this.changeDropdown("Denied")}>Denied</a> :
                                <a className="dropdown-item" onClick={() => this.changeDropdown("Denied")}>Denied</a>}
                        </div>
                    </div>
                </div>

                {this.state.milestones.length === 0 ?
                    <h2 style={{'textAlign': "center"}}>There are no new milestones submitted</h2> :
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                        </tr>
                        </thead>

                        <tbody>
                        {this.state.milestones.map((milestone, index) =>
                            <tr key={index} onClick={() => this.goToMilestonePage(milestone)}>
                                <td>{milestone.naam}</td>
                                <td>{milestone.beschrijving}</td>
                            </tr>)}
                        </tbody>
                    </table>}
            </div>
        )
    }
}