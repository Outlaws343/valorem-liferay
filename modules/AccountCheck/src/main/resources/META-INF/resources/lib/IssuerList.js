import React from "react";
import IssuerPage from "./IssuerPage";

export default class IssuerList extends React.Component {
    constructor() {
        super();

        this.state = {
            accounts: [],
            account: null,
            accountscheck: [],
            accountsaccepted: [],
            accountsdenied: [],
            dropdowntext: "Controle",
            dropdownselected: {
                checked: true,
                accepted: false,
                denied: false
            }
        }

        this.goToList = this.goToList.bind(this);

        this.getIssuers = this.getIssuers.bind(this);

        this.changeIssuerStatus = this.changeIssuerStatus.bind(this);
    }

    getIssuers(issuers, key) {
        switch (key) {
            case "notchecked":
                this.setState({accounts: issuers, accountscheck: issuers});
                break;
            case "accepted":
                this.setState({accountsaccepted: issuers});
                break;
            case "denied":
                this.setState({accountsdenied: issuers});
                break;
        }
    }

    componentDidMount() {
        Liferay.Service(
            '/account.issuer/get-not-checked-issuers',
            (obj) => this.getIssuers(obj, "notchecked"));

        Liferay.Service(
            '/account.issuer/get-accepted-issuers',
            (obj) => this.getIssuers(obj, "accepted"));

        Liferay.Service(
            '/account.issuer/get-denied-issuers',
            (obj) => this.getIssuers(obj, "denied"));
    }

    goToAccountPage(account) {
        this.setState({account: account});
    }

    goToList() {
        this.setState({account: null});
    }

    changeDropdown(selected) {
        switch (selected) {
            case "Controle":
                this.setState({
                    accounts: this.state.accountscheck, dropdowntext: selected,
                    dropdownselected: {checked: true, accepted: false, denied: false}
                });
                break;
            case "Geaccepteerd":
                this.setState({
                    accounts: this.state.accountsaccepted, dropdowntext: selected,
                    dropdownselected: {checked: false, accepted: true, denied: false}
                });
                break;
            case "Geweigerd":
                this.setState({
                    accounts: this.state.accountsdenied, dropdowntext: selected,
                    dropdownselected: {checked: false, accepted: false, denied: true}
                });
                break;
        }
    }

    changeIssuerStatus(isAccept, reason){
        let newAccountsCheck = this.state.accountscheck.filter(issuer => issuer !== this.state.account);

        this.state.account.reden = reason;

        if (isAccept){
            this.setState({accountsaccepted: this.state.accountsaccepted.concat(this.state.account), accountscheck: newAccountsCheck, accounts: newAccountsCheck});
        } else {
            this.setState({accountsdenied: this.state.accountsdenied.concat(this.state.account), accountscheck: newAccountsCheck, accounts: newAccountsCheck});
        }
    }

    render() {
        if (this.state.account !== null) {
            return <IssuerPage account={this.state.account} checked={this.state.dropdownselected}
                               goToList={this.goToList} changeIssuerStatus={this.changeIssuerStatus}/>
        }

        return (<div>
                <div>
                    {this.state.accounts.length === 1 ?
                        <h2>Issuer account controle</h2>
                        : <h2>Issuer accounts controle</h2>}

                    <div className="dropdown">
                        <button className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            {this.state.dropdowntext}
                        </button>

                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            {this.state.dropdownselected.checked === true ?
                                <a className="dropdown-item active"
                                   onClick={() => this.changeDropdown("Controle")}>Controle</a> :
                                <a className="dropdown-item" onClick={() => this.changeDropdown("Controle")}>Controle</a>}

                            {this.state.dropdownselected.accepted === true ?
                                <a className="dropdown-item active"
                                   onClick={() => this.changeDropdown("Geaccepteerd")}>Geaccepteerd</a> :
                                <a className="dropdown-item"
                                   onClick={() => this.changeDropdown("Geaccepteerd")}>Geaccepteerd</a>}

                            {this.state.dropdownselected.denied === true ?
                                <a className="dropdown-item active"
                                   onClick={() => this.changeDropdown("Geweigerd")}>Geweigerd</a> :
                                <a className="dropdown-item" onClick={() => this.changeDropdown("Geweigerd")}>Geweigerd</a>}
                        </div>
                    </div>
                </div>

                {this.state.accounts.length === 0 ?
                    <h2 style={{'textAlign': "center"}}>Er zijn geen accountsverzoeken ingediend</h2> :
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Voornaam</th>
                            <th scope="col">Achternaam</th>
                            <th scope="col">Bedrijfsnaam</th>
                        </tr>
                        </thead>

                        <tbody>
                        {this.state.accounts.map((account, index) =>
                            <tr key={index} onClick={() => this.goToAccountPage(account)}>
                                <th scope="row">{account.issuerId}</th>
                                <td>{account.voornaam}</td>
                                <td>{account.achternaam}</td>
                                <td>{account.bedrijfsnaam}</td>
                            </tr>)}
                        </tbody>
                    </table>}
            </div>
        )
    }
}