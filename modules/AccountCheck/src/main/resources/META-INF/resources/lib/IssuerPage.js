import React from "react";

export default class IssuerPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            account: props.account,
            modal: false,
            isDenied: props.checked.denied,
            isCheck: props.checked.checked
        }

        this.hideModal = this.hideModal.bind(this);
        this.saveAction = this.saveAction.bind(this);
    }

    showModal(isAccept) {
        this.setState({modal: true, isAccept: isAccept});
    }

    hideModal() {
        this.setState({modal: false});
    }

    saveAction(description) {
        Liferay.Service(
            '/account.issuer/update-issuer',
            {
                issuerId: this.state.account.issuerId,
                geaccepteerd: this.state.isAccept,
                reden: description
            },
            function (obj) {
                console.log(obj);
            }
        );

        this.props.changeIssuerStatus(this.state.isAccept, description);

        this.setState({isCheck: false})
        this.hideModal();
    }

    render() {
        return (
            <div>
                <div>
                    <svg className="bi bi-arrow-left-short" width="48" height="48" viewBox="0 0 16 16"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg" onClick={this.props.goToList}>
                        <path fillRule="evenodd"
                              d="M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z"/>
                        <path fillRule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </div>
                <div className="portlet-div">
                    <div className="flexboxhorizontal">
                        <div className="childcenter"><img
                            src="https://www.liferay.com/o/osb-www-theme/images/custom/open_graph_image.png"
                            alt="Logo"
                            width="300px" height="300px"/></div>
                        <div className="roundborder">
                            <div className="contactinfo">
                                <h2>Name</h2>
                                <p>{this.state.account.voornaam} {this.state.account.achternaam}</p>
                                <h2>E-mail</h2>
                                <p>{this.state.account.email}</p>
                                <h2>Telefoon nummer</h2>
                                <p>{this.state.account.tel_nummer}</p>
                                <h2>Bedrijfsnaam</h2>
                                <p>{this.state.account.bedrijfsnaam}</p>
                            </div>
                        </div>
                    </div>

                    <div className="flexboxhorizontal">
                        <div className="flexboxvertical">
                            <div>
                                <h2>ID</h2>
                                <p>{this.state.account.issuerId}</p>
                            </div>
                            <div style={{margin: "0"}}>
                                <h2>KvK</h2>
                                <p>{this.state.account.kvk}</p>
                            </div>
                        </div>

                        <div className="flexboxvertical">
                            <div>
                                <h2>Vog</h2>
                                <p>{this.state.account.vog}</p>
                            </div>
                            <div style={{margin: "0"}}>
                                <h2>BKR</h2>
                                <p>{this.state.account.bkr}</p>
                            </div>
                        </div>

                        <div className="childborder">
                            <h2>Beschrijving</h2>
                            {this.state.account.beschrijving}
                        </div>
                    </div>

                    {this.state.isDenied === true ?
                        <div className="flexboxhorizontal">
                            <div className="childborder">
                                <h2>Reden van afwijzing</h2>
                                <p>{this.state.account.reden}</p>
                            </div>
                        </div>
                    : <div/>}


                    {this.state.isCheck === true ?
                        <div className="flexbuttons">
                            <button type="button" className="btn btn-success" onClick={() => this.showModal(true)}
                                    style={{margin: "0px 12px 0px 0px"}}>Accepteer
                            </button>
                            <button type="button" className="btn btn-danger"
                                    onClick={() => this.showModal(false)}>Weiger
                            </button>
                        </div>
                        : <div/>}
                </div>
                {this.state.modal === true ?
                    <Modal isAccept={this.state.isAccept} hideModal={this.hideModal} account={this.state.account}
                           saveAction={this.saveAction}/>
                    : <div/>}
            </div>
        )
    }
}

class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isAccept: props.isAccept,
            account: props.account
        }
    }

    render() {
        return <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        {this.state.isAccept === true ?
                            <h5 className="modal-title" id="exampleModalLongTitle">Issuer accepteren</h5>
                            : <h5 className="modal-title" id="exampleModalLongTitle">Issuer weigeren</h5>}

                        <button type="button" className="close" aria-label="Close" onClick={this.props.hideModal}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        {this.state.isAccept === true ?
                            <p>Naam: {this.state.account.voornaam} {this.state.account.achternaam}
                                <br/> Bedrijfsnaam: {this.state.account.bedrijfsnaam}</p>
                            : <textarea className="form-control" id="description" rows="5"/>}
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={this.props.hideModal}>Annuleer
                        </button>
                        {this.state.isAccept === true ?
                            <button type="button" className="btn btn-primary"
                                    onClick={() => this.props.saveAction("")}>Opslaan</button>
                            : <button type="button" className="btn btn-primary"
                                      onClick={() => this.props.saveAction(document.getElementById('description').value)}>Opslaan</button>}
                    </div>
                </div>
            </div>
        </div>
    }
}

