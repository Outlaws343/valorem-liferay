import React from 'react';
import ReactDOM from 'react-dom';
import IssuerList from "./IssuerList.js";

export default function (elementId) {
	ReactDOM.render(<IssuerList/>, document.getElementById(elementId));
}